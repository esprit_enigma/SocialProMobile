/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Models.Donneschart;
import Models.Entreprise;
import Models.ProjetEntreprise;
import com.codename1.components.ToastBar;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.io.Storage;
import com.codename1.ui.FontImage;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import java.util.Map;

/**
 *
 * @author marwa
 */
public class ProjetDAO {

    private ConnectionRequest connectionRequest;
public void updateProjet(ProjetEntreprise projet) {

        ArrayList<Map<String, Object>> content = new ArrayList<Map<String, Object>>();

        connectionRequest = new ConnectionRequest() {
            @Override
            protected void postResponse() {

                ToastBar.showMessage("Projet ajouté avec Succes...", FontImage.MATERIAL_INFO);
            }
        };
            System.out.println("projeeeeeeeeeet "+Storage.getInstance().readObject("idEntreprise"));
        connectionRequest.setUrl("http://localhost/SocialProScripts/Projet/updateProjet.php?id=" + Storage.getInstance().readObject("idEntreprise") + "&idproj=" + projet.getId() + "&nom=" + projet.getNom() + "&destination=" + projet.getDestination() + "&description=" + projet.getDescription() + "&date=" + projet.getDateAjout());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

public List<ProjetEntreprise> findProjetId(String id) {
        String nom;
        List<ProjetEntreprise> projet = new ArrayList<>();
        ProjetEntreprise proj = new ProjetEntreprise();

        connectionRequest = new ConnectionRequest() {

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                    ArrayList<Map<String, Object>> content = (ArrayList<Map<String, Object>>) data.get("root");
                    projet.clear();
                    System.out.println("prdddddn" + content);
                    for (Map<String, Object> obj : content) {
                        projet.add(new ProjetEntreprise(Integer.parseInt((String) obj.get("id")), (String) obj.get("nom"), (String) obj.get("destinaation"), (String) obj.get("description")));

                        System.out.println("pooooo" + projet);

                    }

                } catch (IOException err) {
                    Log.e(err);
                }
            }

        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/Projet/getProjetId.php?id=" + id);
        connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
        if (projet == null) {
            //aucun projet
            return null;
        } else {
            return projet;
        }
    }




    public void removeBook(String id){   // remove book by title
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void postResponse() {
          
            }           
        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/Projet/deleteProjet.php?id=" + id);
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }
    
    
    public List<Donneschart> getpie(String id) {
        String nom;
        List<Donneschart> projet = new ArrayList<>();
        Donneschart proj = new Donneschart();

        connectionRequest = new ConnectionRequest() {

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                    ArrayList<Map<String, Object>> content = (ArrayList<Map<String, Object>>) data.get("root");
                    projet.clear();
                    System.out.println("prdddddn" + content);
                    for (Map<String, Object> obj : content) {

                     
                        projet.add(new Donneschart(Double.parseDouble((String)obj.get("nbr")), (String)obj.get("year")));

                       

                    }

                } catch (IOException err) {
                    Log.e(err);
                }
            }

        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/Chart/getChart.php?id=" + id);
        connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
        if (projet == null) {
            //aucun projet
            return null;
        } else {
            return projet;
        }
    }



































    public List<ProjetEntreprise> findProjet(String id) {
        String nom;
        List<ProjetEntreprise> projet = new ArrayList<>();
        ProjetEntreprise proj = new ProjetEntreprise();

        connectionRequest = new ConnectionRequest() {

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                    ArrayList<Map<String, Object>> content = (ArrayList<Map<String, Object>>) data.get("root");
                    projet.clear();
                    System.out.println("prdddddn" + content);
                    for (Map<String, Object> obj : content) {
//                      proj.setId(Integer.parseInt((String)obj.get("id")));
//                      proj.setNom((String)obj.get("nom"));
//                     proj.setDestination((String)obj.get("organisation"));
//                      proj.setDescription((String)obj.get("description"));
//                       System.out.println("prpppp"+proj);
                        projet.add(new ProjetEntreprise(Integer.parseInt((String) obj.get("id")), (String) obj.get("nom"), (String) obj.get("destinaation"), (String) obj.get("description")));

                        System.out.println("pooooo" + projet);

                    }

                } catch (IOException err) {
                    Log.e(err);
                }
            }

        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/Projet/getProjet.php?id_entreprise=" + id);
        connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
        if (projet == null) {
            //aucun projet
            return null;
        } else {
            return projet;
        }
    }
    
        public void addProjet(ProjetEntreprise projet) {

        ArrayList<Map<String, Object>> content = new ArrayList<Map<String, Object>>();

        connectionRequest = new ConnectionRequest() {
            @Override
            protected void postResponse() {
//            Dialog d = new Dialog("Entreprise");
//            TextArea popupBody = new TextArea("Entreprise ajoutée avec Succes");
//            popupBody.setUIID("PopupBody");
//            popupBody.setEditable(false);
//            d.setLayout(new BorderLayout());
//            d.add(BorderLayout.CENTER, popupBody);
//            d.showDialog();
                ToastBar.showMessage("Projet ajouté avec Succes...", FontImage.MATERIAL_INFO);
            }
        };
            System.out.println("projeeeeeeeeeet "+Storage.getInstance().readObject("idEntreprise"));
        connectionRequest.setUrl("http://localhost/SocialProScripts/Projet/insert.php?id=" + Storage.getInstance().readObject("idEntreprise") + "&nom=" + projet.getNom() + "&destination=" + projet.getDestination() + "&description=" + projet.getDescription() + "&date=" + projet.getDateAjout());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

    public Entreprise findEntreprise(String user_id) {
        String nom;
        ArrayList<Entreprise> entreprise = new ArrayList<>();
        Entreprise entrepris = new Entreprise();
        Entreprise entrep = new Entreprise();

        connectionRequest = new ConnectionRequest() {

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                    ArrayList<Map<String, Object>> content = (ArrayList<Map<String, Object>>) data.get("root");
                    entreprise.clear();

                    for (Map<String, Object> obj : content) {
                        entrepris.setId(Integer.parseInt((String) obj.get("id")));
                        entrepris.setNom((String) obj.get("nom"));
                        entrepris.setAdresse((String) obj.get("adresse"));
                        entrepris.setNationalite((String) obj.get("Nationalite"));
                        entrepris.setSiteWeb((String) obj.get("SiteWeb"));
                        entrepris.setNumTel(Integer.parseInt((String) obj.get("numTel")));
                        entrepris.setDescription((String) obj.get("description"));
                        entrepris.setFacebook((String) obj.get("facebook"));
                        entrepris.setTwitter((String) obj.get("twitter"));
                        entrepris.setLinkedin((String) obj.get("linkedin"));
                        entrepris.setGooglePlus((String) obj.get("googlePlus"));

                    }

                } catch (IOException err) {
                    Log.e(err);
                }
            }

        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/Entreprise/getEntreprise.php?user_id=" + user_id);
        connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
        if (entrepris == null) {
            return null;
        } else {
            Storage s = Storage.getInstance();
            System.out.println("ZZ" + entrepris.getId());
            s.writeObject("idEntreprise", String.valueOf(entrepris.getId()));
            return entrepris;
        }
    }

}
