/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Models.CodePostal;
import Models.Delegation;
import Models.Entreprise;
import Models.Ville;
import com.codename1.components.ToastBar;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.io.Storage;
import com.codename1.ui.FontImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author marwa
 */
public class CodePostalDao {
      private ConnectionRequest connectionRequest;
      private String code;
      String nomD;
     int idd;
      String idDel;
      String idVille;
       CodePostal codeP=new CodePostal();
       Delegation de=new Delegation();
          public  List <Delegation> getAllDelegationid(String id){
       String nom;
          ArrayList<Delegation> delegations = new ArrayList<>();
      Delegation  delegation=new Delegation();

 
   
       connectionRequest = new ConnectionRequest() {
       
            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                  ArrayList<Map<String,Object>>   content = (ArrayList<Map<String, Object>>) data.get("root");
                    delegations.clear();
                  
             for( Map<String,Object> obj : content){
                   System.out.println("dddd"+content);
                     idd=(Integer.parseInt((String)obj.get("id")));
                    nomD=((String)obj.get("nom"));
                     idVille=(String)obj.get("idville");
                    // Ville v=getAllVilles(idVille).get(0);
              //delegations.add(new Delegation(Integer.parseInt((String)obj.get("id")),v,(String)obj.get("nom")));
                      
                   
                      
                       
                    }
     
                } catch (IOException err) {
                    Log.e(err);
                }
            }

           @Override
           protected void postResponse() {
             Ville v=getAllVilles(idVille).get(0);
               System.out.println("villeevile"+v);
             
             Delegation c=new Delegation(idd, v, nomD);
               System.out.println("dededede"+c.getVille());
             delegations.add(c);
           }
            
        
            
        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/CodePostal/getDelegationId.php?id="+id);
      
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
     if(delegations==null){return  null;}
     else{ 
         
         return  delegations;}
   }
      
        public     ArrayList<Ville>   getAllVilles(String id){
       String nom;
          ArrayList<Ville> villes = new ArrayList<>();
           ArrayList<String> villesN = new ArrayList<>();
           String[] villesname ;
   
 
   
       connectionRequest = new ConnectionRequest() {
       
            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                  ArrayList<Map<String,Object>>   content = (ArrayList<Map<String, Object>>) data.get("root");
                    villes.clear();
                  
             for( Map<String,Object> obj : content){
                    
                 Ville  ville=new Ville(Integer.parseInt((String)obj.get("id")),(String)obj.get("nom"));
                  System.out.println("ccville"+ville);   
                 villes.add(ville);
                  
                 
                   
                      
                       
                    }
     
                } catch (IOException err) {
                    Log.e(err);
                }
            }
            
        
            
        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/CodePostal/getVilleId.php?id="+id);
       connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
     if(villes==null){return  null;}
     else{ 
//          villesname = new String[ villesN.size() ];
//                  villesN.toArray( villesname );   
         return  villes;}
   }   
     
      
      public     ArrayList<Ville>   getAll(){
       String nom;
          ArrayList<Ville> villes = new ArrayList<>();
           ArrayList<String> villesN = new ArrayList<>();
           String[] villesname ;
   
 
   
       connectionRequest = new ConnectionRequest() {
       
            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                  ArrayList<Map<String,Object>>   content = (ArrayList<Map<String, Object>>) data.get("root");
                    villes.clear();
                  
             for( Map<String,Object> obj : content){
                    Ville  ville=new Ville();

                   ville=new Ville(Integer.parseInt((String)obj.get("id")),(String)obj.get("nom"));
                     
                 villes.add(ville);
                  villesN.add(ville.getNom());
                 
                   
                      
                       
                    }
     
                } catch (IOException err) {
                    Log.e(err);
                }
            }
            
        
            
        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/CodePostal/getVille.php");
       connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
     if(villes==null){return  null;}
     else{ 
//          villesname = new String[ villesN.size() ];
//                  villesN.toArray( villesname );   
         return  villes;}
   }
        public  List <Delegation> getAllDelegation(String id){
       String nom;
          ArrayList<Delegation> delegations = new ArrayList<>();
      Delegation  delegation=new Delegation();

 
   
       connectionRequest = new ConnectionRequest() {
       
            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                  ArrayList<Map<String,Object>>   content = (ArrayList<Map<String, Object>>) data.get("root");
                    delegations.clear();
                  
             for( Map<String,Object> obj : content){
                   System.out.println("dddd"+content);
                     
                 delegations.add(new Delegation(Integer.parseInt((String)obj.get("id")),(String)obj.get("nom")));
                      
                   
                      
                       
                    }
     
                } catch (IOException err) {
                    Log.e(err);
                }
            }
            
        
            
        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/CodePostal/getDelegation.php?idville="+id);
       connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
     if(delegations==null){return  null;}
     else{ 
         
         return  delegations;}
   }
      
      
       public   ArrayList<CodePostal> getCodePostal(String id){
       
          ArrayList<CodePostal> codes = new ArrayList<>();
      CodePostal  code=new CodePostal();

 
   
       connectionRequest = new ConnectionRequest() {
       
            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                  ArrayList<Map<String,Object>>   content = (ArrayList<Map<String, Object>>) data.get("root");
                    codes.clear();
                  
             for( Map<String,Object> obj : content){
                   System.out.println("dddd"+content);
                     
                 codes.add(new CodePostal(Integer.parseInt((String)obj.get("id")),(String)obj.get("cp")));
                      
                   
                      
                       
                    }
     
                } catch (IOException err) {
                    Log.e(err);
                }
            }
            
        
            
        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/CodePostal/getCodePostal.php?iddel="+id);
       connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
     if(codes==null){return  null;}
     else{ 
         
         return  codes;}
   }
        public   ArrayList<CodePostal> getCodePostalName(String name){
       
          ArrayList<CodePostal> codes = new ArrayList<>();
      CodePostal  code=new CodePostal();

 
   
       connectionRequest = new ConnectionRequest() {
       
            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                  ArrayList<Map<String,Object>>   content = (ArrayList<Map<String, Object>>) data.get("root");
                    codes.clear();
                  
             for( Map<String,Object> obj : content){
                   System.out.println("dddd"+content);
                     
                 codes.add(new CodePostal(Integer.parseInt((String)obj.get("id")),(String)obj.get("cp")));
                      
                   
                      
                       
                    }
     
                } catch (IOException err) {
                    Log.e(err);
                }
            }
            
        
            
        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/CodePostal/getCodePostalName.php?cp="+name);
       connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
     if(codes==null){return  null;}
     else{ 
         
         return  codes;}
   }
      
      public void updateCodePOstal(String id){
       
       ArrayList<Map<String,Object>> content=new ArrayList<Map<String,Object>>();
   
        connectionRequest=new ConnectionRequest(){
            @Override
            protected void postResponse() {

            ToastBar.showMessage("Localisation Ajoutée Avec Succes..", FontImage.MATERIAL_INFO);
            }
        };
            System.out.println("eeeee"+Storage.getInstance().readObject("id") );
        //entreprise.getUserId().getId()
        connectionRequest.setUrl("http://localhost/SocialProScripts/CodePostal/updateCodePostal.php?id=" + Storage.getInstance().readObject("identreprise") + "&idcp=" +id);
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }   
      public Entreprise findEntreprise(String user_id){
          
          ArrayList<Entreprise> entreprise = new ArrayList<>();
      Entreprise  entrepris=new Entreprise();
     

 Entreprise  entrep=new Entreprise();
   connectionRequest = new ConnectionRequest() {
       
            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                  ArrayList<Map<String,Object>>   content = (ArrayList<Map<String, Object>>) data.get("root");
                    entreprise.clear();
                  
             for( Map<String,Object> obj : content){
                      entrepris.setId(Integer.parseInt((String)obj.get("id")));
                      entrepris.setNom((String)obj.get("nom"));
                      entrepris.setAdresse((String)obj.get("adresse"));
                      entrepris.setNationalite((String)obj.get("Nationalite"));
                     entrepris.setEmailentrp((String) obj.get("emailentrp"));
                      entrepris.setNumTel(Integer.parseInt((String)obj.get("numTel")));
                      entrepris.setDescription((String)obj.get("description"));
                      entrepris.setFacebook((String) obj.get("facebook"));
                        entrepris.setTwitter((String) obj.get("twitter"));
                        entrepris.setLinkedin((String) obj.get("linkedin"));
                        entrepris.setGooglePlus((String) obj.get("googlePlus"));
                      code=(String)obj.get("idcp");
                 // entrepris.setCodePostal();
                   System.out.println("ent"+code);
                      
                       
                    }
     
                } catch (IOException err) {
                    Log.e(err);
                }
            }

              @Override
              protected void postResponse() {
                  if(code!=null)
                  {  CodePostal c=    getCodePostalId(code).get(0);
                  entrepris.setCodePostal(c);
              }}
            
        
            
        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/CodePostal/getEntr.php?user_id="+user_id);
       connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
     if(entrepris==null){return  null;}
     else{ 
         
         return  entrepris;}
   }
      
      
      
        public void updateEntreprisereseau(Entreprise entreprise){
       
       ArrayList<Map<String,Object>> content=new ArrayList<Map<String,Object>>();
   
        connectionRequest=new ConnectionRequest(){
            @Override
            protected void postResponse() {

            ToastBar.showMessage("Réseaux Sociaux ajoutées avec Succes...", FontImage.MATERIAL_INFO);
            }
        };
            System.out.println("eeeee"+Storage.getInstance().readObject("id") );
        //entreprise.getUserId().getId()
        connectionRequest.setUrl("http://localhost/SocialProScripts/Entreprise/updateEntreprisereseau.php?user_id=" + Storage.getInstance().readObject("id") + "&facebook=" + entreprise.getAdresse()+"&twitter=" + entreprise.getTwitter()+"&googlePlus=" +entreprise.getGooglePlus()+"&linkedin=" + entreprise.getLinkedin());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }
       
  public   ArrayList<CodePostal> getCodePostalId(String id){
       
          ArrayList<CodePostal> codes = new ArrayList<>();
      CodePostal  code=new CodePostal();

 
   
       connectionRequest = new ConnectionRequest() {
       
            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                  ArrayList<Map<String,Object>>   content = (ArrayList<Map<String, Object>>) data.get("root");
                    codes.clear();
                  
             for( Map<String,Object> obj : content){
                   System.out.println("dddd"+content);
                     idDel=(String)obj.get("iddel");
                    codeP.setId(Integer.parseInt((String)obj.get("id")));
                    codeP.setNom((String)obj.get("cp"));
                // codes.add(new CodePostal(Integer.parseInt((String)obj.get("id")),(String)obj.get("cp")));
                      
                   
                      
                       
                    }
     
                } catch (IOException err) {
                    Log.e(err);
                }
            }

              @Override
              protected void postResponse() {
                Delegation d=  getAllDelegationid(idDel).get(0);
                  System.out.println("ddddddggg"+d.getVille());
                codeP.setDelegation(d);
                codeP.getDelegation().setVille(d.getVille());
                
                codes.add(codeP);
              }
            
        
            
        };
       
        connectionRequest.setUrl("http://localhost/SocialProScripts/CodePostal/getCodePostalId.php?id="+id);
       
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
     if(codes==null){return  null;}
     else{ 
         
         return  codes;}
   }    
}
