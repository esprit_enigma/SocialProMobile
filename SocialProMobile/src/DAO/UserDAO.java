/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Models.User;
import com.codename1.components.ToastBar;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.io.Storage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.List;
import com.codename1.ui.util.Resources;
import com.mycompany.myapp.InitProfile;
import com.mycompany.myapp.Profile;
import com.mycompany.myapp.ProfileAffichage;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

/**
 *
 * @author ASUS
 */
public class UserDAO {
     private ConnectionRequest connectionRequest;
     Resources resourceObjectInstance;
     String id;
     String role;
     String name;
    ArrayList<User> books = new ArrayList<>();
       ArrayList<Map<String,Object>> content=new ArrayList<Map<String,Object>>();
   
      public void findUser(String username){
       connectionRequest = new ConnectionRequest() {
       
            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                    content = (ArrayList<Map<String, Object>>) data.get("root");
                    books.clear();
                  
             for( Map<String,Object> obj : content){
                      
                        id=(String)obj.get("id");
                         role=(String)obj.get("roles");
                         name=(String)obj.get("username");
                    }
                } catch (IOException err) {
                    Log.e(err);
                }
            }
            
        @Override
            protected void postResponse() {
            System.out.println("iddd"+id); 
            System.out.println("iddd"+role);
            Storage storage=Storage.getInstance();
           
             if (id ==null && role==null){
                    ToastBar.showMessage("Verifier Vos Données", FontImage.MATERIAL_INFO);
            }
             else if(!id.equals(null)&&role.equals("a:1:{i:0;s:15:\"ROLE_ENTREPRISE\";}"))
             { 
              storage.writeObject("id", id);
            storage.writeObject("role",role);
            storage.writeObject("nameuser", name);
            System.out.println("iduser"+storage.readObject("id"));
             new ProfileAffichage(resourceObjectInstance).show();}
            else if (!id.equals(null)&&role.equals("a:1:{i:0;s:18:\"ROLE_PREENTREPRISE\";}")){
                 storage.writeObject("id", id);
            storage.writeObject("role",role);
             storage.writeObject("nameuser", name);
        System.out.println("iduser"+storage.readObject("id"));
                    new InitProfile();
                    
            }
             
            else
                ToastBar.showMessage("Verifier Vos Données", FontImage.MATERIAL_INFO);
            }
            
        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/User/getUser.php?username="+username);
        NetworkManager.getInstance().addToQueue(connectionRequest);
     
     }
      public void updateUser(String roles) {

        ArrayList<Map<String, Object>> content = new ArrayList<Map<String, Object>>();

        connectionRequest = new ConnectionRequest() {
            @Override
            protected void postResponse() {

               
            }
        };
            
        connectionRequest.setUrl("http://localhost/SocialProScripts/User/updateUser.php?id=" + Storage.getInstance().readObject("id") + "&roles=" + roles );
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }
     
//        public void findAllBooks(){
//        connectionRequest = new ConnectionRequest() {
//        List<Book> books = new ArrayList<>();
//            @Override
//            protected void readResponse(InputStream in) throws IOException {
//
//                JSONParser json = new JSONParser();
//                try {
//                    Reader reader = new InputStreamReader(in, "UTF-8");
//
//                    Map<String, Object> data = json.parseJSON(reader);
//                    List<Map<String, Object>> content = (List<Map<String, Object>>) data.get("root");
//                    books.clear();
//                  
//                    for (Map<String, Object> obj : content) {
//                        books.add(new Book((String) obj.get("title"),(String) obj.get("author"),(String) obj.get("category"),Integer.parseInt((String) obj.get("isbn")))
//                        );
//                    }
//                } catch (IOException err) {
//                    Log.e(err);
//                }
//            }
//
//            @Override
//            protected void postResponse() {
//                //System.out.println(libs.size());
//                listOfBooks = new Form();
//                com.codename1.ui.List uiLibsList = new com.codename1.ui.List();
//                ArrayList<String> libsNoms = new ArrayList<>();
//                for(Book l :books){
//                    libsNoms.add(l.getTitle());
//                }
//                com.codename1.ui.list.DefaultListModel<String> listModel = new com.codename1.ui.list.DefaultListModel<>(libsNoms);
//                uiLibsList.setModel(listModel);
//                uiLibsList.addActionListener(new ActionListener() {
//
//                    @Override
//                    public void actionPerformed(ActionEvent evt) {
//                        Book currentBook = books.get(uiLibsList.getCurrentSelected());
//                        new Abook(currentBook.getTitle(),currentBook.getAuthor(),currentBook.getCategory(),currentBook.getIsbn()).show();
//                    }
//                });
//                listOfBooks.setLayout(new BorderLayout());
//                listOfBooks.add(BorderLayout.NORTH,uiLibsList);
//                listOfBooks.add(BorderLayout.SOUTH,Statics.createBackBtn());
//                listOfBooks.show();             
//            }
//        };
//        connectionRequest.setUrl("http://localhost/shelfie/getbooks.php");
//        NetworkManager.getInstance().addToQueue(connectionRequest);
//    }
//    
}
