/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Models.Entreprise;
import Models.User;
import com.codename1.components.ToastBar;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.io.Storage;
import com.codename1.ui.Dialog;
import com.codename1.ui.FontImage;
import com.codename1.ui.TextArea;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.util.Resources;
import com.mycompany.myapp.InitProfile;
import com.mycompany.myapp.Profile;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author ASUS
 */
public class EntrepriseDAO {

    private ConnectionRequest connectionRequest;

    public void addEntreprise(Entreprise entreprise) {

        ArrayList<Map<String, Object>> content = new ArrayList<Map<String, Object>>();

        connectionRequest = new ConnectionRequest() {
            @Override
            protected void postResponse() {

                ToastBar.showMessage("Entreprise Ajoutée Avec Succes...", FontImage.MATERIAL_INFO);
            }
        };
        //entreprise.getUserId().getId()
        connectionRequest.setUrl("http://localhost/SocialProScripts/Entreprise/insert.php?user_id=" + Storage.getInstance().readObject("id") + "&nom=" + entreprise.getNom() + "&adresse=" + entreprise.getAdresse() + "&Nationalite=" + entreprise.getNationalite() + "&numTel=" + entreprise.getNumTel() + "&emailentrp=" + entreprise.getEmailentrp() + "&description=" + entreprise.getDescription());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }
    
        public void updateEntreprise(Entreprise entreprise) {

        ArrayList<Map<String, Object>> content = new ArrayList<Map<String, Object>>();

        connectionRequest = new ConnectionRequest() {
            @Override
            protected void postResponse() {

                ToastBar.showMessage("Entreprise Modiftée Avec Succes...", FontImage.MATERIAL_INFO);
            }
        };
        System.out.println("Entrepriseeeeeeeeeee"+entreprise);
        connectionRequest.setUrl("http://localhost/SocialProScripts/Entreprise/update.php?user_id=" + Storage.getInstance().readObject("id") + "&nom=" + entreprise.getNom() + "&adresse=" + entreprise.getAdresse() + "&Nationalite=" + entreprise.getNationalite() + "&numTel=" + entreprise.getNumTel() + "&emailentrp=" + entreprise.getEmailentrp() + "&description=" + entreprise.getDescription());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

    public Entreprise findEntreprise(String user_id) {
        String nom;
        ArrayList<Entreprise> entreprise = new ArrayList<>();
        Entreprise entrepris = new Entreprise();
        Entreprise entrep = new Entreprise();

        connectionRequest = new ConnectionRequest() {

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                    ArrayList<Map<String, Object>> content = (ArrayList<Map<String, Object>>) data.get("root");
                    entreprise.clear();

                    for (Map<String, Object> obj : content) {
                        entrepris.setId(Integer.parseInt((String) obj.get("id")));
                       // entrepris.setId(Integer.parseInt((String) obj.get("idcp")));
                        entrepris.setNom((String) obj.get("nom"));
                        entrepris.setAdresse((String) obj.get("adresse"));
                        entrepris.setNationalite((String) obj.get("Nationalite"));
                        entrepris.setEmailentrp((String) obj.get("emailentrp"));
                        entrepris.setNumTel(Integer.parseInt((String) obj.get("numTel")));
                        entrepris.setDescription((String) obj.get("description"));
                        entrepris.setFacebook((String) obj.get("facebook"));
                        entrepris.setTwitter((String) obj.get("twitter"));
                        entrepris.setLinkedin((String) obj.get("linkedin"));
                        entrepris.setGooglePlus((String) obj.get("googlePlus"));

                    }

                } catch (IOException err) {
                    Log.e(err);
                }
            }

        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/Entreprise/getEntreprise.php?user_id=" + user_id);
        connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
        if (entrepris == null) {
            return null;
        } else {
            Storage s = Storage.getInstance();
            System.out.println("ZZ" + entrepris.getId());
            s.writeObject("idEntreprise", String.valueOf(entrepris.getId()));
            return entrepris;
        }
    }

    public void updateEntreprisereseau(Entreprise entreprise) {

        ArrayList<Map<String, Object>> content = new ArrayList<Map<String, Object>>();

        connectionRequest = new ConnectionRequest() {
            @Override
            protected void postResponse() {

                ToastBar.showMessage("Réseaux Sociaux modifiée avec Succes...", FontImage.MATERIAL_INFO);
            }
        };
        System.out.println("eeeee" + Storage.getInstance().readObject("id"));
        //entreprise.getUserId().getId()
      
        connectionRequest.setUrl("http://localhost/SocialProScripts/Entreprise/updateEntreprisereseau.php?user_id=" + Storage.getInstance().readObject("id") + "&facebook=" + entreprise.getFacebook()+ "&twitter=" + entreprise.getTwitter() + "&googlePlus=" + entreprise.getGooglePlus() + "&linkedin=" + entreprise.getLinkedin());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }

    public Entreprise findEntrepriseReseau(String user_id) {
        String nom;
        ArrayList<Entreprise> entreprise = new ArrayList<>();
        Entreprise entrepris = new Entreprise();
        Entreprise entrep = new Entreprise();

        connectionRequest = new ConnectionRequest() {

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                    ArrayList<Map<String, Object>> content = (ArrayList<Map<String, Object>>) data.get("root");
                    entreprise.clear();

                    for (Map<String, Object> obj : content) {
                        System.out.println("dddddd" + obj.get("facebook"));
                        entrepris.setId(Integer.parseInt((String) obj.get("id")));
                        entrepris.setFacebook((String) obj.get("facebook"));
                        entrepris.setGooglePlus((String) obj.get("googlePlus"));
                        entrepris.setTwitter((String) obj.get("twitter"));
                        entrepris.setLinkedin((String) obj.get("linkedin"));

                    }

                } catch (IOException err) {
                    Log.e(err);
                }
            }

        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/Entreprise/getEntreprise.php?user_id=" + user_id);
        connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
        if (entrepris == null) {
            return null;
        } else {
            Storage s = Storage.getInstance();
            System.out.println("ZZ" + entrepris.getId());
            s.writeObject("idEntreprise", String.valueOf(entrepris.getId()));
            return entrepris;
        }
    }

}
