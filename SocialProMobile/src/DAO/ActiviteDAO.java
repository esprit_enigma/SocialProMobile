/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Models.Activite;
import Models.AvoirActivite;
import Models.Categorie;
import Models.Entreprise;
import Models.ProjetEntreprise;
import com.codename1.components.ToastBar;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.io.Storage;
import com.codename1.ui.FontImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ASUS
 */
public class ActiviteDAO {

    private ConnectionRequest connectionRequest;
    int idact;
    int idct;
    String description;
   ArrayList<Map<String, Object>> conten=new  ArrayList<Map<String, Object>> ();
  
    public void removeAvoirAct(String id){   // remove book by title
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void postResponse() {
          
            }           
        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/Activite/deleteAvoirAct.php?id=" + id);
        NetworkManager.getInstance().addToQueue(connectionRequest);
    } 
    
    public void removeAct(String id){   // remove book by title
        connectionRequest = new ConnectionRequest() {
            @Override
            protected void postResponse() {
          
            }           
        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/Activite/deletAct.php?id=" + id);
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }
   public void updateActiviteId(Activite act) {

        ArrayList<Map<String, Object>> content = new ArrayList<Map<String, Object>>();

        connectionRequest = new ConnectionRequest() {
            @Override
            protected void postResponse() {
//            Dialog d = new Dialog("Entreprise");
//            TextArea popupBody = new TextArea("Entreprise ajoutée avec Succes");
//            popupBody.setUIID("PopupBody");
//            popupBody.setEditable(false);
//            d.setLayout(new BorderLayout());
//            d.add(BorderLayout.CENTER, popupBody);
//            d.showDialog();
                ToastBar.showMessage("Activite modifié avec succes...", FontImage.MATERIAL_INFO);
            }
        };
           // System.out.println("projeeeeeeeeeet "+Storage.getInstance().readObject("idEntreprise"));
        connectionRequest.setUrl("http://localhost/SocialProScripts/Activite/updateActId.php?id=" + act.getId() + "&categorie_id=" + act.getCategorie().getId()+ "&nom=" + act.getNom());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }
   
   public void updateActivite(AvoirActivite act) {

        ArrayList<Map<String, Object>> content = new ArrayList<Map<String, Object>>();

        connectionRequest = new ConnectionRequest() {
            @Override
            protected void postResponse() {
//            Dialog d = new Dialog("Entreprise");
//            TextArea popupBody = new TextArea("Entreprise ajoutée avec Succes");
//            popupBody.setUIID("PopupBody");
//            popupBody.setEditable(false);
//            d.setLayout(new BorderLayout());
//            d.add(BorderLayout.CENTER, popupBody);
//            d.showDialog();
                ToastBar.showMessage("Activite modifié avec succes...", FontImage.MATERIAL_INFO);
            }
        };
           // System.out.println("projeeeeeeeeeet "+Storage.getInstance().readObject("idEntreprise"));
        connectionRequest.setUrl("http://localhost/SocialProScripts/Activite/updateAct.php?id_entreprise=" + Storage.getInstance().readObject("idEntreprise") + "&id_activite=" + act.getActivite().getId() + "&description=" + act.getDescription() + "&id=" + act.getId() );
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }
    public List<AvoirActivite> findAvoirActivite(String id) {
        String nom;
        List<AvoirActivite> avoiractivites = new ArrayList<>();
        AvoirActivite proj = new AvoirActivite();
        

        connectionRequest = new ConnectionRequest() {

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
          conten = (ArrayList<Map<String, Object>>) data.get("root");
                    avoiractivites.clear();
                    System.out.println("avoir" + conten);
                 
                } catch (IOException err) {
                    Log.e(err);
                }
            }
             @Override
              protected void postResponse() {
                     for (Map<String, Object> obj : conten) {
                        idact=Integer.parseInt((String) obj.get("id_activite"));
                        description=(String) obj.get("description");
                        String id=(String)obj.get("id");
                        //avoiractivites.add(new AvoirActivite((String) obj.get("description"),(Activite)findActivite(Integer.parseInt((String) obj.get("id_activite")))));

                       Activite d=  findActivite(idact);
             avoiractivites.add(new AvoirActivite(Integer.parseInt(id),description,d));

               System.out.println("ddddddggg"+d);

                    }

             
                
              }

        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/Activite/getAvoirActivites.php?id=" + id);
        connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
        if (avoiractivites == null) {
            //aucun projet
            return null;
        } else {
               System.out.println("pooooo" + avoiractivites);
            return avoiractivites;
        }
    }
     
       public List<AvoirActivite> findAvoirActiviteId(String id) {
        String nom;
        List<AvoirActivite> avoiractivites = new ArrayList<>();
        AvoirActivite proj = new AvoirActivite();
        

        connectionRequest = new ConnectionRequest() {

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
          conten = (ArrayList<Map<String, Object>>) data.get("root");
                    avoiractivites.clear();
                    System.out.println("avoir" + conten);
                 
                } catch (IOException err) {
                    Log.e(err);
                }
            }
             @Override
              protected void postResponse() {
                     for (Map<String, Object> obj : conten) {
                        idact=Integer.parseInt((String) obj.get("id_activite"));
                        description=(String) obj.get("description");
                        String id=(String)obj.get("id");
                        //avoiractivites.add(new AvoirActivite((String) obj.get("description"),(Activite)findActivite(Integer.parseInt((String) obj.get("id_activite")))));

                       Activite d=  findActivite(idact);
             avoiractivites.add(new AvoirActivite(Integer.parseInt(id),description,d));

               System.out.println("ddddddggg"+d);

                    }

             
                
              }

        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/Activite/getAvoirActId.php?id=" + id);
        connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
        if (avoiractivites == null) {
            //aucun projet
            return null;
        } else {
               System.out.println("pooooo" + avoiractivites);
            return avoiractivites;
        }
    }
     public Activite findActivite(int id) {
        String nom;
        ArrayList<Activite> entreprise = new ArrayList<>();
        Activite entrepris = new Activite();
        Activite entrep = new Activite();

        connectionRequest = new ConnectionRequest() {

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                    ArrayList<Map<String, Object>> content = (ArrayList<Map<String, Object>>) data.get("root");
                    entreprise.clear();

                    for (Map<String, Object> obj : content) {
                        idct=Integer.parseInt((String) obj.get("categorie_id"));
                        entrepris.setId(Integer.parseInt((String) obj.get("id")));
                        entrepris.setNom((String) obj.get("nom"));
                       // entrepris.setCategorie((Categorie)findCategorie((Integer.parseInt((String) obj.get("categorie_id")))));
        
                        
                    }

                } catch (IOException err) {
                    Log.e(err);
                }
            }
               @Override
              protected void postResponse() {
               Categorie d=  findCategorie(idct);
                entrepris.setCategorie(d);
               System.out.println("ddddddggg"+d.getNom());
               
                
              }

        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/Activite/getActivite.php?idact=" + id);
        connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
        if (entrepris == null) {
            return null;
        } else {
           
            return entrepris;
        }
    }
     

    public Categorie findCategorie(int id) {
        String nom;
        ArrayList<Categorie> entreprise = new ArrayList<>();
        Categorie entrepris = new Categorie();
        Categorie entrep = new Categorie();

        connectionRequest = new ConnectionRequest() {

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                    ArrayList<Map<String, Object>> content = (ArrayList<Map<String, Object>>) data.get("root");
                    entreprise.clear();

                    for (Map<String, Object> obj : content) {
                        entrepris.setId(Integer.parseInt((String) obj.get("id")));
                        entrepris.setNom((String) obj.get("nom"));

                    }

                } catch (IOException err) {
                    Log.e(err);
                }
            }

        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/Activite/getCategorie.php?id=" + id);
        connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
        if (entrepris == null) {
            return null;
        } else {
         
            return entrepris;
        }
    }

    public ArrayList<Categorie> getAll() {
        String nom;
        ArrayList<Categorie> categories = new ArrayList<>();
        ArrayList<String> categorie = new ArrayList<>();
        String[] categoriesname;

        connectionRequest = new ConnectionRequest() {

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                    ArrayList<Map<String, Object>> content = (ArrayList<Map<String, Object>>) data.get("root");
                    categories.clear();

                    for (Map<String, Object> obj : content) {
                        Categorie cat = new Categorie();

                        cat = new Categorie((String) obj.get("nom"));
//Integer.parseInt((String) obj.get("id")), 
                        categories.add(cat);
                        categorie.add(cat.getNom());

                    }

                } catch (IOException err) {
                    Log.e(err);
                }
            }

        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/Categorie/getCategorie.php");
        connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
        if (categories == null) {
            return null;
        } else { 
            return categories;
        }
    }
    public Categorie findCategorieByNom(String nom) {
        String nomn;
        ArrayList<Categorie> entreprise = new ArrayList<>();
        Categorie entrepris = new Categorie();
        Categorie entrep = new Categorie();

        connectionRequest = new ConnectionRequest() {

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                    ArrayList<Map<String, Object>> content = (ArrayList<Map<String, Object>>) data.get("root");
                    entreprise.clear();

                    for (Map<String, Object> obj : content) {
                        entrepris.setId(Integer.parseInt((String) obj.get("id")));
                        entrepris.setNom((String) obj.get("nom"));

                    }

                } catch (IOException err) {
                    Log.e(err);
                }
            }

        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/Categorie/findCategorieNom.php?nom=" + nom);
        connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
//        if (entrepris == null) {
//            return null;
//        } else {
//            Storage s = Storage.getInstance();
//            System.out.println("ZZ" + entrepris.getId());
//            s.writeObject("idEntreprise", String.valueOf(entrepris.getId()));
            return entrepris;
        
        
    }
    public Activite findActByNom(String nom) {
        String nomn;
        ArrayList<Activite> entreprise = new ArrayList<>();
        Activite entrepris = new Activite();
        Activite entrep = new Activite();

        connectionRequest = new ConnectionRequest() {

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                    ArrayList<Map<String, Object>> content = (ArrayList<Map<String, Object>>) data.get("root");
                    entreprise.clear();

                    for (Map<String, Object> obj : content) {
                        entrepris.setId(Integer.parseInt((String) obj.get("id")));
                        entrepris.setNom((String) obj.get("nom"));

                    }

                } catch (IOException err) {
                    Log.e(err);
                }
            }

        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/Activite/findActiviteNom.php?nom=" + nom);
        connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
//        if (entrepris == null) {
//            return null;
//        } else {
//            Storage s = Storage.getInstance();
//            System.out.println("ZZ" + entrepris.getId());
//            s.writeObject("idEntreprise", String.valueOf(entrepris.getId()));
            return entrepris;
        
        
    }
    public Activite findActiviteByNom(String nom) {
        String nomn;
        ArrayList<Activite> entreprise = new ArrayList<>();
        Activite entrepris = new Activite();
        Activite entrep = new Activite();

        connectionRequest = new ConnectionRequest() {

            @Override
            protected void readResponse(InputStream in) throws IOException {

                JSONParser json = new JSONParser();
                try {
                    Reader reader = new InputStreamReader(in, "UTF-8");

                    Map<String, Object> data = json.parseJSON(reader);
                    ArrayList<Map<String, Object>> content = (ArrayList<Map<String, Object>>) data.get("root");
                    entreprise.clear();

                    for (Map<String, Object> obj : content) {
//                        idct = Integer.parseInt((String) obj.get("categorie_id"));
//                        entrepris.getCategorie().setId(idct);
                        entrepris.setId(Integer.parseInt((String) obj.get("id")));
                        entrepris.setNom((String) obj.get("nom"));

                    }

                } catch (IOException err) {
                    Log.e(err);
                }
            }

        };
        connectionRequest.setUrl("http://localhost/SocialProScripts/Activite/findActiviteNom.php?nom="+nom);
        connectionRequest.setPost(false);
        NetworkManager.getInstance().addToQueueAndWait(connectionRequest);
//        if (entrepris == null) {
//            return null;
//        } else {
//            Storage s = Storage.getInstance();
//            System.out.println("ZZ" + entrepris.getId());
//            s.writeObject("idEntreprise", String.valueOf(entrepris.getId()));
//            return entrepris;
//        
   return entrepris;
    }
    public void addActivite(Activite projet) {
        String t = null;
        ArrayList<Map<String, Object>> content = new ArrayList<Map<String, Object>>();

        connectionRequest = new ConnectionRequest() {
            @Override
            protected void postResponse() {
                //ToastBar.showMessage("Projet ajouté avec Succes...", FontImage.MATERIAL_INFO);
                System.out.println("Activite Ajoutée");
            }
            
        };
        
           //findCategorieByNom(t).getId()
        connectionRequest.setUrl("http://localhost/SocialProScripts/Activite/insertActivite.php?categorie_id=" + projet.getCategorie().getId()+ "&nom=" + projet.getNom());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }
    
        public void addAvoirActivite(AvoirActivite projet) {
        String t = null;
        ArrayList<Map<String, Object>> content = new ArrayList<Map<String, Object>>();

        connectionRequest = new ConnectionRequest() {
            @Override
            protected void postResponse() {
                ToastBar.showMessage("Activite ajoutée avec Succes...", FontImage.MATERIAL_INFO);
                System.out.println("Activite Ajoutée");
            }
            
        };
        //findActiviteByNom(t)
        
           System.out.println("projeeeeeeeeeet "+Storage.getInstance().readObject("idEntreprise"));
        connectionRequest.setUrl("http://localhost/SocialProScripts/Activite/insertAvoirActivite.php?id_activite=" + projet.getActivite().getId()+ "&id_entreprise=" + Storage.getInstance().readObject("idEntreprise") + "&description=" + projet.getDescription());
        NetworkManager.getInstance().addToQueue(connectionRequest);
    }
    
    

}
