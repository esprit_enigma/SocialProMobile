/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import DAO.ActiviteDAO;
import DAO.ProjetDAO;
import Models.AvoirActivite;
import Models.ProjetEntreprise;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.MultiButton;
import com.codename1.components.ToastBar;
import com.codename1.io.Storage;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.SwipeableContainer;
import com.codename1.ui.events.ScrollListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.table.TableLayout;
import com.codename1.ui.util.Resources;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class Activite extends SideMenu {
private long lastScroll;
    private boolean messageShown;
    private Object circleMask;
    private int circleMaskWidth;
    private int circleMaskHeight;
    private Font letterFont;
    private boolean finishedLoading;
    Image circleImage;
    
      public Activite() {
         this(Resources.getGlobalResources());
    }
    

    public Activite(Resources resourceObjectInstance) {
        super(BoxLayout.y());
          //initGuiBuilderComponents(resourceObjectInstance);
        getToolbar().setUIID("tool");
        getToolbar().setTitleComponent(
                FlowLayout.encloseCenterMiddle(
                        new Label("Activités", "Title")
                       
                )
        );
         installSidemenu(resourceObjectInstance);
//         BorderLayout layout=new BorderLayout();
//         setLayout(layout);
//         Button b1=new Button("rahma");
//         addComponent(BorderLayout.SOUTH, b1);
Storage storage = Storage.getInstance();
        String id = (String) storage.readObject("idEntreprise");
        System.out.println("uddddd" + id);

        circleImage = MyApplication.theme.getImage("admina.png");
        //circleLineImage = MyApplication.theme.getImage("circle-line.png");
        addPointerDraggedListener(e -> lastScroll = System.currentTimeMillis());
        addShowListener(e -> {
            if (!messageShown) {
                messageShown = true;
                ToastBar.showMessage("Swipe the contacts to both sides to expose additional options", FontImage.MATERIAL_COMPARE_ARROWS, 2000);
            }
        });

     //   circleMask = circleImage.createMask();
//        circleMaskWidth = circleImage.getWidth();
//        circleMaskHeight = circleImage.getHeight();
        letterFont = Font.createTrueTypeFont("native:MainThin", "native:MainThin");
        letterFont = letterFont.derive(circleMaskHeight - circleMaskHeight / 3, Font.STYLE_PLAIN);
       
        final Container contactsDemo = new Container(BoxLayout.y());
        contactsDemo.setScrollableY(true);
        contactsDemo.add(FlowLayout.encloseCenterMiddle(new InfiniteProgress()));

        Display.getInstance().scheduleBackgroundTask(() -> {
            List<AvoirActivite> activites = new ArrayList<AvoirActivite>();
            activites = new ActiviteDAO().findAvoirActivite(id);
            System.out.println("prr" + activites);

            List<AvoirActivite> contacts = activites;

            Display.getInstance().callSerially(() -> {
                contactsDemo.removeAll();
                for (AvoirActivite c : contacts) {
                    
                         String des = c.getDescription();
                
                    String dname = new ActiviteDAO().findActivite(c.getActivite().getId()).getNom();
                   
                    String dcategorie = c.getActivite().getCategorie().getNom();
                  
                    if (dname == null || dname.length() == 0) {
                        continue;
                    }
                    MultiButton mb = new MultiButton(dname);
//                        mb.add(circleImage);
                    mb.setIcon(circleImage);
                   mb.setTextLine2(dcategorie);
                    mb.setTextLine3(des);
                    mb.setIconUIID("ContactIcon");

                    // we need this for the SwipableContainer below
                    mb.getAllStyles().setBgTransparency(255);

                    Button delete = new Button();
                    delete.setUIID("SwipeableContainerButton");
                    FontImage.setMaterialIcon(delete, FontImage.MATERIAL_DELETE, 8);

                    Button info = new Button();
                    info.setUIID("SwipeableContainerInfoButton");
                    FontImage.setMaterialIcon(info, FontImage.MATERIAL_EDIT, 8);
                    info.addActionListener(e -> {
                        Dialog dlg = new Dialog(dname);
                        TableLayout tl = new TableLayout(3, 2);
                        dlg.setLayout(tl);

                    });

                    

                    Container options;
                    if (c.getDescription()!= null) {

                        options = GridLayout.encloseIn(2, info);
                    } else {
                        options = GridLayout.encloseIn(2, info);
                    }

                    SwipeableContainer sc = new SwipeableContainer(
                            options,
                            GridLayout.encloseIn(1, delete),
                            mb);
                    contactsDemo.add(sc);
                    sc.setUIID("conline");
                    sc.addSwipeOpenListener(e -> {
                        // auto fold the swipe when we go back to scrolling
                        contactsDemo.addScrollListener(new ScrollListener() {
                            int initial = -1;

                            @Override
                            public void scrollChanged(int scrollX, int scrollY, int oldscrollX, int oldscrollY) {
                                // scrolling is very sensitive on devices...
                                if (initial < 0) {
                                    initial = scrollY;
                                }
                                lastScroll = System.currentTimeMillis();
                                if (Math.abs(scrollY - initial) > mb.getHeight() / 2) {
                                    if (sc.getParent() != null) {
                                        sc.close();
                                    }
                                    contactsDemo.removeScrollListener(this);
                                }
                            }
                        });
                    });

                    delete.addActionListener(e -> {
//                        if(Dialog.show("Delete", "Are you sure?\nThis will delete this contact permanently!", "Delete", "Cancel")) {
//                            // can happen in the case of got() contacts
//                            if(c.getId() != null) {
//                                Display.getInstance().deleteContact(c.getId());
//                            }
                        sc.remove();
                        contactsDemo.animateLayout(800);

                    });

                }
                contactsDemo.revalidate();

                finishedLoading = true;
            });
        });
        
        FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ADD);
        fab.addActionListener(e -> {
             new ActiviteUnique().show();
            //ToastBar.showMessage("Floating action button pressed...", FontImage.MATERIAL_INFO);
        });
    FlowLayout flow = new FlowLayout(Component.RIGHT);
    flow.setValign(Component.BOTTOM);
    Container conUpper = new Container();
    //conUpper.addComponent(fab);
    conUpper.setScrollable(false);

        conUpper = fab.bindFabToContainer(contactsDemo);
        //add(BorderLayout.CENTER,contactsDemo);
        //addComponent(BorderLayout.CENTER,contactsDemo);
        addComponent(conUpper);
        //setScrollable(false);
              //addComponent(c); marwasalem08@gmail.com

    }
     @Override
    protected boolean isCurrentActivite() {
        return true;
    }
    
}
