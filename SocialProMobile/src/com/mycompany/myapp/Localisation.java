/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import DAO.CodePostalDao;
import Models.CodePostal;
import Models.Delegation;
import Models.Entreprise;
import Models.Ville;
import com.codename1.components.ToastBar;
import com.codename1.googlemaps.MapContainer;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.io.Storage;
import com.codename1.location.Location;
import com.codename1.location.LocationManager;
import com.codename1.maps.Coord;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.events.FocusListener;
import com.codename1.ui.events.SelectionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.list.DefaultListModel;
import com.codename1.ui.list.ListModel;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import java.util.List;
import java.util.Map;


/**
 *
 * @author ASUS
 */
public class Localisation extends Form{
     private static final String HTML_API_KEY = "AIzaSyA2Ur7ngPkr65rkooE9998nxs96doZR-rk";
    Coord coords;
    Resources resourceObjectInstance;
    
    boolean onBack() {
        return true;
    }

    public Localisation() {
        final MapContainer cnt = new MapContainer(HTML_API_KEY);

         Style iconStyle = getUIManager().getComponentStyle("Title");
   
         FontImage markerImg = FontImage.createMaterial(FontImage.MATERIAL_LOCATION_ON, iconStyle, 4);
     
               
        setUIID("background");
        Toolbar.setGlobalToolbar(true);
        getToolbar().setUIID("tool");
         getToolbar().setTitleComponent(
                FlowLayout.encloseCenterMiddle(
                        new Label("Localisation", "Title")
                       
                )
        );
         Form previous = Display.getInstance().getCurrent();
         getToolbar().setBackCommand(" ", ee -> {
            if (onBack()) {
                previous.showBack();
            }
        });
         BorderLayout mainLayout = new BorderLayout();
        mainLayout.setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);
        setLayout(mainLayout);
        setTitle("Localisation");
        
      
       ComboBox choix=new ComboBox();
        choix.setName("ville");
         ComboBox choix2=new ComboBox();
         TextField name = new TextField("", "Code Postale", 20, TextField.ANY);
    Button save = new Button("Enregistrer");
    save.setUIID("LoginButton");
        Entreprise ent=new Entreprise();
        ent=new CodePostalDao().findEntreprise((String)Storage.getInstance().readObject("id"));
        System.out.println("ffffffff"+ent);
         if(ent.getCodePostal() == null){
            System.out.println("nullentreprise");
            choix.addItem(" __choisissez une Ville__");
           choix2.addItem("__choisissez une Delegation__");
           
       ArrayList<Ville>villes=new CodePostalDao().getAll();
              
      DefaultListModel<Ville>c=new DefaultListModel<>();
      for(Ville v:villes)
      { c.addItem(v);}
      
       choix.setModel(c);
        
       choix.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                  Ville v=(Ville)choix.getSelectedItem();
                 System.out.println("villese"+v);
                    coords = getCoords(v.getNom());
            System.out.println("coord"+coords);
           
              cnt.addMarker(
                    EncodedImage.createFromImage(markerImg, false),
                    coords,
                    v.getNom(),
                    "",
                    e3->{
                        ToastBar.showMessage("You clicked ", FontImage.MATERIAL_PLACE);
                    }
            );
               cnt.setCameraPosition(coords);

                List<Delegation> deleg=new CodePostalDao().getAllDelegation(String.valueOf(v.getId()));
                DefaultListModel<Delegation>del=new DefaultListModel<>();
  for(Delegation d:deleg)
      { del.addItem(d);}
      
        choix2.setHint("Delegation");
        choix2.setModel(del);
         choix2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
               
               
                Delegation d=(Delegation)choix2.getSelectedItem();
                  System.out.println("ccccccddd"+choix2.getSelectedItem());
                 ArrayList<CodePostal>  codes=new CodePostalDao().getCodePostal(String.valueOf(d.getId()));
                 name.setText(codes.get(0).getNom());
                 save.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                      
                     ArrayList<CodePostal> code= new CodePostalDao().getCodePostalName(codes.get(0).getNom());
                        System.out.println("coderet"+code);
                      new CodePostalDao().updateCodePOstal(String.valueOf(code.get(0).getId()));
                       new ProfileAffichage(resourceObjectInstance).show(); 
                        
                    }
                });
            }
        });
            }
        });

           
           
           
           
           
           
           
           
           
           
           
           
           
           
            
        }
        else{
             // System.out.println("rempentreprise"+ent.getCodePostal().getDelegation().getVille().getNom());
              
         //  choix.setHint(ent.getCodePostal().getDelegation().getVille().getNom());
            name.setHint(ent.getCodePostal().getNom());
         choix2.addItem(ent.getCodePostal().getDelegation().getNom());
          //choix.addItem(ent.getCodePostal().getDelegation().getVille().getNom());
             System.out.println("fffbbb"+ent.getCodePostal().getDelegation().getVille().getNom());
             
              ArrayList<Ville>villes=new CodePostalDao().getAll();
              
      DefaultListModel<Ville>c=new DefaultListModel<>();
       DefaultListModel<Ville>cb =new DefaultListModel<>();
      for(Ville v:villes)
      {   c.addItem(v);
          if(v.getNom().equals(ent.getCodePostal().getDelegation().getVille().getNom()))
      { 
      cb.addItem(v);
      }
      }
          for(Ville ve:villes){
              if (!(ve.getNom().equals(ent.getCodePostal().getDelegation().getVille().getNom())))
                      {cb.addItem(ve);}
          }
      
      
       choix.setModel(cb);
        
       choix.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                  Ville v=(Ville)choix.getSelectedItem();
                    coords = getCoords(v.getNom());
            System.out.println("coord"+coords);
           
              cnt.addMarker(
                    EncodedImage.createFromImage(markerImg, false),
                    coords,
                    v.getNom(),
                    "",
                    e3->{
                        ToastBar.showMessage("You clicked ", FontImage.MATERIAL_PLACE);
                    }
            );
               cnt.setCameraPosition(coords);

                 System.out.println("villese"+v);
                List<Delegation> deleg=new CodePostalDao().getAllDelegation(String.valueOf(v.getId()));
                DefaultListModel<Delegation>del=new DefaultListModel<>();
  for(Delegation d:deleg)
      { del.addItem(d);}
      
        choix2.setHint("Delegation");
        choix2.setModel(del);
         choix2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
               
               
                Delegation d=(Delegation)choix2.getSelectedItem();
                  System.out.println("ccccccddd"+choix2.getSelectedItem());
                 ArrayList<CodePostal>  codes=new CodePostalDao().getCodePostal(String.valueOf(d.getId()));
                 name.setText(codes.get(0).getNom());
                 save.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                      
                     ArrayList<CodePostal> code= new CodePostalDao().getCodePostalName(codes.get(0).getNom());
                        System.out.println("coderet"+code);
                      new CodePostalDao().updateCodePOstal(String.valueOf(code.get(0).getId()));
                      new ProfileAffichage(resourceObjectInstance).show(); 
                        
                        
                    }
                });
            }
        });
            }
        });
             
             
             
             
            
        }
    
      
      

 
            
            Button btnMoveCamera = new Button("Move Camera");
            btnMoveCamera.addActionListener(e->{
                cnt.setCameraPosition(new Coord(-33.867, 151.206));
            });
            
            
          
           
  
           
      Container root = LayeredLayout.encloseIn(
                    
                    
                    BorderLayout.center(cnt)
            );
        
       root.setPreferredSize(new Dimension(100, 500));
        
            
        FontImage.setMaterialIcon(name.getHintLabel(), FontImage.MATERIAL_FACE);
         
         //Container comps = Container();
       Container actionsC = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        //actionsC.setUIID("InputContainerBackground");
        actionsC.addComponent(root);
        actionsC.addComponent(choix);
        actionsC.addComponent(choix2);
        actionsC.addComponent(name);
        
       actionsC.addComponent(save);
        
     
         addComponent(BorderLayout.CENTER,actionsC);}
    
    
    
    
    
    
    
    
     public static Coord getCoords(String address) {

        Coord ret = null;

        try {

            ConnectionRequest request = new ConnectionRequest("https://maps.googleapis.com/maps/api/geocode/json", false);

            request.addArgument("key", HTML_API_KEY);

            request.addArgument("address", address);



            NetworkManager.getInstance().addToQueueAndWait(request);

            Map<String, Object> response = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(request.getResponseData()), "UTF-8"));

            if (response.get("results") != null) {

                ArrayList results = (ArrayList) response.get("results");

                if (results.size() > 0) {

                    LinkedHashMap location = (LinkedHashMap) ((LinkedHashMap) ((LinkedHashMap) results.get(0)).get("geometry")).get("location");

                    ret = new Coord((double) location.get("lat"), (double) location.get("lng"));

                }

            }

        } catch (IOException e) {

            e.printStackTrace();

        }

        return ret;

    }

    
    
    
    
    
}
