/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import Models.ProjetEntreprise;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Font;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.List;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.list.ListCellRenderer;
import java.io.IOException;

/**
 *
 * @author ASUS
 */
public class ProjetCell extends Container implements ListCellRenderer<ProjetEntreprise>{
    private Label icone= new Label("");
    private Image icon;
    private Label nom = new Label("");
    private Label client = new Label("");
    private Label description = new Label("");

    public ProjetCell() {
       try{
            icon=Image.createImage("/mallet.jpg");
        }catch(IOException ex)
        { Dialog.show("Erreur", ex.getMessage(), "Ok", null); }
        setLayout(new BorderLayout());
        addComponent(BorderLayout.WEST,icone);
        Container c = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        //nom.getAllStyles().setBgTransparency(0);
        nom.getAllStyles().setFont(Font.createSystemFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_MEDIUM));
        //client.getAllStyles().setBgTransparency(0);
        //description.getAllStyles().setBgTransparency(0);
        c.addComponent(nom);
        c.addComponent(client);
        c.addComponent(description);
      
        addComponent(BorderLayout.CENTER,c);
       
    }
    
    @Override
    public Component getListFocusComponent(List list) {
        return this;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Component getListCellRendererComponent(List list, ProjetEntreprise value, int index, boolean isSelected) {
    nom.setText(value.getNom());
    client.setText(value.getDestination());
    description.setText(value.getDescription());
    icone.setIcon(icon);
      getStyle().setMarginTop(20);
        getStyle().setBgColor(0xff0000);
    return this;
    }
    
}
