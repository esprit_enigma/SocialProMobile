/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import DAO.CodePostalDao;
import DAO.EntrepriseDAO;
import Models.Entreprise;
import com.codename1.components.SpanLabel;
import com.codename1.io.Log;
import com.codename1.io.Storage;
import com.codename1.io.services.ImageDownloadService;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.table.TableLayout;
import com.codename1.ui.util.Resources;
import java.io.IOException;

/**
 *
 * @author ASUS
 */
public class ProfileAffichage extends SideMenu {

    private EncodedImage ei;

    public ProfileAffichage() {
        this(Resources.getGlobalResources());
    }

    public ProfileAffichage(Resources res) {
        super(BoxLayout.y());
        setUIID("profile");
        setLayout(new BorderLayout());
//        Toolbar tb = new Toolbar(true);
//        setToolbar(tb);
        getToolbar().setUIID("tt");
        getToolbar().getAllStyles().setBgTransparency(0);
        installSidemenu(res);
        Style iconStyle = getUIManager().getComponentStyle("Title");
        FontImage leftArrow = FontImage.createMaterial(FontImage.MATERIAL_PIN_DROP, iconStyle, 4);
        getToolbar().addCommandToRightBar("", leftArrow, (e) -> new Localisation().show());
        FontImage leftArrowA = FontImage.createMaterial(FontImage.MATERIAL_PUBLIC, iconStyle, 4);
        getToolbar().addCommandToRightBar("", leftArrowA, (e) -> new ReseauSociaux().show());
        FontImage rightArrow = FontImage.createMaterial(FontImage.MATERIAL_ARROW_FORWARD, iconStyle, 4);
        Storage storage = Storage.getInstance();
        String id = (String) storage.readObject("id");
        System.out.println("uddddd" + id);

        System.out.println("ennnn" + new EntrepriseDAO().findEntreprise(id));
        Entreprise ent = new Entreprise();
       ent=new CodePostalDao().findEntreprise((String)Storage.getInstance().readObject("id"));
//        try {
//            ei = EncodedImage.create("/splashScreen.png");
//        } catch (IOException ex) {
//            Dialog.show("Erreur", ex.getMessage(), "OK", null);
//        }

        String nameImage = (String) Storage.getInstance().readObject("nameuser");

        //Image profilePic = URLImage.createToStorage(ei, "Mahot", "http://localhost/SocialProScripts/ressources/" + nameImage + ".jpg", URLImage.RESIZE_SCALE);
        Label image = new Label();
        
        //System.out.println("rahma" + profilePic.getImageName());
        Image mask = MyApplication.theme.getImage("NN.png");
        //profilePic = profilePic.fill(mask.getWidth(), mask.getHeight());
        ImageDownloadService.createImageToStorage("http://localhost/SocialProScripts/ressources/" + nameImage + ".jpg",image, nameImage, null);
        //Label profilePicLabel = new Label(profilePic, "ProfilePicTitle");
        //profilePicLabel.setMask(mask.createMask());
        image.setUIID("ProfilePicTitle");
        image.setMask(mask.createMask());

        Container titleContainer = Container.encloseIn(
                new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER),
                image, BorderLayout.CENTER);
        //titleContainer.setUIID("TitleContainer");
        titleContainer.getAllStyles().setBgTransparency(0);
        Label name = new Label();
        Label nationalite = new Label();
        Label telephone = new Label();
        Label email = new Label();
        Label adresse = new Label();
        SpanLabel  description = new SpanLabel ();
        SpanLabel facebook = new SpanLabel();
        Label googleplus = new Label();
        SpanLabel linkedin = new SpanLabel();
        Label Twitter = new Label();
        Label code = new Label();
        Label ville = new Label();
        Label deleg = new Label();
        name.setText("Nom : " + ent.getNom());
        FontImage.setMaterialIcon(name, FontImage.MATERIAL_FACE);
        nationalite.setText("Nationalité : " + ent.getNationalite());
        FontImage.setMaterialIcon(nationalite, FontImage.MATERIAL_PERSON);
        telephone.setText("Telephone : " + String.valueOf(ent.getNumTel()));
        FontImage.setMaterialIcon(telephone, FontImage.MATERIAL_PHONE);
        email.setText("Email : " + ent.getEmailentrp());
        FontImage.setMaterialIcon(email, FontImage.MATERIAL_EMAIL);
        adresse.setText("Adresse : " + ent.getAdresse());
        FontImage.setMaterialIcon(adresse, FontImage.MATERIAL_PERSON);
        description.setText("Description : " + ent.getDescription());
        FontImage.setMaterialIcon(description, FontImage.MATERIAL_DESCRIPTION);
        
        if(ent.getFacebook()== null)
        {facebook.setText("Facebook : " );}
        else
        {facebook.setText("Facebook : " + ent.getFacebook());}
        FontImage.setMaterialIcon(facebook, FontImage.MATERIAL_THUMB_UP);
        
        if(ent.getGooglePlus()== null)
        {googleplus.setText("Google Plus : " );}
        else
        {googleplus.setText("Google Plus : " + ent.getGooglePlus());}
        
        FontImage.setMaterialIcon(googleplus, FontImage.MATERIAL_PUBLIC);
        
         if(ent.getLinkedin()== null)
        {linkedin.setText("Linkedin : " );}
        else
        {linkedin.setText("Linkedin : " + ent.getLinkedin());}
        
        FontImage.setMaterialIcon(linkedin, FontImage.MATERIAL_ACCOUNT_BOX);
        if(ent.getTwitter()== null)
        {Twitter.setText("Twitter : " );}
        else
        {Twitter.setText("Twitter : " + ent.getTwitter());}
        
        FontImage.setMaterialIcon(Twitter, FontImage.MATERIAL_MODE_COMMENT);
        
        if(ent.getCodePostal()== null)
        {ville.setText("Ville : " );}
        else
        {ville.setText("Ville : " + ent.getCodePostal().getDelegation().getVille().getNom());}
        
        FontImage.setMaterialIcon(ville, FontImage.MATERIAL_MAP);
        
        if( ent.getCodePostal()== null)
        {code.setText("Delegation : " );}
        else
        {code.setText("Delegation : " + ent.getCodePostal().getDelegation().getNom());}
        
        FontImage.setMaterialIcon(deleg, FontImage.MATERIAL_DRAFTS);
        
        if(ent.getCodePostal()== null)
        {deleg.setText("Code Postale : " );}
        else
        {deleg.setText("Code Postale : " + ent.getCodePostal().getNom());}
        
        
        FontImage.setMaterialIcon(code, FontImage.MATERIAL_CODE);
        
        name.setUIID("text");
        nationalite.setUIID("text");
        telephone.setUIID("text");
        adresse.setUIID("text");
        description.setUIID("text");
        facebook.setUIID("text");
        googleplus.setUIID("text");
        email.setUIID("text");
        linkedin.setUIID("text");
        Twitter.setUIID("text");
        code.setUIID("text");
        deleg.setUIID("text");
        ville.setUIID("text");
        
        TableLayout fullNameLayout = new TableLayout(1, 3);
        TableLayout fulls = new TableLayout(1, 3);
        TableLayout fullNames = new TableLayout(1, 3);
         TableLayout fullLa = new TableLayout(1, 3);
         Container fullN = new Container(fullLa);
        Container fullName = new Container(fullNameLayout);
        fullName.add(fullNameLayout.createConstraint().widthPercentage(49), name).
                add(fullNameLayout.createConstraint().widthPercentage(1), createSeparator()).
                add(fullNameLayout.createConstraint().widthPercentage(50), nationalite);
        fullN.add(fullLa.createConstraint().widthPercentage(49), ville).
              add(fullLa.createConstraint().widthPercentage(1), createSeparator()).
                add(fullLa.createConstraint().widthPercentage(50), deleg);
        
        Container fullPhone = new Container(fullNames);
        fullPhone.add(fullNameLayout.createConstraint().widthPercentage(49), telephone).
                add(fullNameLayout.createConstraint().widthPercentage(1), createSeparator()).
                add(fullNameLayout.createConstraint().widthPercentage(50), adresse);
        Container full = new Container(fulls);
        fullPhone.add(fullNameLayout.createConstraint().widthPercentage(49), Twitter).
                add(fullNameLayout.createConstraint().widthPercentage(1), createSeparator()).
                add(fullNameLayout.createConstraint().widthPercentage(50), googleplus);

        Button southButton = new Button("Modifier", rightArrow);
        southButton.setTextPosition(Component.LEFT);
        southButton.setUIID("SouthButton");
        southButton.addActionListener(e -> {

            new UpdateProfile();
        });

        Container by = BoxLayout.encloseY(
                fullName,
                createSeparator(),
                email,
                createSeparator(),
                fullPhone,
                createSeparator(),
                linkedin,
                createSeparator(),
                facebook, 
                createSeparator(),
                full,
                createSeparator(),
                description,
                createSeparator(),
                fullN,
//                createSeparator(),
//                deleg,
                createSeparator(),
                code
        );
        by.setScrollableY(true);
        by.setUIID("tt");
        add(BorderLayout.NORTH, titleContainer).
                add(BorderLayout.SOUTH, southButton).
                add(BorderLayout.CENTER, by);

    }

    private Label createSeparator() {
        Label sep = new Label();
        sep.setUIID("Separator");
        // the separator line  is implemented in the theme using padding and background color, by default labels 
        // are hidden when they have no content, this method disables that behavior
        sep.setShowEvenIfBlank(true);
        return sep;
    }

    @Override
    protected boolean isCurrentProfile() {
        return true;
    }

}
