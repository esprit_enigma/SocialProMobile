/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import Controllers.EmailVerificationController;
import Controllers.InitProfileJoberController;
import Controllers.LoginController;
import Controllers.ProfileJoberController;
import Controllers.RegisterController;
import DAO.UserDAO;
import Models.User;
import com.codename1.components.ToastBar;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import static com.codename1.ui.Component.LEFT;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ASUS
 */
public class LoginForm extends Form {
    public MyApplication parent;
    static User currentUser;
    User u = new User();
    public LoginForm(Resources theme) {
      super(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER_ABSOLUTE));
        setUIID("LoginForm");
        Container welcome = FlowLayout.encloseCenter(
            new Label("Bienvenue", "BienvenuWhite")
//               // new Label("Jennifer", "WelcomeBlue")
       );
        
        getTitleArea().setUIID("Container");
         
        
       TextField login = new TextField("", "Email", 20, TextField.ANY) ;
        TextField password = new TextField("", "Password", 20, TextField.PASSWORD) ;
        password.setConstraint(TextField.PASSWORD);
       
            
        login.getAllStyles().setMargin(LEFT, 0);
        password.getAllStyles().setMargin(LEFT, 0);
        Label loginIcon = new Label("", "TextField");
        Label passwordIcon = new Label("", "TextField");
        loginIcon.getAllStyles().setMargin(RIGHT, 0);
        passwordIcon.getAllStyles().setMargin(RIGHT, 0);
        FontImage.setMaterialIcon(loginIcon, FontImage.MATERIAL_EMAIL, 3);
        FontImage.setMaterialIcon(passwordIcon, FontImage.MATERIAL_LOCK_OUTLINE, 3);
        Button loginButton = new Button("LOGIN");
        Button btnregister = new Button("REGISTER");
        loginButton.setUIID("LoginButton");
        btnregister.setUIID("LoginButton");
        loginButton.addActionListener(e -> {
            
            
             ConnectionRequest req = new ConnectionRequest();
            req.setUrl("http://localhost/PIMobile/src/PHP/login.php?username=" + login.getText() + "&password=" + password.getText() + "");
            req.addResponseListener(new ActionListener<NetworkEvent>() {
                @Override
                public void actionPerformed(NetworkEvent evt) {
                    byte[] data = (byte[]) evt.getMetaData();
                    String s = new String(data);
                    if (s.equals("jober")) {
                        currentUser = SearchUsersByUsername(login.getText());
                        ProfileJoberController pjc = new ProfileJoberController();
                        pjc.getF().show();
                    } else if (s.equals("entrep")) {
                        new UserDAO().findUser(login.getText());
                       // currentUser = SearchUsersByUsername(login.getText());
//                        ProfileEntrep pe = new ProfileEntrep(tfusername.getText());
//                        pe.getF().show();
                    } else if (s.equals("prejober")) {
                        currentUser = SearchUsersByUsername(login.getText());
                        InitProfileJoberController ipjc = new InitProfileJoberController();
                        ipjc.getF().show();
                    } else if (s.equals("preentrep")) {
                        new UserDAO().findUser(login.getText());
                        //currentUser = SearchUsersByUsername(login.getText());
//                        ProfileEntrep pe = new ProfileEntrep(tfusername.getText());
//                        pe.getF().show();
                    } else if (s.equals("notenabledjober")) {
                        currentUser = SearchUsersByUsername(login.getText());
                        EmailVerificationController evc = new EmailVerificationController(login.getText(), currentUser.getEmail(), "jober");
                        evc.getF().show();
                    } else if (s.equals("notenabledentrep")) {
                        currentUser = SearchUsersByUsername(login.getText());
                        EmailVerificationController evc = new EmailVerificationController(login.getText(), currentUser.getEmail(), "entrep");
                        evc.getF().show();
                    } else if (s.equals("not found")) {
                        //Dialog.show("Wrong Credentials", "Please check your username or password", "OK", "Cancel");
                         ToastBar.showMessage("Verifier Vos Données", FontImage.MATERIAL_INFO);
                    }
                }
            });
            NetworkManager.getInstance().addToQueueAndWait(req);
            
            
            
            
            
            
            
            
            
            
            
            
          
           // new UserDAO().findUser(login.getText());
            Toolbar.setGlobalToolbar(false);
            
            
            Toolbar.setGlobalToolbar(true);
        });
         btnregister.addActionListener(e -> {
            RegisterController r = new RegisterController();
            r.getF().show();
        });
        
        
//        Button createNewAccount = new Button("CREATE NEW ACCOUNT");
//        createNewAccount.setUIID("CreateNewAccountButton");
//        
        // We remove the extra space for low resolution devices so things fit better
        Label spaceLabel;
        if(!Display.getInstance().isTablet() && Display.getInstance().getDeviceDensity() < Display.DENSITY_VERY_HIGH) {
            spaceLabel = new Label();
        } else {
            spaceLabel = new Label(" ");
        }
        
        
        Container by = BoxLayout.encloseY(
                welcome,
               
                spaceLabel,
                BorderLayout.center(login).
                        add(BorderLayout.WEST, loginIcon),
                BorderLayout.center(password).
                        add(BorderLayout.WEST, passwordIcon),
                loginButton,
                        btnregister
//                createNewAccount
        );
        add(BorderLayout.CENTER, by);
        
        // for low res and landscape devices
        by.setScrollableY(false);
        by.setScrollVisible(false);
    }
    
    
    
     public static User getCurrentUser() {
        return currentUser;
    }

    public static void setCurrentUser(User currentUser) {
        LoginForm.currentUser = currentUser;
    }

    public User SearchUsersByUsername(String username) {

        ConnectionRequest cr = new ConnectionRequest();
        cr.setUrl("http://localhost/PIMobile/src/PHP/GetUser.php");
        cr.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ArrayList<User> l = getUser(new String(cr.getResponseData()));
                for (User k : l) {
                    if (k.getUsername().equals(username)) {
                        u = k;
                    }
                }
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(cr);
        return u;
    }

    public ArrayList<User> getUser(String json) {
        ArrayList<User> listusers = new ArrayList<>();
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> etudiants = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) etudiants.get("user");

            for (Map<String, Object> obj : list) {
                int id = Integer.parseInt(obj.get("id").toString());
                String usern = obj.get("username").toString();
                String email = obj.get("email").toString();
                String password = obj.get("password").toString();
                String role = obj.get("roles").toString();
                listusers.add(new User(id, usern, usern, email, email, password, role));
            }

        } catch (IOException ex) {
        }
        return listusers;

    }






}
