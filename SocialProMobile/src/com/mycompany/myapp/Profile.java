/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import DAO.EntrepriseDAO;
import Models.Entreprise;
import com.codename1.components.FloatingActionButton;
import com.codename1.io.Storage;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import static com.codename1.ui.Component.CENTER;
import com.codename1.ui.Container;
import com.codename1.ui.FontImage;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;

/**
 *
 * @author ASUS
 */
public class Profile extends SideMenu {

    public Profile() {
        this(Resources.getGlobalResources());
    }

    public Profile(Resources res) {
        super(BoxLayout.y());

        installSidemenu(res);

        Toolbar tb = getToolbar();
        tb.setUIID("tool");
        tb.setTitleCentered(true);
        Storage storage = Storage.getInstance();
        String id = (String) storage.readObject("id");
        System.out.println("uddddd" + id);

        System.out.println("ennnn" + new EntrepriseDAO().findEntreprise(id));
        Entreprise ent = new Entreprise();
        ent = new EntrepriseDAO().findEntreprise(id);

        Image profilePic = MyApplication.theme.getImage("user-picture.jpg");
        Image mask = MyApplication.theme.getImage("NN.png");
        profilePic = profilePic.fill(mask.getWidth(), mask.getHeight());
        Label profilePicLabel = new Label(profilePic, "ProfilePicTitle");
        profilePicLabel.setMask(mask.createMask());

//        Button menuButton = new Button("");
//        menuButton.setUIID("Title");
//        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
//        menuButton.addActionListener(e -> ((SideMenuBar)getToolbar().getMenuBar()).openMenu(null));
        Button b = new Button("Reseaux Sociaux");
        b.setUIID("RemainingTasks");
        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                new ReseauSociaux().show();
            }
        });
        Container remainingTasks = BoxLayout.encloseY(
                b
        );
        remainingTasks.setUIID("RemainingTasks");

        Button d = new Button("Localisation");
        d.setUIID("RemainingTasks");
        d.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                new Localisation().show();
            }
        });
        Container completedTasks = BoxLayout.encloseY(
                d
        );
        completedTasks.setUIID("RemainingTasks");

        Container titleCmp = BoxLayout.encloseY(
                // FlowLayout.encloseIn(menuButton),
                BorderLayout.centerAbsolute(
                        BoxLayout.encloseY(
                                new Label(ent.getNom(), "Title"),
                                new Label(ent.getNationalite(), "SubTitle")
                        )
                ).add(BorderLayout.WEST, profilePicLabel),
                GridLayout.encloseIn(2, remainingTasks, completedTasks)
        );
        
        FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_CREATE);
        fab.getAllStyles().setMarginUnit(Style.UNIT_TYPE_PIXELS);
       // fab.getAllStyles().setBgColor(333333);
        fab.getAllStyles().setMargin(BOTTOM, completedTasks.getPreferredH() - fab.getPreferredH() / 2);
        tb.setTitleComponent(fab.bindFabToContainer(titleCmp, CENTER, BOTTOM));
       
        fab.addActionListener(e -> {
            new InitProfile().show();
        });
        
        
        
    

    }

}
