/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import DAO.EntrepriseDAO;
import DAO.ProjetDAO;
import Models.Entreprise;
import Models.ProjetEntreprise;
import com.codename1.io.Storage;

import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Calendar;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import java.util.Date;

/**
 *
 * @author ASUS
 */
public class ProjetUnique extends Form{
    
    
    boolean onBack() {
        return true;
    }
    public ProjetUnique() {
        //maxresdefault.jpg
        setUIID("projet");
        Toolbar.setGlobalToolbar(true);
        getToolbar().setUIID("tool");
         getToolbar().setTitleComponent(
                FlowLayout.encloseCenterMiddle(
                        new Label("Details Projets", "Title")
                       
                )
        );
         Form previous = Display.getInstance().getCurrent();
         getToolbar().setBackCommand(" ", ee -> {
            if (onBack()) {
                previous.showBack();
            }
        });
      
         BorderLayout mainLayout = new BorderLayout();
        
        mainLayout.setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);
        setLayout(mainLayout);
        setTitle("Details Projets");
        setScrollable(true);
        
        TextField name = new TextField("", "Nom De Projet", 20, TextField.ANY);  
        FontImage.setMaterialIcon(name.getHintLabel(), FontImage.MATERIAL_SUBJECT);
        TextField client = new TextField("", "Client", 20, TextField.ANY);
        FontImage.setMaterialIcon(client.getHintLabel(), FontImage.MATERIAL_FACE);
        Calendar cal=new Calendar();
        TextArea description = new TextArea("",3, 5);
        description.setHint("Description");
        //FontImage.setMaterialIcon(description.getHintLabel(), FontImage.MATERIAL_SUBJECT);
        
         //String nom, String destination, String description, String dateAjout, Entreprise entreprise
         
         
         Button save = new Button("Enregistre");
         //save.setUIID("LoginButton");
         save.addActionListener(e -> {
              Storage storage=Storage.getInstance();
         String id=(String)storage.readObject("id");
         Entreprise ent=new Entreprise();
         ent= new EntrepriseDAO().findEntrepriseReseau(id);
         Date d = new Date();
         d=cal.getDate();
          Date  h = null;
        // L10NManager l10n = L10NManager.getInstance();
        // String db= l10n.formatDateShortStyle(d);
         SimpleDateFormat dc=new SimpleDateFormat("yyyy-MM-dd");
          
            String f = dc.format(d);
               System.out.println("dateeeeeeeeeeee"+f);
            
        

            
            //String nom, String adresse, String nationalite, int numTel, String description, String emailentrp, User userId
            ProjetEntreprise projet = new ProjetEntreprise(name.getText(),client.getText(), description.getText(), f, ent);
             System.out.println("projet unique "+projet);
            new ProjetDAO().addProjet(projet);
            new Projet(MyApplication.theme).show();
            //ToastBar.showMessage("Save pressed...", FontImage.MATERIAL_INFO);
          
        });
        
        //Container comps = Container();
        Container actionsC = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        //actionsC.setUIID("InputContainerBackground");
        actionsC.addComponent(name);
        actionsC.addComponent(client);
        actionsC.addComponent(description);
        actionsC.addComponent(cal);
        actionsC.addComponent(save);
        
        Container partageC = new Container(new FlowLayout(Component.CENTER));
       // partageC.addComponent(save);
         addComponent(BorderLayout.CENTER,actionsC);
        //addComponent(BorderLayout.CENTER,partageC);
//        save.addAct
    }
  
}
