/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import DAO.EntrepriseDAO;
import Models.Entreprise;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.MultiButton;
import com.codename1.components.ToastBar;
import com.codename1.io.Storage;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import static com.codename1.ui.Component.CENTER;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import com.codename1.ui.validation.RegexConstraint;
import com.codename1.ui.validation.Validator;

/**
 *
 * @author ASUS
 */
public class ReseauSociaux extends Form{
    
     Resources resourceObjectInstance;
    boolean onBack() {
        return true;
    }
    public ReseauSociaux() {
        //maxresdefault.jpg
        setUIID("background");
        Toolbar.setGlobalToolbar(true);
        getToolbar().setUIID("tool");
         getToolbar().setTitleComponent(
                FlowLayout.encloseCenterMiddle(
                        new Label("Mes Réseaux Sociaux", "Title")
                       
                )
        );
            Storage storage=Storage.getInstance();
            String id=(String)storage.readObject("id");
            System.out.println("uddddd"+id);
    
        System.out.println("ennnn"+ new EntrepriseDAO().findEntreprise(id));
        Entreprise ent=new Entreprise();
        ent= new EntrepriseDAO().findEntrepriseReseau(id);
        
         Form previous = Display.getInstance().getCurrent();
         getToolbar().setBackCommand(" ", ee -> {
            if (onBack()) {
                previous.showBack();
            }
        });
         BorderLayout mainLayout = new BorderLayout();
        mainLayout.setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);
        setLayout(mainLayout);
        setTitle("Mes Réseaux Sociaux");
        //getStyle().setBgImage(MyApplication.theme.getImage("Socialpro.png"));
        TextField name = new TextField("", "Facebook", 20, TextField.URL);
        
        FontImage.setMaterialIcon(name.getHintLabel(), FontImage.MATERIAL_FACE);
        name.setText(ent.getFacebook());
        TextField email = new TextField("", "Twitter", 20, TextField.URL);
        FontImage.setMaterialIcon(email.getHintLabel(), FontImage.MATERIAL_SUBJECT);
        email.setText(ent.getTwitter());
        TextField password = new TextField("", "Linkedin", 20, TextField.URL);
        FontImage.setMaterialIcon(password.getHintLabel(), FontImage.MATERIAL_LOCK);
        password.setText(ent.getLinkedin());
        TextField bio = new TextField("", "GooglePlus", 20,TextField.URL);
        FontImage.setMaterialIcon(bio.getHintLabel(), FontImage.MATERIAL_LIBRARY_BOOKS);
        bio.setText(ent.getGooglePlus());
        
        Validator v = new Validator();
        v.addConstraint(name, RegexConstraint.validURL("Ce champ doit être une URL valide"));
        v.addConstraint(email, RegexConstraint.validURL("Ce champ doit être une URL valide"));
        v.addConstraint(password, RegexConstraint.validURL("Ce champ doit être une URL valide"));
        v.addConstraint(bio, RegexConstraint.validURL("Ce champ doit être une URL valide"));
         Button save = new Button("Save");
         //save.setUIID("LoginButton");
         v.addSubmitButtons(save);
         save.addActionListener(e -> {
            if (name.getText().length() > 0 && email.getText().length() > 0 && password.getText().length() > 0 && bio.getText().length() > 0) {
            Entreprise entreprise = new Entreprise(name.getText(),email.getText(),email.getText(),bio.getText());
            new EntrepriseDAO().updateEntreprisereseau(entreprise);
            new ProfileAffichage(resourceObjectInstance).show();
            } else {
                ToastBar.showMessage("Verifier Les URLS", FontImage.MATERIAL_BLOCK);
            };
          
        });
        
        //Container comps = Container();
        Container actionsC = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        //actionsC.setUIID("InputContainerBackground");
        actionsC.addComponent(name);
        actionsC.addComponent(email);
        actionsC.addComponent(password);
        actionsC.addComponent(bio);
        actionsC.addComponent(save);
        
        Container partageC = new Container(new FlowLayout(Component.CENTER));
       // partageC.addComponent(save);
         addComponent(BorderLayout.CENTER,actionsC);
        //addComponent(BorderLayout.CENTER,partageC);
//        save.addActionListener(this);
        
    
    }
    
    
    
        
    
    
}
