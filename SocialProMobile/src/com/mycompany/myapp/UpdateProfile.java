/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import DAO.EntrepriseDAO;
import DAO.UserDAO;
import Models.Entreprise;
import com.codename1.capture.Capture;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.ToastBar;
import com.codename1.io.Log;
import com.codename1.io.MultipartRequest;
import com.codename1.io.NetworkManager;
import com.codename1.io.Storage;
import com.codename1.io.services.ImageDownloadService;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.animations.CommonTransitions;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.table.TableLayout;
import com.codename1.ui.util.Resources;
import com.codename1.ui.validation.LengthConstraint;
import com.codename1.ui.validation.RegexConstraint;
import com.codename1.ui.validation.Validator;
import java.io.IOException;

/**
 *
 * @author ASUS
 */
public class UpdateProfile extends Form {

//    public MyApplication main;
//    private Command quitter;
//    private Command retour;
    Resources resourceObjectInstance;

    public UpdateProfile(Resources resourceObjectInstance) {
        super(BoxLayout.y());
        //installSidemenu(resourceObjectInstance);
    }

    public UpdateProfile() {
        super(BoxLayout.y());
        Toolbar.setGlobalToolbar(true);

        //this.main = main;
        //LoginForm d;
//        MyApplication d = new MyApplication();
        Form previous = Display.getInstance().getCurrent();
        Form f = new Form("Modifier Profile", new BorderLayout());
        f.getToolbar().setUIID("tool");
        f.add(BorderLayout.CENTER, createDemo(f));
        f.getToolbar().setBackCommand(" ", ee -> {
            if (onBack()) {
                previous.showBack();
            }
        });

        f.show();

    }

    private void addComps(Form parent, Container cnt, Component... cmps) {
        if (Display.getInstance().isTablet() || !Display.getInstance().isPortrait()) {
            TableLayout tl = new TableLayout(cmps.length / 2, 2);
            cnt.setLayout(tl);
            tl.setGrowHorizontally(true);
            for (Component c : cmps) {
                if (c instanceof Container) {
                    cnt.add(tl.createConstraint().horizontalSpan(2), c);
                } else {
                    cnt.add(c);
                }
            }
        } else {
            cnt.setLayout(BoxLayout.y());
            for (Component c : cmps) {
                cnt.add(c);
            }
        }
        if (cnt.getClientProperty("bound") == null) {
            cnt.putClientProperty("bound", "true");
            if (!Display.getInstance().isTablet()) {
                parent.addOrientationListener((e) -> {
                    Display.getInstance().callSerially(() -> {
                        cnt.removeAll();
                        addComps(parent, cnt, cmps);
                        cnt.animateLayout(800);
                    });
                });
            }
        }
    }

    public Container createDemo(Form parent) {

        Storage storage = Storage.getInstance();
        String id = (String) storage.readObject("id");
        System.out.println("uddddd" + id);

        Entreprise ent = new Entreprise();
        ent = new EntrepriseDAO().findEntreprise(id);
        System.out.println("laaaaaaaaaaaaaaaa" + new EntrepriseDAO().findEntreprise(id));
        TextField name = new TextField("", "Nom", 20, TextField.ANY);
        FontImage.setMaterialIcon(name.getHintLabel(), FontImage.MATERIAL_PERSON);
        name.setText(ent.getNom());
        TextField nationnalite = new TextField("", "Nationalité", 20, TextField.ANY);
        FontImage.setMaterialIcon(nationnalite.getHintLabel(), FontImage.MATERIAL_PLACE);
        nationnalite.setText(ent.getNationalite());
        TextField email = new TextField("", "E-mail", 20, TextField.EMAILADDR);
        FontImage.setMaterialIcon(email.getHintLabel(), FontImage.MATERIAL_EMAIL);
        email.setConstraint(TextField.EMAILADDR);
        email.setText(ent.getEmailentrp());
        TextField adresse = new TextField("", "Adresse", 20, TextField.ANY);
        FontImage.setMaterialIcon(adresse.getHintLabel(), FontImage.MATERIAL_LOCATION_CITY);
        adresse.setText(ent.getAdresse());
        TextField tel = new TextField("", "Telephone", 20, TextField.NUMERIC);
        FontImage.setMaterialIcon(tel.getHintLabel(), FontImage.MATERIAL_PHONE);
        tel.setText(String.valueOf(ent.getNumTel()));
        TextField description = new TextField("", "Description", 2, 20);
        FontImage.setMaterialIcon(description.getHintLabel(), FontImage.MATERIAL_LIBRARY_BOOKS);
        description.setText(ent.getDescription());
        description.setSingleLineTextArea(false);
        //Entreprise entrep = new Entreprise(name.getText(),adresse.getText(),nationnalite.getText(),Integer.parseInt(tel.getText()),description.getText(),email.getText());
        String phoneRegex="^[0-9]{8}$";
        Validator v = new Validator();
        v.addConstraint(email, RegexConstraint.validEmail("Ce champ doit être un e-mail valide")).
          addConstraint(tel, new RegexConstraint(phoneRegex, "Ce champ doit Avoir 8 numeros")) ;

        Label b = new Label("Nom", "InputContainerLabel");
        b.getUnselectedStyle().setMargin(Component.TOP, 100);
        Container comps = new Container();
        addComps(parent, comps,
                b,
                name,
                new Label("Nationalité", "InputContainerLabel"),
                nationnalite,
                new Label("E-Mail", "InputContainerLabel"),
                email,
                new Label("Adresse", "InputContainerLabel"),
                adresse,
                new Label("Tlephone", "InputContainerLabel"),
                tel,
                new Label("Description", "InputContainerLabel"),
                description);
        comps.setScrollableY(true);
        comps.setUIID("PaddedContainer");

        Container content = BorderLayout.center(comps);
        String nameImage = (String) Storage.getInstance().readObject("nameuser");
        Button save = new Button("Modifier");
        save.setUIID("InputAvatarImage");
        content.add(BorderLayout.SOUTH, save);
        v.addSubmitButtons(save);

        save.addActionListener(e -> {
            if (name.getText().length() > 0 && nationnalite.getText().length() > 0 && description.getText().length() > 0) {
                if (email.getText().length() > 0) {
                    if (tel.getText().length() > 0) {
           //email.getText().length()>0 && tel.getText().length()>0 &&
                        //String nom, String adresse, String nationalite, int numTel, String description, String emailentrp, User userId
                        Entreprise entreprise = new Entreprise(name.getText(), adresse.getText(), nationnalite.getText(), Integer.parseInt(tel.getText()), description.getText(), email.getText());
                        new EntrepriseDAO().updateEntreprise(entreprise);
                        //ToastBar.showMessage("Save pressed...", FontImage.MATERIAL_INFO);
                        System.out.println("rouriiiiiiiii" + entreprise);
                        new ProfileAffichage(resourceObjectInstance).show();
                    } else {
                        ToastBar.showMessage("Veuillez Entrer un numéro de Telephone valide", FontImage.MATERIAL_BLOCK);
                    }
                } else {
                    ToastBar.showMessage("Veuillez Entrer un e-mail valide", FontImage.MATERIAL_BLOCK);
                }
            } else {
                ToastBar.showMessage("Veuillez Remplir Tous les Champs", FontImage.MATERIAL_BLOCK);
            }

        });

        content.setUIID("InputContainerForeground");


        Button avatar = new Button("");
        avatar.setUIID("InputAvatar");
        
//        Image defaultAvatar = FontImage.createMaterial(FontImage.MATERIAL_CAMERA, "InputAvatarImage", 8);
       Image circleMaskImage = MyApplication.theme.getImage("circle.png");
//      defaultAvatar = defaultAvatar.scaled(circleMaskImage.getWidth(), circleMaskImage.getHeight());
//      defaultAvatar = ((FontImage) defaultAvatar).toEncodedImage();
       Object circleMask = circleMaskImage.createMask();
        //defaultAvatar = defaultAvatar.applyMask(circleMask);
        
        String na = (String) Storage.getInstance().readObject("nameuser");

        Label image = new Label();

        ImageDownloadService.createImageToStorage("http://localhost/SocialProScripts/ressources/" + na + ".jpg", avatar, nameImage,new Dimension(circleMaskImage.getWidth(), circleMaskImage.getHeight()) );
//        image.setMask(circleMask);
//        avatar.setIcon(defaultAvatar);
        
        final String url = "http://localhost/SocialProScripts/upload.php";
        avatar.addActionListener(e -> {
            if (Dialog.show(" Galerie", "Voulez Vous Utilisez  la galerie pour le logo?", "cancel", "Galerie")) {
                String pic = Capture.capturePhoto();
                if (pic != null) {
                    try {
                        Image img = Image.createImage(pic).fill(circleMaskImage.getWidth(), circleMaskImage.getHeight());
                        avatar.setIcon(img.applyMask(circleMask));
                        System.out.println("ccccc");
                    } catch (IOException err) {
                        ToastBar.showErrorMessage("An error occured while loading the image: " + err);
                        Log.e(err);
                    }
                }
            } else {
                Display.getInstance().openGallery(ee -> {
                    if (ee.getSource() != null) {
                        System.out.println("source" + ee.getSource());
                        try {
                            System.out.println((String) ee.getSource());

                            Image img = Image.createImage((String) ee.getSource()).fill(circleMaskImage.getWidth(), circleMaskImage.getHeight());
                            avatar.setIcon(img.applyMask(circleMask));

                            MultipartRequest cr = new MultipartRequest();
                            String filePath = ee.getSource().toString();
                            cr.setUrl(url);
                            cr.setPost(true);
                            String mime = "image/jpeg";
                            try {
                                cr.addData("file", filePath, mime);

                            } catch (IOException ex) {
                                Dialog.show("Erreur", ex.getMessage(), "Ok", null);
                            }
                            cr.setFilename("file", nameImage + ".jpg");//any unique name you want
                            Storage.getInstance().deleteStorageFile(nameImage);
                            InfiniteProgress prog = new InfiniteProgress();
                            Dialog dlg = prog.showInifiniteBlocking();
                            cr.setDisposeOnCompletion(dlg);
                            NetworkManager.getInstance().addToQueueAndWait(cr);

                        } catch (IOException err) {
                            ToastBar.showErrorMessage("Une Erreur se produite lors de Chargement de L'image : " + err);
                            Log.e(err);
                        }
                    }
                }, Display.GALLERY_IMAGE);
            }

        });

        Container actualContent = LayeredLayout.encloseIn(content,
                FlowLayout.encloseCenter(avatar));

        Container input;
        if (!Display.getInstance().isTablet()) {
            Label placeholder = new Label(" ");

            Component.setSameHeight(actualContent, placeholder);
            Component.setSameWidth(actualContent, placeholder);

            input = BorderLayout.center(placeholder);

            parent.addShowListener(e -> {
                if (placeholder.getParent() != null) {
                    input.replace(placeholder, actualContent, CommonTransitions.createFade(1500));
                }
            });
        } else {
            input = BorderLayout.center(actualContent);
        }
        input.setUIID("InputContainerBackground");

        return input;
    }

    boolean onBack() {
        return true;
    }
}
