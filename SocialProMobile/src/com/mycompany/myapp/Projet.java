/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import DAO.ProjetDAO;
import Models.ProjetEntreprise;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.MultiButton;
import com.codename1.components.ToastBar;
import com.codename1.io.Storage;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Image;
import com.codename1.ui.Label;

import com.codename1.ui.SwipeableContainer;
import com.codename1.ui.events.ScrollListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.table.TableLayout;
import com.codename1.ui.util.Resources;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class Projet extends SideMenu {

    private long lastScroll;
    private boolean messageShown;
    private Object circleMask;
    private int circleMaskWidth;
    private int circleMaskHeight;
    private Font letterFont;
    private boolean finishedLoading;
    Image circleImage;

    public Projet() {
        this(Resources.getGlobalResources());
    }

    public Projet(com.codename1.ui.util.Resources resourceObjectInstance) {

        super(BoxLayout.y());
          //initGuiBuilderComponents(resourceObjectInstance);
        setLayout(new LayeredLayout());
    getToolbar().setUIID("tool");
         getToolbar().setTitleComponent(
                FlowLayout.encloseCenterMiddle(
                        new Label("Projets", "Title")
                       
                )
        );
       
        installSidemenu(resourceObjectInstance);

        Storage storage = Storage.getInstance();
        String id = (String) storage.readObject("idEntreprise");
        System.out.println("uddddd" + id);

        circleImage = MyApplication.theme.getImage("adminp.png");
        //circleLineImage = MyApplication.theme.getImage("circle-line.png");
        addPointerDraggedListener(e -> lastScroll = System.currentTimeMillis());
        addShowListener(e -> {
            if (!messageShown) {
                messageShown = true;
                ToastBar.showMessage("Faites glisser le Projet des deux côtés pour exposer des options supplémentaires", FontImage.MATERIAL_COMPARE_ARROWS, 2000);
            }
        });

        circleMask = circleImage.createMask();
        circleMaskWidth = circleImage.getWidth();
        circleMaskHeight = circleImage.getHeight();
        letterFont = Font.createTrueTypeFont("native:MainThin", "native:MainThin");
        letterFont = letterFont.derive(circleMaskHeight - circleMaskHeight / 3, Font.STYLE_PLAIN);
       
        final Container contactsDemo = new Container(BoxLayout.y());
        contactsDemo.setScrollableY(true);
        contactsDemo.add(FlowLayout.encloseCenterMiddle(new InfiniteProgress()));

        Display.getInstance().scheduleBackgroundTask(() -> {
           List<ProjetEntreprise> projects = new ArrayList<ProjetEntreprise>();
            projects = new ProjetDAO().findProjet(id);
            System.out.println("prr" + projects);

            List<ProjetEntreprise> contacts = projects;

            Display.getInstance().callSerially(() -> {
                contactsDemo.removeAll();
                for (ProjetEntreprise c : contacts) {
                    String dname = c.getNom();
                    String dclient = c.getDestination();
                    String des = c.getDescription();
                    if (dname == null || dname.length() == 0) {
                        continue;
                    }
                    MultiButton mb = new MultiButton(dname);
//                        mb.add(circleImage);
                    mb.setIcon(circleImage);
                    mb.setTextLine2(dclient);
                    mb.setTextLine3(des);
                    mb.setIconUIID("ContactIcon");

                    // we need this for the SwipableContainer below
                    mb.getAllStyles().setBgTransparency(255);

                    Button delete = new Button();
                    delete.setUIID("SwipeableContainerButton");
                    FontImage.setMaterialIcon(delete, FontImage.MATERIAL_DELETE, 8);

                    Button info = new Button();
                    info.setUIID("SwipeableContainerInfoButton");
                    FontImage.setMaterialIcon(info, FontImage.MATERIAL_EDIT, 8);
                    info.addActionListener(e -> {
                        System.out.println("idprofff"+c.getId());
                         Storage.getInstance().writeObject("idproj", c.getId());
                      new ProjetUpdate().show();

                    });

                    Container options;
                    if (c.getNom() != null) {

                        options = GridLayout.encloseIn(2, info);
                    } else {
                        options = GridLayout.encloseIn(2, info);
                    }

                    SwipeableContainer sc = new SwipeableContainer(
                            options,
                            GridLayout.encloseIn(1, delete),
                            mb);
                    contactsDemo.add(sc);
                    sc.setUIID("conline");
                    sc.addSwipeOpenListener(e -> {
                        // auto fold the swipe when we go back to scrolling
                        contactsDemo.addScrollListener(new ScrollListener() {
                            int initial = -1;

                            @Override
                            public void scrollChanged(int scrollX, int scrollY, int oldscrollX, int oldscrollY) {
                                // scrolling is very sensitive on devices...
                                if (initial < 0) {
                                    initial = scrollY;
                                }
                                lastScroll = System.currentTimeMillis();
                                if (Math.abs(scrollY - initial) > mb.getHeight() / 2) {
                                    if (sc.getParent() != null) {
                                        sc.close();
                                    }
                                    contactsDemo.removeScrollListener(this);
                                }
                            }
                        });
                    });

              delete.addActionListener(e -> {
                        if(Dialog.show("Delete", "Vous êtes sure?\nCela va supprimer ce projet en permanence!", "Supprimer", "Cancel")) {
                            // can happen in the case of got() contacts
                            if(String.valueOf(c.getId())!= null) {
                                
                            new ProjetDAO().removeBook(String.valueOf(c.getId()));
                                Display.getInstance().deleteContact(String.valueOf(c.getId()));
                            }
                        sc.remove();
                        contactsDemo.animateLayout(800);
                        }
                });

                }
                contactsDemo.revalidate();

                finishedLoading = true;
            });
        });
        
        FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ADD);
        fab.addActionListener(e -> {
             new ProjetUnique().show();
            //ToastBar.showMessage("Floating action button pressed...", FontImage.MATERIAL_INFO);
        });
//    FlowLayout flow = new FlowLayout(Component.RIGHT);
//    flow.setValign(Component.BOTTOM);
//    Container conUpper = new Container();
//    //conUpper.addComponent(fab);
//    conUpper.setScrollable(false);
//
//        conUpper = fab.bindFabToContainer(contactsDemo);
//        //add(BorderLayout.CENTER,contactsDemo);
//        //addComponent(BorderLayout.CENTER,contactsDemo);
//        addComponent(conUpper);
//        //setScrollable(false);
//              //addComponent(c); marwasalem08@gmail.com
FlowLayout flow = new FlowLayout(Component.RIGHT);
    
    flow.setValign(Component.BOTTOM);
    Container conUpper = new Container(flow);
    //conUpper.addComponent(fab);
     conUpper = fab.bindFabToContainer(contactsDemo);
    conUpper.setScrollable(false);
getLayeredPane().addComponent(conUpper);
getLayeredPane().setLayout(new LayeredLayout());

    }

    @Override
    protected boolean isCurrentProjet() {
        return true;
    }

}
