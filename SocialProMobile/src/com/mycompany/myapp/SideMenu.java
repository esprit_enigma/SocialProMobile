/*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package com.mycompany.myapp;

import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.Layout;
import com.codename1.ui.util.Resources;

/**
 * Utility methods common to forms e.g. for binding the side menu
 *
 * @author Shai Almog
 */
public class SideMenu extends Form {

    public SideMenu(Layout content) {
        super(content);
    }
    
    public void installSidemenu(Resources res) {
        Image selection = MyApplication.theme.getImage("selection-in-sidemenu.png");
        
        Image profileImage = null;
        if(isCurrentProfile()) profileImage = selection;

        Image trendingImage = null;
        if(isCurrentActivite()) trendingImage = selection;
        
        Image calendarImage = null;
        if(isCurrentStatsProjets()) calendarImage = selection;
        
        Image statsImage = null;
        if(isCurrentProjet()) statsImage = selection;
        
        Button inboxButton = new Button("Profile", profileImage);
        inboxButton.setUIID("SideCommand");
        inboxButton.getAllStyles().setPaddingBottom(0);
        Container inbox = FlowLayout.encloseMiddle(inboxButton, 
                new Label("", "SideCommandNumber"));
        inbox.setLeadComponent(inboxButton);
        inbox.setUIID("SideCommand");
        inboxButton.addActionListener(e -> new ProfileAffichage().show());
        getToolbar().addComponentToSideMenu(inbox);
        
        getToolbar().addCommandToSideMenu("Projet", statsImage, e -> new Projet(res).show());
        getToolbar().addCommandToSideMenu("Stats De Mes Projetss", calendarImage, e -> new StatsProjets(res).show());
        //getToolbar().addCommandToSideMenu("Profile", profileImage, e -> new Profile(res).show())
       
         getToolbar().addCommandToSideMenu("Activite", trendingImage, e -> new ActiviteEnterprise(res).show());
        getToolbar().addMaterialCommandToSideMenu("Quitter", FontImage.MATERIAL_EXIT_TO_APP,  e -> new LoginForm(res).show());
        
        
        // spacer
        getToolbar().addComponentToSideMenu(new Label(" ", "SideCommand"));
        getToolbar().addComponentToSideMenu(new Label("Merci Pour Choisir", "SideCommandNoPad"));
        getToolbar().addComponentToSideMenu(new Label(MyApplication.theme.getImage("Logoma.png"), "Container"));
        
        
    }

        
    protected boolean isCurrentProfile() {
        return false;
    }
    
    protected boolean isCurrentActivite() {
        return false;
    }

    protected boolean isCurrentProjet() {
        return false;
    }

    protected boolean isCurrentStatsProjets() {
        return false;
    }
}
