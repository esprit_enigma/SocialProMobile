/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import DAO.ActiviteDAO;
import DAO.CodePostalDao;
import DAO.EntrepriseDAO;
import DAO.ProjetDAO;
import Models.AvoirActivite;
import Models.Categorie;
import Models.Entreprise;
import Models.ProjetEntreprise;
import Models.Activite;
import com.codename1.io.Storage;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Calendar;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.list.DefaultListModel;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author ASUS
 */
public class ActiviteUnique extends Form{
    
    Categorie v=new Categorie();
    Activite activi = new Activite();
    boolean onBack() {
        return true;
    }
    public ActiviteUnique() {
        //maxresdefault.jpg
        setUIID("projet");
        Toolbar.setGlobalToolbar(true);
        getToolbar().setUIID("tool");
         getToolbar().setTitleComponent(
                FlowLayout.encloseCenterMiddle(
                        new Label("Details Activite", "Title")
                       
                )
        );
         Form previous = Display.getInstance().getCurrent();
         getToolbar().setBackCommand(" ", ee -> {
            if (onBack()) {
                previous.showBack();
            }
        });
      
         BorderLayout mainLayout = new BorderLayout();
        
        mainLayout.setCenterBehavior(BorderLayout.CENTER_BEHAVIOR_CENTER);
        setLayout(mainLayout);
        setTitle("Details Activite");
        setScrollable(true);
        
        TextField name = new TextField("", "Nom De L'activité", 20, TextField.ANY);  
        FontImage.setMaterialIcon(name.getHintLabel(), FontImage.MATERIAL_UPDATE);
        ComboBox choix=new ComboBox();
        choix.setName("Categorie");
        //choix.addItem(" __choisissez une Categorie__");
      
        TextArea description = new TextArea("",3, 5);
        description.setHint("Description");
        //FontImage.setMaterialIcon(description.getHintLabel(), FontImage.MATERIAL_SUBJECT);
        
         //String nom, String destination, String description, String dateAjout, Entreprise entreprise
         
               ArrayList<Categorie>villes=new ActiviteDAO().getAll();
              
      DefaultListModel<String>c=new DefaultListModel<>();
      for(Categorie v:villes)
      { c.addItem(v.getNom());}
      
       choix.setModel(c);
       
        //Categorie v=(Categorie)choix.getSelectedItem();
//        Categorie v=new ActiviteDAO().findCategorieByNom((String)choix.getSelectedItem());
//                 System.out.println("villese"+v);
        
       choix.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                  Categorie c=new ActiviteDAO().findCategorieByNom((String)choix.getSelectedItem());
                 System.out.println("villese"+c);
                 v=c;
            }});
       
         Button save = new Button("Enregistre");
         save.setUIID("LoginButton");
         save.addActionListener(e -> {
              Storage storage=Storage.getInstance();
         String id=(String)storage.readObject("id");
         Entreprise ent=new Entreprise();
         ent= new EntrepriseDAO().findEntrepriseReseau(id);
     
//        
            System.out.println("Categorieeeeeeeeeeeee"+v);
            //Activite activ = new Activite(v, name.getText());
             activi.setCategorie(v);
             activi.setNom(name.getText());
          System.out.println("activitééééééééééééé"+activi);
            new ActiviteDAO().addActivite(activi);
              Activite activii= new ActiviteDAO().findActByNom(name.getText());
            
          
             System.out.println("activitééééééééééééé"+activii);
            //String nom, String adresse, String nationalite, int numTel, String description, String emailentrp, User userId
            
            AvoirActivite act = new AvoirActivite(description.getText(),activii);
            System.out.println("Avoirractivitééééééééééééé"+act); 
             new ActiviteDAO().addAvoirActivite(act);
             new ActiviteEnterprise(MyApplication.theme).show();
            //ProjetEntreprise projet = new ProjetEntreprise(name.getText(),client.getText(), description.getText(), f, ent);
             //System.out.println("projet unique "+projet);
//            new ProjetDAO().addProjet(projet);
//            new Projet(MyApplication.theme).show();
            //ToastBar.showMessage("Save pressed...", FontImage.MATERIAL_INFO);
          
        });
        
        //Container comps = Container();
        Container actionsC = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        //actionsC.setUIID("InputContainerBackground");
        actionsC.addComponent(name);
        actionsC.addComponent(choix);
        actionsC.addComponent(description);
        
        actionsC.addComponent(save);
        
        Container partageC = new Container(new FlowLayout(Component.CENTER));
       // partageC.addComponent(save);
         addComponent(BorderLayout.CENTER,actionsC);
        //addComponent(BorderLayout.CENTER,partageC);
//        save.addAct
    }
  
}
