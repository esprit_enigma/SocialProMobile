/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import DAO.ProjetDAO;
import Models.Donneschart;
import com.codename1.charts.ChartComponent;
import com.codename1.charts.models.CategorySeries;
import com.codename1.charts.renderers.DefaultRenderer;
import com.codename1.charts.renderers.SimpleSeriesRenderer;
import com.codename1.charts.util.ColorUtil;
import com.codename1.charts.views.PieChart;
import com.codename1.io.Storage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.util.Resources;
import java.util.List;


/**
 *
 * @author ASUS
 */
public class StatsProjets extends SideMenu {
String k;
   
       public StatsProjets() {
         this(Resources.getGlobalResources());
    }
    

    public StatsProjets(Resources resourceObjectInstance) {
       
        super(BoxLayout.y());
          //initGuiBuilderComponents(resourceObjectInstance);
         setUIID("stat");
        getToolbar().setUIID("tool");
        getToolbar().setTitleComponent(
                FlowLayout.encloseCenterMiddle(
                        new Label("Statistiques", "Title")
                       
                )
        );
         installSidemenu(resourceObjectInstance);
       Form f = new Form();
       f=createPieChartForm();
       f.show();
   
   }
    /**

     * Creates a renderer for the specified colors.

     */
 
    private DefaultRenderer buildCategoryRenderer(int[] colors) {

        DefaultRenderer renderer = new DefaultRenderer();

        renderer.setLabelsTextSize(25);

        renderer.setLegendTextSize(25);

        renderer.setMargins(new int[]{20, 30, 15, 0});

        for (int color : colors) {

            SimpleSeriesRenderer r = new SimpleSeriesRenderer();

            r.setColor(color);
           

            renderer.addSeriesRenderer(r);

        }

        return renderer;

    }



    /**

     * Builds a category series using the provided values.

     *

     * 

     * @param t the value

     * @return the category series

     */

    protected CategorySeries buildCategoryDataset(List<Donneschart> t) {

       CategorySeries series = new CategorySeries("Statistique");

      

        for (Donneschart value : t) {
             //k=value.getYear();
               System.out.println(value.getYear());
               System.out.println(value.getNbr());

            series.add("Année "+value.getYear(), value.getNbr());
            

        }



        return series;

    }



    public Form createPieChartForm() {



        // Generate the values
        Storage storage = Storage.getInstance();
        String id = (String) storage.readObject("idEntreprise");

       List<Donneschart> values = new ProjetDAO().getpie(id);
    


        // Set up the renderer 255, 156, 177

        int[] colors = new int[]{ColorUtil.rgb(207, 78, 57), ColorUtil.rgb(31, 157, 192), ColorUtil.rgb(255,156 ,177), ColorUtil.YELLOW, ColorUtil.CYAN};

        DefaultRenderer renderer = buildCategoryRenderer(colors);

        renderer.setZoomButtonsVisible(true);

        renderer.setZoomEnabled(true);

        renderer.setChartTitleTextSize(30);

        renderer.setDisplayValues(true);

        renderer.setShowLabels(true);
        //renderer.setLabelsColor();

        SimpleSeriesRenderer r = renderer.getSeriesRendererAt(0);

        r.setGradientEnabled(true);

        r.setGradientStart(0, ColorUtil.rgb(207, 78, 57));

        r.setGradientStop(0, ColorUtil.YELLOW);

        r.setHighlighted(true);



        // Create the chart ... pass the values and renderer to the chart object.

        PieChart chart = new PieChart(buildCategoryDataset(values), renderer);



        // Wrap the chart in a Component so we can add it to a form

        ChartComponent c = new ChartComponent(chart);
        setLayout(new BorderLayout());

         addComponent(BorderLayout.CENTER, c);
           return this;
    }
  

        

       
    
     @Override
    protected boolean isCurrentStatsProjets() {
        return true;
    }
    
    
}
