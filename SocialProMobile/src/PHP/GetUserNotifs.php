<?php
require_once ('connect.php');

$req = "SELECT * FROM notification";
$result = $conn->query($req);
$json = new SimpleXMLElement('<xml/>');
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $mydata = $json->addChild('notif');
        $mydata->addChild('id',$row['id']);
		$mydata->addChild('user_id',$row['user_id']);
        $mydata->addChild('text',$row['text']);
		$mydata->addChild('state',$row['state']);
         }
} else {
    echo "0 results";
}

echo( json_encode ($json));
?>