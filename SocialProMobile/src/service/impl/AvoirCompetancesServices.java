/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import Models.Avoir_Competance;
import Models.Competance;
import Models.Jober;
import com.codename1.components.ToastBar;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Dialog;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import service.interfaces.ICompetance;

/**
 *
 * @author oudayblouza
 */
public class AvoirCompetancesServices implements ICompetance {

    List<Avoir_Competance> listCompetance = new ArrayList<>();
    Avoir_Competance sp = new Avoir_Competance();

    @Override
    public void add(Avoir_Competance comp) {
        ConnectionRequest req = new ConnectionRequest();
        req.setUrl("http://localhost/PIMobile/src/PHP/Avoir_Competance/Insert_ac.php?profil=" + comp.getJober().getId() + "&competance=" + comp.getCompetance().getId() + "&niveau=" + comp.getNiveau() + "&dateAjout=" + comp.getDateAjout() + "");

        req.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {

                byte[] data = (byte[]) evt.getMetaData();
                String s = new String(data);
                System.out.println(s);
                if (s.equals("success")) {
                    Dialog.show("Confirmation", "ajout ok", "Ok", null);
                } else {
                    Dialog.show("Erreur", "erreur", "Ok", null);
                }
            }
        });

        NetworkManager.getInstance().addToQueue(req);
    }

    @Override
    public void delete(int id) {
    }

    @Override
    public void update(Avoir_Competance comp) {
        ConnectionRequest req = new ConnectionRequest();
        System.out.println(comp.getNiveau() + "test");
        System.out.println(comp.getId());
        req.setUrl("http://localhost/PIMobile/src/PHP/Avoir_Competance/Update_ac.php?id=" + comp.getId() + "&niveau=" + comp.getNiveau() + "");

        req.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {

                byte[] data = (byte[]) evt.getMetaData();
                String s = new String(data);
                System.out.println(s);
                if (s.equals("success")) {
                    Dialog.show("Confirmation", "update ok", "Ok", null);
                } else {
                    Dialog.show("Erreur", "erreur", "Ok", null);
                }
            }
        });

        NetworkManager.getInstance().addToQueue(req);
    }

    @Override
    public List<Avoir_Competance> findbyJober(Jober j) {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/Avoir_Competance/select_AC.php?jober=" + j.getId() + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {
                try {
                    try {
                        listCompetance = getListAvoirComp(new String(con.getResponseData()));
                    } catch (Exception e) {
                        Avoir_Competance sp = getOneAvoirComp(new String(con.getResponseData()));
                        listCompetance.add(sp);
                    }
                } catch (Exception e1) {
                    ToastBar.showErrorMessage("Aucune Experience");
                }

            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);

        return listCompetance;

    }

    @Override
    public Avoir_Competance findbyJoberComp(Jober j, Competance c) {
        ConnectionRequest con = new ConnectionRequest();

        con.setUrl("http://localhost/PIMobile/src/PHP/Avoir_Competance/select_AC_J_C.php?jober=" + j.getId() + "&competance=" + c.getId() + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {
                try {
                    Avoir_Competance sp = getOneAvoirComp(new String(con.getResponseData()));
                } catch (Exception e) {
                    System.out.println("introuvable");
                }
            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);
        return sp;
    }

    public ArrayList<Avoir_Competance> getListAvoirComp(String json) {
        ArrayList<Avoir_Competance> listComp = new ArrayList<Avoir_Competance>();
        try {

            JSONParser j = new JSONParser();
            Map<String, Object> competances = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) competances.get("competance");

            for (Map<String, Object> obj : list) {
                Avoir_Competance e = new Avoir_Competance();
                e.setId(Integer.parseInt(obj.get("id").toString()));

                int joberid = Integer.parseInt(obj.get("profil").toString());
                JoberService js = new JoberService();
                Jober j1 = js.findById(joberid);
                e.setJober(j1);

                int competanceid = Integer.parseInt(obj.get("competance").toString());
                CompetanceService cs = new CompetanceService();
                Competance c = cs.findById(competanceid);
                e.setCompetance(c);

                e.setNiveau(Integer.parseInt(obj.get("niveau").toString()));

                listComp.add(e);

            }

        } catch (IOException ex) {

//            Dialog.show("Erreur", "Pas de specialite pour cette Nature", "Ok", null);
        }

        return listComp;

    }

    public Avoir_Competance getOneAvoirComp(String json) {
        Avoir_Competance e = new Avoir_Competance();
        try {

            JSONParser j = new JSONParser();
            Map<String, Object> competances = j.parseJSON(new CharArrayReader(json.toCharArray()));
            Map<String, Object> obj = (Map<String, Object>) competances.get("competance");
            System.out.println(Integer.parseInt(obj.get("id").toString()) + "zebiiiiii");
            e.setId(Integer.parseInt(obj.get("id").toString()));

            int joberid = Integer.parseInt(obj.get("profil").toString());
            JoberService js = new JoberService();
            Jober j1 = js.findById(joberid);
            e.setJober(j1);

            int competanceid = Integer.parseInt(obj.get("competance").toString());
            CompetanceService cs = new CompetanceService();
            Competance c = cs.findById(competanceid);
            e.setCompetance(c);

            e.setNiveau(Integer.parseInt(obj.get("niveau").toString()));

        } catch (IOException ex) {
        }

        return e;

    }
}
