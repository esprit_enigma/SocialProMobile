/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import Models.Nature;
import Models.Specialite;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;

import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import service.interfaces.IdelegationSpecialite;

/**
 *
 * @author oudayblouza
 */
public class SpecialiteService implements IdelegationSpecialite<Specialite, Integer, Nature> {

    List<Specialite> listSpecialites = new ArrayList();
    Specialite spec = new Specialite();

    @Override
    public void add(Specialite t) {
    }

    @Override
    public void delete(Integer id) {
    }

    @Override
    public Specialite findById(Integer id) {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/Specialite/select_specialite_ById.php?id=" + id + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {

                spec = getOneSpecialite(new String(con.getResponseData()));

            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);

        return spec;
    }

    @Override
    public List<Specialite> findBynv(Nature nv) {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/Specialite/select_Spec.php?nature=" + nv.getId() + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {
                try {
                    listSpecialites = getListSpecialite(new String(con.getResponseData()));
                } catch (Exception e) {
                    Specialite sp = getOneSpecialite(new String(con.getResponseData()));
                    listSpecialites.add(sp);
                }

            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);

        return listSpecialites;

    }

    @Override
    public List<Specialite> getAll() {
        return listSpecialites;
    }

    public Specialite findByNom(String nom) {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/Specialite/select_specialite_ByNom.php?nom=" + nom + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {

                spec = getOneSpecialite(new String(con.getResponseData()));

            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);

        return spec;
    }

    public ArrayList<Specialite> getListSpecialite(String json) {
        ArrayList<Specialite> listSpecialite = new ArrayList<Specialite>();
        try {

            JSONParser j = new JSONParser();
            Map<String, Object> delegations = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) delegations.get("specialite");

            for (Map<String, Object> obj : list) {
                Specialite e = new Specialite();
                e.setId(Integer.parseInt(obj.get("id").toString()));
                int natureid = Integer.parseInt(obj.get("nature").toString());
                NatureService ns = new NatureService();
                Nature n = ns.findById(natureid);
                e.setNature(n);
                e.setNom(obj.get("nom").toString());

                listSpecialite.add(e);

            }

        } catch (IOException ex) {

//            Dialog.show("Erreur", "Pas de specialite pour cette Nature", "Ok", null);
        }

        return listSpecialite;

    }

    public Specialite getOneSpecialite(String json) {
        Specialite e = new Specialite();
        try {

            JSONParser j = new JSONParser();
            Map<String, Object> specialites = j.parseJSON(new CharArrayReader(json.toCharArray()));
            Map<String, Object> obj = (Map<String, Object>) specialites.get("specialite");

            e.setId(Integer.parseInt(obj.get("id").toString()));

            int natureid = Integer.parseInt(obj.get("nature").toString());
            NatureService ns = new NatureService();
            Nature n = ns.findById(natureid);
            e.setNature(n);
            e.setNom(obj.get("nom").toString());

        } catch (IOException ex) {
        }

        return e;

    }
}
