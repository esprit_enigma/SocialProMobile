/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import Models.Delegation;

import java.util.ArrayList;

import java.util.List;

import Models.Jober;
import Models.Specialite;
import Models.User;
import com.codename1.io.CharArrayReader;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;

import com.codename1.ui.Dialog;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Map;

import service.interfaces.IJober;

/**
 *
 * @author Le Parrain
 */
public class JoberService implements IJober {

    Jober j = new Jober();

    @Override
    public void add(Jober t) {
        ConnectionRequest req = new ConnectionRequest();
        req.setUrl("http://localhost/PIMobile/src/PHP/Jober/Insert_Jober.php?delegation=" + t.getDelegation().getId() + "&specialite=" + t.getSpecialite().getId() + "&nom=" + t.getNom() + "&prenom=" + t.getPrenom() + "&date=" + t.getDateDeNaissance() + "&adresse=" + t.getAdresse() + "&description=" + t.getDescription() + "&numTel=" + t.getNumTel() + "&dateCreation=" + t.getDateCreation() + "&user=" + t.getUser().getId() + "&sexe=" + t.getSexe() + "");

        req.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {

                byte[] data = (byte[]) evt.getMetaData();
                String s = new String(data);
                System.out.println(s);
                if (s.equals("success")) {
                    Dialog.show("Confirmation", "ajout ok", "Ok", null);
                } else {
                    Dialog.show("Erreur", "erreur", "Ok", null);
                }
            }
        });

        NetworkManager.getInstance().addToQueue(req);
    }

    @Override
    public void update(Jober t) {
        ConnectionRequest req = new ConnectionRequest();
        System.out.println(t.getDelegation().getId());
        req.setUrl("http://localhost/PiMobile/src/PHP/Jober/Update_Jober.php?id=" + t.getId() + "&delegation=" + t.getDelegation().getId() + "&specialite=" + t.getSpecialite().getId() + "&nom=" + t.getNom() + "&prenom=" + t.getPrenom() + "&date=" + t.getDateDeNaissance() + "&adresse=" + t.getAdresse() + "&description=" + t.getDescription() + "&numTel=" + t.getNumTel() + "&dateCreation=" + t.getDateCreation() + "&sexe=" + t.getSexe() + "");

        req.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {

                byte[] data = (byte[]) evt.getMetaData();
                String s = new String(data);
                System.out.println(s);
                if (s.equals("success")) {
                    Dialog.show("Confirmation", "Modification ok", "Ok", null);
                } else {
                    Dialog.show("Erreur", "erreur", "Ok", null);
                }
            }
        });

        NetworkManager.getInstance().addToQueue(req);
    }

    @Override
    public void delete(int id) {
    }

    @Override
    public List<Jober> getAll() {
        List<Jober> j = new ArrayList();
        return j;
    }

    @Override
    public List<Jober> findBySpecialite(Specialite spec) {
        List<Jober> j = new ArrayList();
        return j;
    }

    @Override
    public List<Jober> findByDelegation(Delegation del) {
        List<Jober> j = new ArrayList();
        return j;
    }

    @Override
    public Jober findById(int id) {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/Jober/select_jober.php?id=" + id + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {

                j = getJober(new String(con.getResponseData()));

            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);
        return j;
    }

    public Jober findByUser(User user) {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/Jober/select_jober_user.php?user=" + user.getId() + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                j = getJober(new String(con.getResponseData()));
            }
        });

        NetworkManager.getInstance().addToQueueAndWait(con);
        return j;

    }

    public Jober getJober(String json) {
        Jober jober = new Jober();
        try {

            JSONParser j = new JSONParser();
            Map<String, Object> jobers = j.parseJSON(new CharArrayReader(json.toCharArray()));

            Map<String, Object> obj = (Map<String, Object>) jobers.get("jober");
            jober.setId(Integer.parseInt(obj.get("id").toString()));
            jober.setNom(obj.get("nom").toString());
            jober.setPrenom(obj.get("prenom").toString());
            jober.setAdresse(obj.get("adresse").toString());
            jober.setSexe(obj.get("sexe").toString());
            jober.setDescription(obj.get("description").toString());
            jober.setNumTel(Integer.parseInt(obj.get("numTel").toString()));

            String phpDateString = obj.get("dateNaissance").toString();
            String phpDateString1 = obj.get("dateCreation").toString();
            Date javaDate = null;
            Date javaDate1 = null;
            DateFormat sdf = new SimpleDateFormat("yyyy-M-dd");
            try {
                javaDate = sdf.parse(phpDateString);
                javaDate1 = sdf.parse(phpDateString1);
            } catch (ParseException ex) {

            }
            jober.setDateDeNaissance(javaDate);
            jober.setDateCreation(javaDate1);
            DelegationService ds = new DelegationService();
            Delegation d = ds.findById(Integer.parseInt(obj.get("delegation").toString()));
            jober.setDelegation(d);
            SpecialiteService ss = new SpecialiteService();
            Specialite s = ss.findById(Integer.parseInt(obj.get("specialite").toString()));
            jober.setSpecialite(s);
            System.out.println(jober.getDateCreation());

        } catch (IOException ex) {
        }

        return jober;

    }

}
