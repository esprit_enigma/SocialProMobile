/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import Models.Cours;
import Models.Entreprise;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Dialog;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import service.interfaces.IServiceCrud;


import java.util.ArrayList;
import java.util.List;

import java.util.Map;

/**
 *
 * @author m_s info
 */
public class CrudCours implements IServiceCrud<Cours>{
     private ConnectionRequest con ;
     List<Cours> listCours = new ArrayList();
     private Cours c;
    
    public CrudCours ()
    {

       
                
    }
    @Override
    public void add(Cours c) {
         con = new ConnectionRequest();
      con.setUrl("http://localhost/PIMobile/src/PHP/Insert_cours.php?user=" +c.getEntreprise().getId()+ "&description=" + c.getDescription()+"&note=" + 0+"&tuto=" + c.getTuto()+"&courpdf=" + c.getCourpdf()+"&date_ajout=" + c.getDateAjout()+"&date_modification=" + c.getDateModification()+"&etat=" + 0+"&titre=" +c.getTitre() +"&image=" + c.getImage());

                con.addResponseListener(new ActionListener<NetworkEvent>() {

                    @Override
                    public void actionPerformed(NetworkEvent evt) {
                        
                        byte[] data = (byte[]) evt.getMetaData();
                        String s = new String(data);
                        System.out.println(s);
                       
                            Dialog.show("Confirmation", "ajout ok", "Ok", null);
                       
                    }
                });

                NetworkManager.getInstance().addToQueue(con);
    }

    @Override
    public void update(Cours c) {
         con = new ConnectionRequest();
      con.setUrl("http://localhost/PIMobile/src/PHP/Modif_cours.php?description=" + c.getDescription()+"&note=" + 0+"&tuto=" + c.getTuto()+"&courpdf=" + c.getCourpdf()+"&date_ajout=" + c.getDateAjout()+"&date_modification=" + c.getDateModification()+"&etat=" + 0+"&titre=" +c.getTitre() +"&image=" + c.getImage()+"&id="+c.getId());

                con.addResponseListener(new ActionListener<NetworkEvent>() {

                    @Override
                    public void actionPerformed(NetworkEvent evt) {
                        
                        byte[] data = (byte[]) evt.getMetaData();
                        String s = new String(data);
                        System.out.println(s);
                       
                            Dialog.show("Confirmation", "Succes de modification", "Ok", null);
                       
                    }
                });

                NetworkManager.getInstance().addToQueue(con);
        
        }

    @Override
    public void delete(int id) {
       }

    @Override
   public List<Cours> getAll() {
    
        con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/elist_cours.php");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {
                   
                 listCours =getListCours(new String(con.getResponseData())) ;
               

            }

        });
       
        NetworkManager.getInstance().addToQueueAndWait(con);
            return listCours;       
    
    }
    public ArrayList<Cours> getListCours(String json) {
       ArrayList<Cours> listCours = new   ArrayList<Cours>();
        try {
           
            JSONParser j = new JSONParser();
            Map<String, Object> cours   = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) cours.get("cours");

            for (Map<String, Object> obj : list) {
                Cours c = new Cours();
                c.setId(Integer.parseInt(obj.get("id").toString()));
                c.setTitre(obj.get("titre").toString());
                Entreprise e = new Entreprise();
                e.setId(0);
                c.setEntreprise(e);
                c.setSpecialite(new SpecialiteService().findByNom("test"));
                c.setDescription(obj.get("description").toString());
                c.setNote(Integer.parseInt(obj.get("note").toString()));
                c.setTuto(obj.get("tuto").toString());
                
               
                try{
                    
               DateFormat f = new SimpleDateFormat("yyyy-M-dd");
               
                    c.setDateAjout(f.parse(obj.get("date_ajout").toString()));
                
                }
                catch(Exception ex)
                        {
                            ex.printStackTrace();
                        }
                c.setCourpdf(obj.get("courspdf").toString());
                c.setImage(obj.get("image").toString());
                
              
                listCours.add(c);
          
            }

        } catch (IOException ex) {
        }

        return listCours;

    }

    @Override
    public Cours findById(int id) {
        c = new Cours();
     ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/select_cours_ById.php?id=" + id + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {

                c = getOneCours(new String(con.getResponseData()));

            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);

        return c;
    }
public Cours getOneCours(String json)
{
    Cours c = new Cours();
        try {

            JSONParser j = new JSONParser();
            Map<String, Object> cours = j.parseJSON(new CharArrayReader(json.toCharArray()));
            Map<String, Object> obj = (Map<String, Object>) cours.get("cours");

              
                c.setId(Integer.parseInt(obj.get("id").toString()));
                c.setTitre(obj.get("titre").toString());
                Entreprise e = new Entreprise();
                e.setId(0);
                c.setEntreprise(e);
                c.setSpecialite(new SpecialiteService().findByNom("test"));
                c.setDescription(obj.get("description").toString());
                c.setNote(Integer.parseInt(obj.get("note").toString()));
                c.setTuto(obj.get("tuto").toString());
                
               
                try{
                    
               DateFormat f = new SimpleDateFormat("yyyy-M-dd");
               
                    c.setDateAjout(f.parse(obj.get("date_ajout").toString()));
                
                }
                catch(Exception ex)
                        {
                            ex.printStackTrace();
                        }
                c.setCourpdf(obj.get("courspdf").toString());
                c.setImage(obj.get("image").toString());
                
              

        } catch (IOException ex) {
        }

        return c;
}
    
    
}
