/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import Models.Ville;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;

import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;

import com.codename1.ui.events.ActionListener;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import service.interfaces.INatureVille;

/**
 *
 * @author oudayblouza
 */
public class VilleService implements INatureVille<Ville, Integer, String> {

    List<Ville> listVilles = new ArrayList();
    Ville vil = new Ville();

    @Override
    public void add(Ville t) {
    }

    @Override
    public void delete(Integer id) {
    }

    @Override
    public Ville findById(Integer id) {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/Ville/select_ville_Byid.php?id=" + id + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                vil = getOneVille(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return vil;
    }

    @Override
    public Ville findByNom(String nom) {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/Ville/select_ville_ByNom.php?nom=" + nom + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {

                vil = getOneVille(new String(con.getResponseData()));

            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);

        return vil;
    }

    @Override

    public List<Ville> getAll() {

        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/Ville/select.php");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {
                try {
                    listVilles = getListVille(new String(con.getResponseData()));
                } catch (Exception e) {
                    vil = getOneVille(new String(con.getResponseData()));
                    listVilles.add(vil);
                }
            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);
        return listVilles;

    }

    public ArrayList<Ville> getListVille(String json) {
        ArrayList<Ville> listVille = new ArrayList<Ville>();
        try {

            JSONParser j = new JSONParser();
            Map<String, Object> villes = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) villes.get("ville");

            for (Map<String, Object> obj : list) {
                Ville e = new Ville();
                e.setId(Integer.parseInt(obj.get("id").toString()));
                e.setNom(obj.get("nom").toString());

                listVille.add(e);

            }

        } catch (IOException ex) {
        }

        return listVille;

    }

    public Ville getOneVille(String json) {
        Ville e = new Ville();
        try {

            JSONParser j = new JSONParser();
            Map<String, Object> villes = j.parseJSON(new CharArrayReader(json.toCharArray()));
            Map<String, Object> obj = (Map<String, Object>) villes.get("ville");

            e.setId(Integer.parseInt(obj.get("id").toString()));
            e.setNom(obj.get("nom").toString());

        } catch (IOException ex) {
        }

        return e;

    }

}
