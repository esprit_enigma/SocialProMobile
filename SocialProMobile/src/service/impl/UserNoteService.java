/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import Models.Cours;
import Models.Entreprise;
import Models.Jober;
import Models.UserNote;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Dialog;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 *
 * @author Seddik
 */
public class UserNoteService {
    private ConnectionRequest con ;
    public UserNote u = new UserNote();
    public void add(UserNote un)
    { con = new ConnectionRequest();
      con.setUrl("http://localhost/PIMobile/src/PHP/user_note.php?id_cour=" +un.getCours().getId()+ "&id_user=" + un.getJober().getId()+"&note=" + un.getNote());

                con.addResponseListener(new ActionListener<NetworkEvent>() {

                    @Override
                    public void actionPerformed(NetworkEvent evt) {
                        
                        byte[] data = (byte[]) evt.getMetaData();
                        String s = new String(data);
                        System.out.println(s);
                       
                            Dialog.show("vous avez u "+un.getNote()+"/n votre notre serai enregistrée dans votre profil", "ajout ok", "Ok", null);
                       
                    }
                });

                NetworkManager.getInstance().addToQueue(con);
        
    }
    
    public void update(UserNote un)
    { con = new ConnectionRequest();
      con.setUrl("http://localhost/PIMobile/src/PHP/update_user_note.php?id_cour=" +un.getCours().getId()+ "&id_user=" + un.getJober().getId()+"&note=" + un.getNote()+"&id="+un.getId());

                con.addResponseListener(new ActionListener<NetworkEvent>() {

                    @Override
                    public void actionPerformed(NetworkEvent evt) {
                        
                        byte[] data = (byte[]) evt.getMetaData();
                        String s = new String(data);
                        System.out.println(s);
                        
                            Dialog.show("vous avez u "+un.getNote()+"/n votre notre serai enregistrée dans votre profil", "ajout ok", "Ok", null);
                             
                    }
                });

                NetworkManager.getInstance().addToQueue(con);
        
    }
    
        public UserNote find(UserNote un) {
        

     ConnectionRequest con = new ConnectionRequest();
            System.out.println("cours "+un.getCours().getId()+" user "+un.getJober().getId());
        con.setUrl("http://localhost/PIMobile/src/PHP/user_note_ByCours.php?id_cour=" + un.getCours().getId() + "&id_user="+un.getJober().getId());
        con.addResponseListener(new ActionListener<NetworkEvent>() {
                        @Override
            public void actionPerformed(NetworkEvent evt) {

                u = getOneUserNote(new String(con.getResponseData()));

            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);

        return u;
    }
public UserNote getOneUserNote(String json)
{
    UserNote c = new UserNote();
        try {

            JSONParser j = new JSONParser();
            Map<String, Object> usern = j.parseJSON(new CharArrayReader(json.toCharArray()));
            Map<String, Object> obj = (Map<String, Object>) usern.get("user_note_c");

              
                c.setId(Integer.parseInt(obj.get("id").toString()));
                
                Jober e = new Jober(4);
                
                c.setCours(new CrudCours().findById(Integer.parseInt(obj.get("id_cour").toString())));
                c.setJober(e);
                c.setNote(Integer.parseInt(obj.get("note").toString()));

        } catch (IOException ex) {
        }

        return c;
}
    
    
}
