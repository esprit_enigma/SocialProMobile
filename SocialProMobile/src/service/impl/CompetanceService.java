/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import Models.Competance;
import Models.Experience;
import Models.Jober;
import com.codename1.components.ToastBar;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import service.interfaces.INatureVille;

/**
 *
 * @author oudayblouza
 */
public class CompetanceService implements INatureVille<Competance, Integer, String> {

    Competance comp = new Competance();
    List<Competance> lstcomp = new ArrayList<>();

    @Override
    public void add(Competance t) {
    }

    @Override
    public void delete(Integer id) {
    }

    @Override
    public List<Competance> getAll() {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/Competance/select_competance.php");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {
                try {
                    try{
                    lstcomp = getListCompetance(new String(con.getResponseData()));
                } catch (Exception e) {
                    comp = getOneCompetance(new String(con.getResponseData()));
                    lstcomp.add(comp);
                }
                  } catch (Exception e1) {
                    ToastBar.showErrorMessage("Aucune Experience");
                }
            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);

        return lstcomp;
    }

    @Override
    public Competance findById(Integer id) {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/Competance/select_competance_Byid.php?id=" + id + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {

                comp = getOneCompetance(new String(con.getResponseData()));

            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);

        return comp;
    }

    @Override
    public Competance findByNom(String nom) {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/Competance/select_competance_Bynom.php?nom=" + nom + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {

                comp = getOneCompetance(new String(con.getResponseData()));
                System.out.println(comp+"dd");
            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);
 System.out.println(comp+"dd");
        return comp;
    }

    public Competance getOneCompetance(String json) {
        Competance e = new Competance();
        try {

            JSONParser j = new JSONParser();
            Map<String, Object> villes = j.parseJSON(new CharArrayReader(json.toCharArray()));
            Map<String, Object> obj = (Map<String, Object>) villes.get("competance");

            e.setId(Integer.parseInt(obj.get("id").toString()));
            e.setNom(obj.get("nom").toString());

        } catch (IOException ex) {
        }

        return e;

    }

    public ArrayList<Competance> getListCompetance(String json) {
        ArrayList<Competance> listComp = new ArrayList<Competance>();
        try {

            JSONParser j = new JSONParser();
            Map<String, Object> competances = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) competances.get("competance");

            for (Map<String, Object> obj : list) {
                Competance e = new Competance();

                e.setId(Integer.parseInt(obj.get("id").toString()));
                e.setNom(obj.get("nom").toString());

                listComp.add(e);

            }

        } catch (IOException ex) {

//            Dialog.show("Erreur", "Pas de specialite pour cette Nature", "Ok", null);
        }

        return listComp;

    }

}
