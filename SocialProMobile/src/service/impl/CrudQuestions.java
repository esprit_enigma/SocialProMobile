/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import Models.Commentaires;
import Models.Cours;
import Models.Entreprise;
import Models.Question;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import service.interfaces.IServiceCrud;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author m_s info
 */
public class CrudQuestions implements IServiceCrud<Question>{
 private ConnectionRequest con ;
     List<Question> listQu = new ArrayList();
    
    
    public CrudQuestions ()
    {
//        connexion = DataSource.getInstance().getConnection();
    }

    @Override
    public void add(Question t) {
        }

    @Override
    public void update(Question t) {
         }

    @Override
    public void delete(int id) {
         }

    @Override
    public List<Question> getAll() {
      con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/Questions.php");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {
                   
                 listQu =getListQ(new String(con.getResponseData())) ;
               

            }

        });
       
        NetworkManager.getInstance().addToQueueAndWait(con);
            return listQu; 
    }

    @Override
    public Question findById(int id) {
    return new Question();
    }
    
    public ArrayList<Question> getListQ(String json)
    {
         
       ArrayList<Question> listQ = new   ArrayList<Question>();
        try {
           
            JSONParser j = new JSONParser();
            Map<String, Object> question   = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) question.get("question");

            for (Map<String, Object> obj : list) {
                Question c = new Question();
                c.setId(Integer.parseInt(obj.get("id").toString()));
                c.setContenu(obj.get("contenu").toString());
             
                c.setCours(new CrudCours().findById(Integer.parseInt(obj.get("id_cour").toString())));
                c.setReponse1(obj.get("reponse1").toString());
                c.setReponse2(obj.get("reponse2").toString());
                c.setReponseV(obj.get("reponse_v").toString());
        
               
               
                
              
                listQ.add(c);
          
            }

        } catch (IOException ex) {
        }

        return listQ;

    }
    
    
}
