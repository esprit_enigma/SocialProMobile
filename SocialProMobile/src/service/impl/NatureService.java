/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import Models.Nature;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import java.util.List;
import java.util.Map;
import service.interfaces.INatureVille;

/**
 *
 * @author oudayblouza
 */
public class NatureService implements INatureVille<Nature, Integer, String> {

    List<Nature> listNatures = new ArrayList<>();
Nature nat = new Nature();
    @Override
    public void add(Nature t) {
    }

    @Override
    public void delete(Integer id) {
    }

    @Override
    public List<Nature> getAll() {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/Nature/select_Nature.php");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {
                try{
                listNatures = getListNature(new String(con.getResponseData()));
                }catch(Exception e){
                    nat =  getOneNature(new String(con.getResponseData()));
                    listNatures.add(nat);
                }
            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);
        return listNatures;
    }

    @Override
    public Nature findById(Integer id) {
                ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/Nature/select_nature_Byid.php?id=" + id + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {

                nat = getOneNature(new String(con.getResponseData()));

            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);

        return nat;
    }

    @Override
    public Nature findByNom(String nom) {
          ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/Nature/select_nature_ByNom.php?nom=" + nom + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {

                nat = getOneNature(new String(con.getResponseData()));

            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);

        return nat;
    }

    public ArrayList<Nature> getListNature(String json) {
        ArrayList<Nature> listNature = new ArrayList<Nature>();
        try {

            JSONParser j = new JSONParser();
            Map<String, Object> natures = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) natures.get("nature");

            for (Map<String, Object> obj : list) {
                Nature e = new Nature();
                e.setId(Integer.parseInt(obj.get("id").toString()));
                e.setNom(obj.get("nom").toString());
               
                listNature.add(e);

            }

        } catch (IOException ex) {
        }

        return listNature;

    }
       public Nature getOneNature(String json) {
        Nature e = new Nature();
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> villes = j.parseJSON(new CharArrayReader(json.toCharArray()));
            Map<String, Object> obj = (Map<String, Object>) villes.get("nature");
            e.setId(Integer.parseInt(obj.get("id").toString()));
            e.setNom(obj.get("nom").toString());

        } catch (IOException ex) {
        }

        return e;

    }
}
