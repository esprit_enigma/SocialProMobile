/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import Models.Experience;
import Models.Jober;
import Models.Projet;
import com.codename1.components.ToastBar;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Dialog;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import service.interfaces.ICvJober;

/**
 *
 * @author oudayblouza
 */
public class ProjetService implements ICvJober<Projet, Integer, Jober> {
List<Projet> listProjet =new ArrayList<>();
    @Override
    public void add(Projet t) {
                       ConnectionRequest req = new ConnectionRequest();
        req.setUrl("http://localhost/PIMobile/src/PHP/Projet/Add_Projet.php?organisation="+t.getOrganisation()+"&idprofil="+t.getJober().getId()+"&nom="+t.getNom()+"&description="+t.getDescription()+"&dateDebut="+t.getDateDebut()+"&dateFin="+t.getDateFin()+"&dateAjout="+t.getDateAjout()+"");

        req.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {

                byte[] data = (byte[]) evt.getMetaData();
                String s = new String(data);
                System.out.println(s);
                if (s.equals("success")) {
                    Dialog.show("Confirmation", "Ajout ok", "Ok", null);
                } else {
                    Dialog.show("Erreur", "erreur", "Ok", null);
                }
            }
        });

        NetworkManager.getInstance().addToQueue(req);
        
        
        
    }

    @Override
    public void delete(Integer id) {
                   ConnectionRequest req = new ConnectionRequest();
        req.setUrl("http://localhost/PIMobile/src/PHP/Projet/Delete_Projet.php?id="+id + "");

        req.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {

                byte[] data = (byte[]) evt.getMetaData();
                String s = new String(data);
                System.out.println(s);
                if (s.equals("success")) {
                    Dialog.show("Confirmation", "Suppression ok", "Ok", null);
                } else {
                    Dialog.show("Erreur", "erreur", "Ok", null);
                }
            }
        });

        NetworkManager.getInstance().addToQueue(req);
    }

    @Override
    public List<Projet> findByJober(Jober jober) {
   ConnectionRequest con = new ConnectionRequest();
        System.out.println(jober.getId());
        con.setUrl("http://localhost/PIMobile/src/PHP/Projet/select_Pro.php?jober=" + jober.getId() + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {
                try {
                    try {
                        listProjet = getListPro(new String(con.getResponseData()));
                    } catch (Exception e) {
                        Projet sp = getOnePro(new String(con.getResponseData()));
                        listProjet.add(sp);
                    }
                } catch (Exception e1) {
                    ToastBar.showErrorMessage("Aucune Experience");
                }

            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);
  
        return listProjet;
    }
    
    
        public ArrayList<Projet> getListPro(String json) {
        ArrayList<Projet> listComp = new ArrayList<Projet>();
        try {

            JSONParser j = new JSONParser();
            Map<String, Object> projets = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) projets.get("projet");

            for (Map<String, Object> obj : list) {
                Projet e = new Projet();
                e.setId(Integer.parseInt(obj.get("id").toString()));

                int joberid = Integer.parseInt(obj.get("profil").toString());
                JoberService js = new JoberService();
                Jober j1 = js.findById(joberid);
                e.setJober(j1);

                e.setOrganisation(obj.get("organisation").toString());
                e.setNom(obj.get("nom").toString());
                e.setDescription(obj.get("description").toString());

                String phpDateString = obj.get("dateDebut").toString();
                String phpDateString1 = obj.get("dateFin").toString();
                String phpDateString2 = obj.get("dateAjout").toString();

                Date javaDate = null;
                Date javaDate1 = null;
                Date javaDate2 = null;
                DateFormat sdf = new SimpleDateFormat("yyyy-M-dd");

                try {
                    javaDate = sdf.parse(phpDateString);

                    javaDate1 = sdf.parse(phpDateString1);
                    javaDate2 = sdf.parse(phpDateString2);
                } catch (ParseException ex) {

                }
                e.setDateDebut(javaDate);
                e.setDateFin(javaDate1);
                e.setDateAjout(javaDate2);

                listComp.add(e);

            }

        } catch (IOException ex) {

//            Dialog.show("Erreur", "Pas de specialite pour cette Nature", "Ok", null);
        }

        return listComp;

    }

    public Projet getOnePro(String json) {
        Projet e = new Projet();
        try {

            JSONParser j = new JSONParser();
            Map<String, Object> projets = j.parseJSON(new CharArrayReader(json.toCharArray()));
            Map<String, Object> obj = (Map<String, Object>) projets.get("projet");
            e.setId(Integer.parseInt(obj.get("id").toString()));

        

            int joberid = Integer.parseInt(obj.get("profil").toString());
            JoberService js = new JoberService();
            Jober j1 = js.findById(joberid);
            e.setJober(j1);

            e.setOrganisation(obj.get("organisation").toString());
            e.setNom(obj.get("nom").toString());
            e.setDescription(obj.get("description").toString());

            String phpDateString = obj.get("dateDebut").toString();
            String phpDateString1 = obj.get("dateFin").toString();
            String phpDateString2 = obj.get("dateAjout").toString();

            Date javaDate = null;
            Date javaDate1 = null;
            Date javaDate2 = null;
            DateFormat sdf = new SimpleDateFormat("yyyy-M-dd");

            try {
                javaDate = sdf.parse(phpDateString);

                javaDate1 = sdf.parse(phpDateString1);
                javaDate2 = sdf.parse(phpDateString2);
            } catch (ParseException ex) {

            }
            e.setDateDebut(javaDate);
            e.setDateFin(javaDate1);
            e.setDateAjout(javaDate2);

        } catch (IOException ex) {
            Dialog.show("Erreur", "Pas de specialite pour cette Nature", "Ok", null);
        }

        return e;

    }
    
}
