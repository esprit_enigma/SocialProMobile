/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.impl;

import Models.Delegation;
import Models.Ville;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Dialog;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import service.interfaces.IdelegationSpecialite;

/**
 *
 * @author oudayblouza
 */
public class DelegationService implements IdelegationSpecialite<Delegation, Integer, Ville> {

    List<Delegation> listDelegations = new ArrayList();
    Delegation del = new Delegation();

    @Override
    public void add(Delegation t) {
    }

    @Override
    public void delete(Integer id) {
    }

    @Override
    public Delegation findById(Integer id) {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/Delegation/select_delegation_ById.php?id=" + id + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {

                del = getOneDelegation(new String(con.getResponseData()));

            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);

        return del;
    }

    @Override
    public List<Delegation> findBynv(Ville nv) {

        ConnectionRequest con = new ConnectionRequest();
        System.out.println(nv.getNom());
        con.setUrl("http://localhost/PIMobile/src/PHP/Delegation/select_Del.php?ville=" + nv.getId() + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {
                try {
                    listDelegations = getListDelegation(new String(con.getResponseData()));
                } catch (Exception e) {
                    del = getOneDelegation(new String(con.getResponseData()));
                    listDelegations.add(del);
                }

            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);
        return listDelegations;

    }

    @Override
    public List<Delegation> getAll() {
        return listDelegations;
    }

    public Delegation findByNom(String nom) {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PIMobile/src/PHP/Delegation/select_delegation_ByNom.php?nom=" + nom + "");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {

                del = getOneDelegation(new String(con.getResponseData()));

            }

        });

        NetworkManager.getInstance().addToQueueAndWait(con);

        return del;
    }

    public ArrayList<Delegation> getListDelegation(String json) {
        ArrayList<Delegation> listDelegation = new ArrayList<Delegation>();
        try {

            JSONParser j = new JSONParser();
            Map<String, Object> delegations = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) delegations.get("delegation");

            for (Map<String, Object> obj : list) {
                Delegation e = new Delegation();

                e.setId(Integer.parseInt(obj.get("id").toString()));
                e.setNom(obj.get("nom").toString());
                listDelegation.add(e);

            }

        } catch (Exception ex) {
            System.out.println("Pas de delegation pour cette Ville");
//            Dialog.show("Erreur", "Pas de delegation pour cette Ville", "Ok", null);

        }

        return listDelegation;

    }

    public Delegation getOneDelegation(String json) {
        Delegation e = new Delegation();
        try {

            JSONParser j = new JSONParser();
            Map<String, Object> specialites = j.parseJSON(new CharArrayReader(json.toCharArray()));
            Map<String, Object> obj = (Map<String, Object>) specialites.get("delegation");

            e.setId(Integer.parseInt(obj.get("id").toString()));
            
            int idville = Integer.parseInt(obj.get("ville").toString());
            VilleService vs = new VilleService();
            Ville v = vs.findById(idville);
            e.setVille(v);
            e.setNom(obj.get("nom").toString());

        } catch (IOException ex) {
        }

        return e;

    }
}
