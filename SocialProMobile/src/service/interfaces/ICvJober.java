/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.interfaces;

import java.util.List;

/**
 *
 * @author Le Parrain
 */
public interface ICvJober<O ,I,J> {
    void add(O t);
    void delete(I id);
    List<O> findByJober(J jober);
}
