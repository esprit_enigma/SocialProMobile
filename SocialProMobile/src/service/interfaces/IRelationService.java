/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service.interfaces;

import java.util.List;
import Models.Relation;

/**
 *
 * @author Alaa
 */
public interface IRelationService {

    void add(Relation r);

    void update(Relation r);

    void delete(int id);

    List<Relation> getAll();

    Relation findById(int id);
}
