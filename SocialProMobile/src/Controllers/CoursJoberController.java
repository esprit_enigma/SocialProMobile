/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Cours;
import Models.Jober;
import Models.Question;
import Models.UserNote;
import com.codename1.components.ImageViewer;
import com.codename1.ui.BrowserComponent;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.SwipeableContainer;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import service.impl.CrudCours;
import service.impl.CrudQuestions;
import service.impl.UserNoteService;

/**
 *
 * @author Alaa
 */
public class CoursJoberController {

    Resources theme;
    EncodedImage enc1;
    public int note;

    public void lire(Cours c) {
        Form hi = new Form("Video", new BorderLayout());
        Style s = UIManager.getInstance().getComponentStyle("TitleCommand");
        FontImage icon = FontImage.createMaterial(FontImage.MATERIAL_BACKSPACE, s);
        hi.getToolbar().addCommandToLeftBar("List Cour", icon, (e) -> listCours());
        BrowserComponent browser = new BrowserComponent();
        browser.setURL(c.getTuto());
        hi.add(BorderLayout.CENTER, browser);
        hi.show();
    }

    public void listCours() {
        Form f = new Form("Liste de mes cours", new BoxLayout(BoxLayout.Y_AXIS));
        Container ctn;
        Container ctn1;
        Container ctn2;

        List<Cours> listcours = new CrudCours().getAll();

        for (Cours c : listcours) {
            String URL1 = "http://localhost/SocialPro/web/images/cours/" + c.getImage();
            System.out.println(URL1);
            theme = UIManager.initFirstTheme("/theme"); //  Logger.getLogger(CoursController.class.getName()).log(Level.SEVERE, null, ex);
            Image img1 = theme.getImage("Load.png");
            enc1 = EncodedImage.createFromImage(img1, false);
            Image imgServer = URLImage.createToStorage(enc1, c.getImage(), URL1);

            Image img = imgServer.scaled(400, 400);
            ImageViewer imgv = new ImageViewer(imgServer);

            Label lblImage = new Label("");
            lblImage.setIcon(img);
            Button lblTest = new Button("Test");
            lblTest.setUIID("mail");
            Button lblMail = new Button("save mail");
            lblMail.setUIID("mail");

            lblMail.addActionListener(l -> {

                passerTest(c);
            });
            Button lblPdf = new Button("show pdf");
            lblPdf.setUIID("pdf");
            lblPdf.addActionListener(l -> {

                lirepdf(c);
            });

            Button lblModif = new Button("Voir Video");
            lblModif.setUIID("youtube");

            lblModif.addActionListener(l -> {
                lire(c);
            });

            Label lblTitre = new Label("Titre : " + c.getTitre());
            Label lblDate = new Label("Date d'ajout : " + c.getDateAjout().toString());

            ctn1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            ctn1.add(lblTitre);
            ctn1.add(lblDate);
            ctn2 = new Container(new BoxLayout(BoxLayout.X_AXIS));
            Container cont = new Container(new BoxLayout(BoxLayout.X_AXIS));
            cont.add(lblModif);
            cont.add(lblPdf);

            SwipeableContainer sc = new SwipeableContainer(lblMail, GridLayout.encloseIn(1, cont),
                    ctn2);

            ctn = new Container(new BoxLayout(BoxLayout.X_AXIS));
            ctn.add(lblImage);

            ctn2.add(ctn);
            ctn2.add(ctn1);

            f.add(sc);

        }
        f.show();

    }

    public void lirepdf(Cours c) {
        Form hi = new Form("Cours en pdf", new BorderLayout());
        Style s = UIManager.getInstance().getComponentStyle("TitleCommand");
        FontImage icon = FontImage.createMaterial(FontImage.MATERIAL_BACKSPACE, s);
        hi.getToolbar().addCommandToLeftBar("", icon, (e) -> listCours());
        BrowserComponent browser = new BrowserComponent();
        browser.setURL(c.getCourpdf());
        hi.add(BorderLayout.CENTER, browser);
        hi.show();
    }

    public void passerTest(Cours c) {
        note = 0;
        List<Question> questions = new CrudQuestions().getAll();
        List<Question> qs = new ArrayList<Question>();
        for (Question q : questions) {
            if (q.getCours().getId() == c.getId()) {
                qs.add(q);
            }
        }

        Form hi = new Form("Ce test serai engeristré dans votre profil", new BoxLayout(BoxLayout.Y_AXIS));
        Style s = UIManager.getInstance().getComponentStyle("TitleCommand");
        FontImage icon = FontImage.createMaterial(FontImage.MATERIAL_BACKSPACE, s);
        hi.getToolbar().addCommandToLeftBar("", icon, (e) -> listCours());
        Collections.shuffle(qs);

        for (Question q : qs) {

            ArrayList<String> rep = new ArrayList<String>();
            rep.add(q.getReponse1());
            rep.add(q.getReponse2());
            rep.add(q.getReponseV());
            Collections.shuffle(rep);
            System.out.println();
            Container ctn1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            Container ctn2 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            Container ctn3 = new Container(new BorderLayout());
            ctn1.add(new Label(q.getContenu() + " :"));
            RadioButton cb1 = new RadioButton(rep.get(0));
            RadioButton cb2 = new RadioButton(rep.get(1));
            RadioButton cb3 = new RadioButton(rep.get(2));
            ButtonGroup grp = new ButtonGroup(cb1, cb2, cb3);
            ctn2.add(cb1);
            ctn2.add(cb2);
            ctn2.add(cb3);
            //attribuer une note 

            ctn3.add(BorderLayout.WEST, ctn1);
            ctn3.add(BorderLayout.EAST, ctn2);

            hi.add(ctn3);

            Button b = new Button("Valider");
            b.addActionListener((evt) -> {
                if (grp.getRadioButton(grp.getSelectedIndex()).getText().equals(q.getReponseV())) {
                    note = note + 1;
                }
                b.setEnabled(false);
                cb1.setEnabled(false);
                cb2.setEnabled(false);
                cb3.setEnabled(false);
            });
            hi.add(b);

        }
        Button btn = new Button("recevoir note");
        btn.addActionListener((evt) -> {
            UserNote usrn = new UserNote();
            usrn.setCours(c);
            usrn.setJober(new Jober(4));
            usrn.setNote(note);
            UserNoteService uns = new UserNoteService();
            UserNote unt = uns.find(usrn);
            if (unt == null) {

                uns.add(usrn);
            } else {
                usrn.setId(unt.getId());
                uns.update(usrn);
            }
        });
        hi.add(btn);
        hi.show();
    }

}
