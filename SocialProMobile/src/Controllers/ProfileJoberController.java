/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Delegation;
import Models.Jober;
import Models.Nature;
import Models.Specialite;
import Models.User;
import Models.Ville;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.list.DefaultListModel;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import java.util.List;
import service.impl.DelegationService;
import service.impl.JoberService;
import service.impl.NatureService;
import service.impl.SpecialiteService;
import service.impl.VilleService;

/**
 *
 * @author Alaa
 */
public class ProfileJoberController {

    Form f;

    int lock = 1;
    String x = "Lock/Unlock";
    TextField tfNom = new TextField();
    TextField tfPrenom = new TextField();
    Picker DSDatenaiss = new Picker();
    TextField tfAdresse = new TextField();
    TextField taDescription = new TextField();
    TextField tfNumtel = new TextField();
    ComboBox sexe = new ComboBox();
    ComboBox ville = new ComboBox();
    ComboBox delegation = new ComboBox();
    ComboBox nature = new ComboBox();
    ComboBox specialite = new ComboBox();
    Command cmd, cmd1;
    Image img, img2, img5, img7, img9;
    private Resources theme;
    Form f1;
    User u = LoginController.getCurrentUser();
    JoberService js = new JoberService();
    Jober j = js.findByUser(u);
    Button btnupsate;

    public ProfileJoberController() {
        theme = UIManager.initFirstTheme("/theme");
        img = theme.getImage("Lock1.png");
        img2 = theme.getImage("logout.png");
        Image img1 = img.scaled(20, 20);
        Image img3 = img2.scaled(20, 20);

        f1 = new Form("Mon Profil", BoxLayout.y());
        Container f = new Container(new GridLayout(2, 2));
        //f.setUIID("ContainerTitle");
        //f1.setUIID("FormCV");

        img5 = theme.getImage("projet.png");
        Image img6 = img5.scaled(20, 20);

        f1.getToolbar().addCommandToSideMenu("Mon Profil", img6, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                ProfileJoberController pc = new ProfileJoberController();
                pc.getF().show();
            }

        });
        img7 = theme.getImage("experience.png");
        Image img8 = img7.scaled(20, 20);
        f1.getToolbar().addCommandToSideMenu("Mon CV", img8, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                CVController cc = new CVController();
                cc.getF().show();
            }

        });

        img9 = theme.getImage("competance.png");
        Image img10 = img9.scaled(20, 20);
        f1.getToolbar().addCommandToSideMenu("Statistiques CV", img10, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                CVNoteController cvn = new CVNoteController();
                cvn.getF().show();
            }

        });
        cmd = new Command(x);
        cmd.setIcon(img1);
        f1.getToolbar().addCommandToOverflowMenu(cmd);

        cmd1 = new Command("Logout");
        cmd1.setIcon(img3);
        f1.getToolbar().addCommandToOverflowMenu(cmd1);

        f1.getToolbar().addCommandToOverflowMenu("List utilisateur", img10, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                ListUsersController lu = new ListUsersController();
                lu.getF().show();
            }

        });

        f1.addCommandListener(evt
                -> {
            if (evt.getCommand() == cmd){
                if (lock == 1) {
                    unlock();
                } else {
                    lock();
                }
            }
        }
        );

        System.out.println(j.getNom() + "test 1");
        Label nom = new Label("Nom :");
        tfNom.setText(j.getNom());
        f.add(nom);
        f.add(tfNom);
        //tfNom.setUIID("textf");

        Label prenom = new Label("Prenom :");
        tfPrenom.setText(j.getPrenom());
        f.add(prenom);
        f.add(tfPrenom);
        //tfPrenom.setUIID("textf");

        Label DateNaiss = new Label("Date Naiss :");
        DSDatenaiss.setDate(j.getDateDeNaissance());
        f.add(DateNaiss);
        f.add(DSDatenaiss);
        //DSDatenaiss.setUIID("textf");

        Label adresse = new Label("Adresse :");
        tfAdresse.setText(j.getAdresse());
        f.add(adresse);
        f.add(tfAdresse);
        //tfAdresse.setUIID("textf");

        Label description = new Label("Description :");
        taDescription.setText(j.getDescription());
        //taDescription.setUIID("textf");
        f.add(description);
        f.add(taDescription);

        Label NumTel = new Label("Num Tel :");
        tfNumtel.setText("" + j.getNumTel());
        f.add(NumTel);
        f.add(tfNumtel);
        //tfNumtel.setUIID("textf");
        Label Sexe = new Label("Sexe :");

        sexe.addItem("Homme");
        sexe.addItem("Femme");
        sexe.addItem("Autre");

        sexe.setSelectedItem(j.getSexe());

        DefaultListModel dlm = (DefaultListModel) ville.getModel();
        dlm.removeAll();
        VilleService vs = new VilleService();
        List<Ville> ls = vs.getAll();
        for (Ville v : ls) {
            ville.addItem(v.getNom());
        }
        ville.setSelectedItem(j.getDelegation().getVille().getNom());

        DefaultListModel dlm1 = (DefaultListModel) delegation.getModel();
        dlm1.removeAll();
        String villes = ville.getSelectedItem().toString();
        VilleService vs1 = new VilleService();

        Ville nv = vs.findByNom(villes);

        DelegationService vd = new DelegationService();
        List<Delegation> ls1 = vd.findBynv(nv);

        for (Delegation v : ls1) {
            delegation.addItem(v.getNom());

        }
        delegation.setSelectedItem(j.getDelegation().getNom());

        f.add(Sexe);
        f.add(sexe);
        Label Ville = new Label("Ville :");
        f.add(Ville);
        f.add(ville);
        Label Delegation = new Label("Delegation :");
        f.add(Delegation);
        f.add(delegation);
        ville.addActionListener((evt) -> {
            DefaultListModel dlm2 = (DefaultListModel) delegation.getModel();
            dlm2.removeAll();

            String villes1 = ville.getSelectedItem().toString();

            Ville nv1 = vs.findByNom(villes1);

            List<Delegation> lsd = vd.findBynv(nv1);

            for (Delegation v : lsd) {
                delegation.addItem(v.getNom());

            }

        });

        DefaultListModel dlm4 = (DefaultListModel) nature.getModel();
        dlm4.removeAll();
        NatureService ns = new NatureService();
        List<Nature> ls4 = ns.getAll();
        for (Nature v : ls4) {
            nature.addItem(v.getNom());
        }
        nature.setSelectedItem(j.getSpecialite().getNature().getNom());

        DefaultListModel dlm5 = (DefaultListModel) specialite.getModel();
        dlm5.removeAll();
        String natures = nature.getSelectedItem().toString();

        Nature nd = ns.findByNom(natures);

        SpecialiteService ss = new SpecialiteService();
        List<Specialite> ls5 = ss.findBynv(nd);
        for (Specialite v : ls5) {
            specialite.addItem(v.getNom());

        }
        specialite.setSelectedItem(j.getSpecialite().getNom());

        Label Nature = new Label("Nature :");
        f.add(Nature);
        f.add(nature);

        Label Specialite = new Label("Specialite :");
        f.add(Specialite);
        f.add(specialite);
        nature.addActionListener((evt) -> {
            DefaultListModel dlm6 = (DefaultListModel) specialite.getModel();
            dlm6.removeAll();

            String natures1 = nature.getSelectedItem().toString();

            Nature n = ns.findByNom(natures1);

            List<Specialite> ls2 = ss.findBynv(n);

            for (Specialite v : ls2) {
                specialite.addItem(v.getNom());
            }
        });

        btnupsate = new Button("Update");
        Label vide = new Label();
        f.add(vide);

        btnupsate.addActionListener(cmd -> {
            updateJober();
        });
        lock();
        f1.add(f);
        f1.add(btnupsate);

    }

    public void lock() {
        tfNom.setEditable(false);
        tfPrenom.setEditable(false);
        DSDatenaiss.setEnabled(false);
        tfAdresse.setEditable(false);
        taDescription.setEditable(false);
        tfNumtel.setEditable(false);
        sexe.setEnabled(false);
        ville.setEnabled(false);
        delegation.setEnabled(false);
        nature.setEnabled(false);
        specialite.setEnabled(false);
        btnupsate.setVisible(false);
        lock = 1;
        x = "Unlock";
    }

    public void unlock() {
        tfNom.setEditable(true);
        tfPrenom.setEditable(true);
        DSDatenaiss.setEnabled(true);
        tfAdresse.setEditable(true);
        taDescription.setEditable(true);
        tfNumtel.setEditable(true);
        sexe.setEnabled(true);
        ville.setEnabled(true);
        delegation.setEnabled(true);
        nature.setEnabled(true);
        specialite.setEnabled(true);
        btnupsate.setVisible(true);
        lock = 0;
        x = "Lock";

    }

    public void updateJober() {
        j.setNom(tfNom.getText());
        j.setPrenom(tfPrenom.getText());
        j.setDateDeNaissance(DSDatenaiss.getDate());
        j.setAdresse(tfAdresse.getText());
        j.setDescription(taDescription.getText());
        j.setNumTel(Integer.parseInt(tfNumtel.getText()));
        j.setSexe(sexe.getSelectedItem().toString());
        DelegationService ds = new DelegationService();
        Delegation d = ds.findByNom(delegation.getSelectedItem().toString());
        j.setDelegation(d);
        SpecialiteService ss = new SpecialiteService();
        Specialite s = ss.findByNom(specialite.getSelectedItem().toString());
        j.setSpecialite(s);
        js.update(j);

    }

    public Form getF() {
        return f1;
    }

    public void setF(Form f1) {
        this.f1 = f1;
    }

}
