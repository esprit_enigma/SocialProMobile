/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Avoir_Competance;
import Models.Competance;
import Models.Jober;
import Models.User;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Command;
import com.codename1.ui.Form;

import com.codename1.ui.Label;
import com.codename1.ui.Slider;

import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.util.Date;
import java.util.List;
import service.impl.AvoirCompetancesServices;
import service.impl.CompetanceService;
import service.impl.JoberService;

/**
 *
 * @author oudayblouza
 */
public class AjoutCompetance {

    Form f;
    private Resources theme;
    JoberService js = new JoberService();
    User u = LoginController.getCurrentUser();

    Jober j = js.findByUser(u);

    public void AjoutComp() {
        theme = UIManager.initFirstTheme("/theme");
        f = new Form("Ajout Competance", BoxLayout.y());

        Command cmd1 = new Command("Back");

        f.getToolbar().addCommandToOverflowMenu(cmd1);

        f.addCommandListener(evt
                -> {
            if (evt.getCommand() == cmd1) {

            }
        }
        );

        //f.setUIID("FormAjout");
        CompetanceService cs = new CompetanceService();
        List<Competance> lc = cs.getAll();

        ComboBox cbcomp = new ComboBox();

        for (Competance c : lc) {
            cbcomp.addItem(c.getNom());
        }
        Slider sl = new Slider();

        sl.setEditable(true);
        sl.setRenderPercentageOnTop(true);
        sl.setProgress(50);

        Button btnAjout = new Button("Ajouter");
        //btnAjout.setUIID("ButtonAjout");
        f.add(cbcomp);
        f.add(new Label(" "));
        f.add(new Label(" "));
        f.add(new Label(" "));

        f.add(sl);
        f.add(new Label(" "));

        f.add(btnAjout);
        btnAjout.addActionListener(l -> {
            AvoirCompetancesServices acs = new AvoirCompetancesServices();
            CompetanceService cc = new CompetanceService();
            Competance c = cc.findByNom(cbcomp.getSelectedItem().toString());

            Avoir_Competance ac = acs.findbyJoberComp(j, c);

            if (ac.getCompetance() != null) {
                ac.setNiveau(sl.getProgress());
                acs.update(ac);

            } else {
                Date d = new Date();
                ac.setDateAjout(d);
                ac.setCompetance(c);
                ac.setJober(j);
                ac.setNiveau(sl.getProgress());
                acs.add(ac);

            }
            CVController cv = new CVController();
            cv.getF().show();
        });

        f.show();

    }

}
