/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.User;
import com.codename1.components.ImageViewer;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Alaa
 */
public class LoginController {

    Form f;

    private Resources theme;
    static User currentUser;
    User u = new User();

    public LoginController() {

        theme = UIManager.initFirstTheme("/theme");
        Image img = theme.getImage("SocialProLogo.png");
        Image img1 = img.scaled(20, 20);
        ImageViewer imgv = new ImageViewer(img);

//        Resources css = Resources.openLayered("/theme.css");
//        UIManager.getInstance().addThemeProps(css.getTheme(css.getThemeResourceNames()[0]));
        UIBuilder ui = new UIBuilder();
        Label errorlabel = new Label("");
        //errorlabel.setUIID("errorlabel");

        f = ui.createContainer(theme, "Login").getComponentForm();
        //f.setUIID("BackgroundA");

        Container c = new Container(BoxLayout.y());

        TextField tfusername = new TextField("", "Username", 20, TextField.ANY);
        TextField tfpassword = new TextField("", "Password", 20, TextField.PASSWORD);

        Button btnlogin = new Button("Login");
        //btnlogin.setUIID("Round1");
        Button btnregister = new Button("Register");
        //btnregister.setUIID("Round2");
        Container ccc = new Container(BoxLayout.x());
        ccc.add(btnlogin).add(btnregister);

        c.add(imgv);
        c.add(tfusername);
        c.add(tfpassword);
        c.add(ccc);
        c.add(errorlabel);
        f.add(c);

        btnlogin.addActionListener(e -> {
            ConnectionRequest req = new ConnectionRequest();
            req.setUrl("http://localhost/PIMobile/src/PHP/login.php?username=" + tfusername.getText() + "&password=" + tfpassword.getText() + "");
            req.addResponseListener(new ActionListener<NetworkEvent>() {
                @Override
                public void actionPerformed(NetworkEvent evt) {
                    byte[] data = (byte[]) evt.getMetaData();
                    String s = new String(data);
                    if (s.equals("jober")) {
                        currentUser = SearchUsersByUsername(tfusername.getText());
                        ProfileJoberController pjc = new ProfileJoberController();
                        pjc.getF().show();
                    } else if (s.equals("entrep")) {
                        currentUser = SearchUsersByUsername(tfusername.getText());
//                        ProfileEntrep pe = new ProfileEntrep(tfusername.getText());
//                        pe.getF().show();
                    } else if (s.equals("prejober")) {
                        currentUser = SearchUsersByUsername(tfusername.getText());
                        InitProfileJoberController ipjc = new InitProfileJoberController();
                        ipjc.getF().show();
                    } else if (s.equals("preentrep")) {
                        currentUser = SearchUsersByUsername(tfusername.getText());
//                        ProfileEntrep pe = new ProfileEntrep(tfusername.getText());
//                        pe.getF().show();
                    } else if (s.equals("notenabledjober")) {
                        currentUser = SearchUsersByUsername(tfusername.getText());
                        EmailVerificationController evc = new EmailVerificationController(tfusername.getText(), currentUser.getEmail(), "jober");
                        evc.getF().show();
                    } else if (s.equals("notenabledentrep")) {
                        currentUser = SearchUsersByUsername(tfusername.getText());
                        EmailVerificationController evc = new EmailVerificationController(tfusername.getText(), currentUser.getEmail(), "entrep");
                        evc.getF().show();
                    } else if (s.equals("not found")) {
                        Dialog.show("Wrong Credentials", "Please check your username or password", "OK", "Cancel");
                    }
                }
            });
            NetworkManager.getInstance().addToQueueAndWait(req);
        });

        btnregister.addActionListener(e -> {
            RegisterController r = new RegisterController();
            r.getF().show();
        });

    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

    public static User getCurrentUser() {
        return currentUser;
    }

    public static void setCurrentUser(User currentUser) {
        LoginController.currentUser = currentUser;
    }

    public User SearchUsersByUsername(String username) {

        ConnectionRequest cr = new ConnectionRequest();
        cr.setUrl("http://localhost/PIMobile/src/PHP/GetUser.php");
        cr.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ArrayList<User> l = getUser(new String(cr.getResponseData()));
                for (User k : l) {
                    if (k.getUsername().equals(username)) {
                        u = k;
                    }
                }
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(cr);
        return u;
    }

    public ArrayList<User> getUser(String json) {
        ArrayList<User> listusers = new ArrayList<>();
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> etudiants = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) etudiants.get("user");

            for (Map<String, Object> obj : list) {
                int id = Integer.parseInt(obj.get("id").toString());
                String usern = obj.get("username").toString();
                String email = obj.get("email").toString();
                String password = obj.get("password").toString();
                String role = obj.get("roles").toString();
                listusers.add(new User(id, usern, usern, email, email, password, role));
            }

        } catch (IOException ex) {
        }
        return listusers;

    }

}
