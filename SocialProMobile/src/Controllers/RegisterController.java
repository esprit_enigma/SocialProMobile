/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.RadioButton;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;

/**
 *
 * @author Alaa
 */
public class RegisterController {

    Form f;
    String role;

    public RegisterController() {

        f = new Form(BoxLayout.y());
        //f.setUIID("BackgroundA");
        Container c = new Container(BoxLayout.y());
        Container c2 = new Container(BoxLayout.x());

        RadioButton rb1 = new RadioButton("Jobber");
        RadioButton rb2 = new RadioButton("Entreprise");
        new ButtonGroup(rb1, rb2);
        rb1.setSelected(true);
        TextField tfusername = new TextField("", "Username", 20, TextField.ANY);
        TextField tfemail = new TextField("", "Email", 20, TextField.EMAILADDR);
        TextField tfpass = new TextField("", "Password", 20, TextField.PASSWORD);
        TextField tfpass2 = new TextField("", "Repeat Password", 20, TextField.PASSWORD);
        Button btnRegister = new Button("Register");
        //btnRegister.setUIID("Round1");
        c2.add(rb1).add(rb2);
        c.add(tfusername);
        c.add(tfemail);
        c.add(tfpass);
        c.add(tfpass2);
        c.add(btnRegister);
        f.add(c2).add(c);

        f.getToolbar().addCommandToOverflowMenu("Back to Login", null, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                try {
                    LoginController login = new LoginController();
                    login.getF().showBack();
                } catch (Exception e) {

                }
            }
        });

        btnRegister.addActionListener(e -> {

            if (rb1.isSelected()) {
                role = "a:1:{i:0;s:10:\"ROLE_JOBER\";}";
            } else if (rb2.isSelected()) {
                role = "a:1:{i:0;s:15:\"ROLE_ENTREPRISE\";}";
            }

            if (tfusername.getText().equals("") || tfemail.getText().equals("") || tfpass.getText().equals("") || tfpass2.getText().equals("")) {
                System.out.println("Fill all fields");
                Dialog.show("Fill all fields", "Please Fill all fields", "OK", "Cancel");
            } else {
                if (!tfpass.getText().equals(tfpass2.getText())) {
                    System.out.println("Check passwords");
                    Dialog.show("Check passwords", "the password fields are not identical", "OK", "Cancel");
                } else {
                    ConnectionRequest con1 = new ConnectionRequest();
                    con1.setUrl("http://localhost/PIMobile/src/PHP/CheckUsername.php?username=" + tfusername.getText() + "");
                    con1.addResponseListener(new ActionListener<NetworkEvent>() {
                        @Override
                        public void actionPerformed(NetworkEvent evt) {
                            byte[] data = (byte[]) evt.getMetaData();
                            String s = new String(data);
                            if (s.equals("not used")) {
                                ConnectionRequest con2 = new ConnectionRequest();
                                con2.setUrl("http://localhost/PIMobile/src/PHP/register.php?username=" + tfusername.getText() + "&email=" + tfemail.getText() + "&password=" + tfpass.getText() + "&role=" + role);
                                con2.addResponseListener(new ActionListener<NetworkEvent>() {
                                    @Override
                                    public void actionPerformed(NetworkEvent evt2) {
                                        byte[] data2 = (byte[]) evt2.getMetaData();
                                        String d = new String(data2);
                                        if (d.equals("success")) {
                                            if (rb1.isSelected()) {
                                                EmailVerificationController ev = new EmailVerificationController(tfusername.getText(), tfemail.getText(), "jober");
                                                ev.getF().show();
                                            } else if (rb2.isSelected()) {
                                                EmailVerificationController ev = new EmailVerificationController(tfusername.getText(), tfemail.getText(), "entrep");
                                                ev.getF().show();
                                            }
                                        } else {
                                            System.out.println("error");
                                        }
                                    }
                                });
                                NetworkManager.getInstance().addToQueueAndWait(con2);
                            } else if (s.equals("used")) {
                                Dialog.show("username already used", "username already used", "OK", "Cancel");
                            }
                        }
                    });
                    NetworkManager.getInstance().addToQueueAndWait(con1);
                }
            }
        });
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

}
