/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import com.codename1.components.ImageViewer;
import com.codename1.components.InfiniteProgress;

import com.codename1.io.FileSystemStorage;
import com.codename1.io.MultipartRequest;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;

import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;

import com.codename1.ui.layouts.BoxLayout;

import java.io.IOException;

/**
 *
 * @author oudayblouza
 */
public class UploadPhotoController {

    EncodedImage enc1;

    public void init() throws IOException {
        Form f = new Form("Upload Image", BoxLayout.y());

        String URL1 = "http://localhost/images/ProfilePic/ouaz/ouaz.jpg";
        try {
            enc1 = EncodedImage.create("/load.png");
        } catch (IOException ex) {
            //Logger.getLogger(MyApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        Image imgServer = URLImage.createToStorage(enc1, "ouday", "http://localhost/images/ProfilePic/ouaz/ouaz.jpg");

        ImageViewer imgv = new ImageViewer(imgServer);
        Button btnupload = new Button("Upload img");
        f.add(btnupload);
        f.add(imgv);

        btnupload.addActionListener((evt) -> {
            Display.getInstance().openGallery(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev) {
                    if (ev != null && ev.getSource() != null) {
                        String filePath = (String) ev.getSource();
                        int fileNameIndex = filePath.lastIndexOf("/") + 1;
                        String fileName = filePath.substring(fileNameIndex);
                        Image img = null;
                        try {
                            img = Image.createImage(FileSystemStorage.getInstance().openInputStream(filePath));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        imgv.setImage(img);
                        System.out.println(filePath);

                        MultipartRequest cr = new MultipartRequest();
                        cr.setUrl("http://localhost/Upload.php");
                        cr.setPost(true);
                        String mime = "image/jpeg";
                        try {
                            cr.addData("file", filePath, mime);

                        } catch (IOException ex) {
                            Dialog.show("Erreur", ex.getMessage(), "Ok", null);
                        }
                        cr.setFilename("file", "ouday.jpg");//any unique name you want
                        //Storage.getInstance().deleteStorageFile("Mahot");
                        InfiniteProgress prog = new InfiniteProgress();
                        Dialog dlg = prog.showInifiniteBlocking();
                        cr.setDisposeOnCompletion(dlg);
                        NetworkManager.getInstance().addToQueueAndWait(cr);

                        //any unique name you want
                        NetworkManager.getInstance().addToQueueAndWait(cr);
                        // Do something, add to List
                    }
                }
            }, Display.GALLERY_IMAGE);
        });

        f.show();

//            update_com.add(logo); 
    }
}
