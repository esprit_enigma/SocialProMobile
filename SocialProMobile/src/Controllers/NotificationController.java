/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;


import Models.Notification;
import Models.User;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Alaa
 */
public class NotificationController{

    Form f;

    public NotificationController() {
        f = new Form(BoxLayout.y());
        //f.setUIID("BackgroundA");
        Container c = new Container(BoxLayout.y());

        if (getUserNotifications(LoginController.getCurrentUser()) != null) {
            ArrayList<Notification> listnotifs = getUserNotifications(LoginController.getCurrentUser());
            for (Notification n : listnotifs) {
                Label l = new Label(n.getText());
                c.add(l);
            }
        }

        f.getToolbar().addCommandToSideMenu("Mon Profile", null, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                ProfileJoberController pj = new ProfileJoberController();
                pj.getF().show();
            }
        });

        f.getToolbar().addCommandToSideMenu("List Users", null, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                ListUsersController lu = new ListUsersController();
                lu.getF().show();
            }
        });
        f.add(c);

    }

    public ArrayList<Notification> getUserNotifications(User u) {
        ArrayList<Notification> listnotif = new ArrayList<>();

        ConnectionRequest cr = new ConnectionRequest();
        cr.setUrl("http://localhost/PIMobile/src/PHP/GetUserNotifs.php");
        cr.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ArrayList<Notification> l = getNotifs(new String(cr.getResponseData()), u);
                for (Notification n : l) {
                    listnotif.add(n);
                }
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(cr);

        return listnotif;
    }

    public ArrayList<Notification> getNotifs(String json, User user) {
        ArrayList<Notification> listnotification = new ArrayList<>();
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> etudiants = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) etudiants.get("notif");

            for (Map<String, Object> obj : list) {
                if (Integer.parseInt(obj.get("user_id").toString()) == user.getId()) {
                    int id = Integer.parseInt(obj.get("id").toString());
                    String text = obj.get("text").toString();
                    String state = obj.get("state").toString();
                    listnotification.add(new Notification(id, user, text, state));
                }
            }
        } catch (IOException ex) {
        }
        return listnotification;

    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

}

