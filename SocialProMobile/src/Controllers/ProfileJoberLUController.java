/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Delegation;
import Models.Jober;
import Models.Nature;
import Models.Specialite;
import Models.User;
import Models.Ville;
import com.codename1.components.SpanLabel;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.list.DefaultListModel;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import java.util.List;
import service.impl.DelegationService;
import service.impl.JoberService;
import service.impl.NatureService;
import service.impl.SpecialiteService;
import service.impl.VilleService;

/**
 *
 * @author Alaa
 */
public class ProfileJoberLUController {

    Form f;

    
    
    int lock = 1;
    String x = "Lock/Unlock";
    TextField tfNom = new TextField();
    TextField tfPrenom = new TextField();
    Picker DSDatenaiss = new Picker();
    TextField tfAdresse = new TextField();
    TextField taDescription = new TextField();
    TextField tfNumtel = new TextField();
    ComboBox sexe = new ComboBox();
    ComboBox ville = new ComboBox();
    ComboBox delegation = new ComboBox();
    ComboBox nature = new ComboBox();
    ComboBox specialite = new ComboBox();
    Command cmd, cmd1;
    Image img, img2, img5, img7, img9;
    private Resources theme;
    Form f1;
    User u = LoginController.getCurrentUser();    
    Button btnupsate;

    public ProfileJoberLUController(User user) {
        theme = UIManager.initFirstTheme("/theme");
        JoberService js = new JoberService();
        Jober j = js.findByUser(user);
        
        
        f = new Form("Mon Profil", BoxLayout.y());
        //f.setUIID("FormCV");
        
        f.getToolbar().addCommandToRightBar("Back to User List",null,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                ListUsersController luc = new ListUsersController();
                luc.getF().showBack();
            }
        });
        
        Button btnrelation = new Button("");
        
        ConnectionRequest cr = new ConnectionRequest();
        cr.setUrl("http://localhost/PIMobile/src/PHP/CheckRelation.php?follower=" + LoginController.getCurrentUser().getId() + "&followed=" + user.getId() + "");
        cr.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                byte[] data = (byte[]) evt.getMetaData();
                String s = new String(data);
                if (s.equals("found")) {
                    btnrelation.setText("desabonneé");
                } else if (s.equals("not found")) {
                    btnrelation.setText("abonnée");
                }
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(cr);
        
        btnrelation.addActionListener(e -> {

            if (btnrelation.getText().equals("abonnée")) {
                ConnectionRequest con1 = new ConnectionRequest();
                con1.setUrl("http://localhost/PIMobile/src/PHP/AddRelation?follower=" + LoginController.getCurrentUser().getId() + "&followed=" + user.getId() + "");
                con1.addResponseListener(new ActionListener<NetworkEvent>() {
                    @Override
                    public void actionPerformed(NetworkEvent evt) {
                        byte[] data = (byte[]) evt.getMetaData();
                        String s = new String(data);
                        if (s.equals("success")) {
                            ConnectionRequest cc = new ConnectionRequest();
                            cc.setUrl("http://localhost/PIMobile/src/PHP/AddNotif?user_id=" + user.getId() + "&text=" + LoginController.getCurrentUser().getUsername() + " followed you");
                            cc.addResponseListener(new ActionListener<NetworkEvent>() {
                                @Override
                                public void actionPerformed(NetworkEvent evt) {
                                    byte[] data = (byte[]) evt.getMetaData();
                                    String s = new String(data);
                                    if (s.equals("success")) {
                                        btnrelation.setText("desabonneé");
                                    }
                                }
                            });
                            NetworkManager.getInstance().addToQueueAndWait(cc);
                        } else {
                            System.out.println("error");
                        }
                    }
                });
                NetworkManager.getInstance().addToQueueAndWait(con1);

            } else if (btnrelation.getText().equals("desabonneé")) {
                ConnectionRequest con2 = new ConnectionRequest();
                con2.setUrl("http://localhost/PIMobile/src/PHP/DeleteRelation?follower=" + LoginController.getCurrentUser().getId() + "&followed=" + user.getId() + "");
                con2.addResponseListener(new ActionListener<NetworkEvent>() {
                    @Override
                    public void actionPerformed(NetworkEvent evt) {
                        byte[] data = (byte[]) evt.getMetaData();
                        String s = new String(data);
                        if (s.equals("success")) {
                            btnrelation.setText("abonnée");
                        } else {
                            System.out.println("error");
                        }
                    }
                });
                NetworkManager.getInstance().addToQueueAndWait(con2);
            }
        });
        
        
        
        SpanLabel nom = new SpanLabel("Nom : "+j.getNom());
        SpanLabel prenom = new SpanLabel("Prenom : "+j.getPrenom());
        SpanLabel DateNaiss = new SpanLabel("Date Naiss : "+j.getDateDeNaissance().toString());
        SpanLabel adresse = new SpanLabel("Adresse : "+j.getAdresse());
        SpanLabel description = new SpanLabel("Description : "+j.getDescription());
        SpanLabel NumTel = new SpanLabel("Num Tel : "+j.getNumTel());
        SpanLabel Sexe = new SpanLabel("Sexe : "+j.getSexe());
        SpanLabel Delegation = new SpanLabel("Delegation : "+j.getDelegation().getNom());
        SpanLabel Specialite = new SpanLabel("Specialité : "+j.getSpecialite().getNom());
        
        f.add(btnrelation).add(nom).add(prenom).add(DateNaiss).add(Sexe).add(adresse).add(NumTel).add(Delegation).add(Specialite).add(description);
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

}
