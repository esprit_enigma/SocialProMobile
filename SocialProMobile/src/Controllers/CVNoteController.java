/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Avoir_Competance;
import Models.Experience;
import Models.Formation;
import Models.Jober;
import Models.Projet;
import Models.Stage;
import Models.User;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Slider;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.util.List;
import service.impl.AvoirCompetancesServices;
import service.impl.ExperienceService;
import service.impl.FormationService;
import service.impl.JoberService;
import service.impl.ProjetService;
import service.impl.StageService;

/**
 *
 * @author Alaa
 */
public class CVNoteController {
    Form f;
    Image img5, img7, img9;
    private Resources theme;
    JoberService js = new JoberService();
    User u = LoginController.getCurrentUser();

    String messagef = "";
    String messagec = "";
    String messagee = "";
    String messagep = "";
    String messages = "";

    int notecv = 0;
    int noteexperience = 0;
    int notecompetance = 0;
    int noteformation = 0;
    int noteprojet = 0;
    int notestage = 0;
    int cv = 1;
    Jober j = js.findByUser(u);

    public CVNoteController() {
        moncv();
        theme = UIManager.initFirstTheme("/theme");
        f = new Form("Statistiques ", BoxLayout.y());
        Container ctn = new Container(BoxLayout.y());
        //f.setUIID("FormCV");
        img5 = theme.getImage("projet.png");
        Image img6 = img5.scaled(20, 20);

        f.getToolbar().addCommandToSideMenu("Mon Profil", img6, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                ProfileJoberController pc = new ProfileJoberController();
                pc.getF().show();
            }

        });
        img7 = theme.getImage("experience.png");
        Image img8 = img7.scaled(20, 20);
        f.getToolbar().addCommandToSideMenu("Mon CV", img8, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                CVController cc = new CVController();
                cc.getF().show();
            }

        });

        img9 = theme.getImage("competance.png");
        Image img10 = img9.scaled(20, 20);
        f.getToolbar().addCommandToSideMenu("Statistiques CV", img10, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {

                CVNoteController cvn = new CVNoteController();
                cvn.getF().show();
            }

        });
        Label l1 = new Label("Note Formation");
        //l1.setUIID("textf");

        Slider sl = new Slider();

        sl.setEditable(false);
        sl.setRenderPercentageOnTop(true);
        sl.setProgress(noteformation * 3);

        Label l2 = new Label("Note Experience");
        //l2.setUIID("textf");
        Slider slE = new Slider();
        slE.setEditable(false);
        slE.setRenderPercentageOnTop(true);
        slE.setProgress(noteexperience * 3);

        Label l3 = new Label("Note Stage");
        //l3.setUIID("textf");
        Slider slS = new Slider();
        slS.setEditable(false);
        slS.setRenderPercentageOnTop(true);
        slS.setProgress(notestage * 10);

        Label l4 = new Label("Note Projet");
        //l4.setUIID("textf");
        Slider slP = new Slider();
        slP.setEditable(false);
        slP.setRenderPercentageOnTop(true);
        slP.setProgress(noteprojet * 10);

        Label l5 = new Label("Note Competances");
        //l5.setUIID("textf");
        Slider slC = new Slider();
        slC.setEditable(false);
        slC.setRenderPercentageOnTop(true);
        slC.setProgress(notecompetance * 5);

        Label l6 = new Label("Note Cv Total");
        //l5.setUIID("textf");
        Slider slCV = new Slider();
        slCV.setEditable(false);
        slCV.setRenderPercentageOnTop(true);
        slCV.setProgress(notecv);

        ctn.add(l5);
        ctn.add(new Label(messagec));
        ctn.add(slC);
        ctn.add(new Label(" "));

        ctn.add(l1);
        ctn.add(new Label(messagef));
        ctn.add(sl);
        ctn.add(new Label(" "));

        ctn.add(l2);
        ctn.add(new Label(messagee));
        ctn.add(slE);
        ctn.add(new Label(" "));

        ctn.add(l3);
        ctn.add(new Label(messages));
        ctn.add(slS);
        ctn.add(new Label(" "));

        ctn.add(l4);
        ctn.add(new Label(messagep));
        ctn.add(slP);
        ctn.add(new Label(" "));

        ctn.add(l6);
        if (cv == 0) {
            ctn.add(new Label("Votre cv est trés faible pour etre génerer"));
        }
        ctn.add(slCV);
        ctn.add(new Label(" "));
        f.add(ctn);
    }

    public void moncv() {

        AvoirCompetancesServices acs = new AvoirCompetancesServices();
        List<Avoir_Competance> competances = acs.findbyJober(j);

        FormationService fs = new FormationService();
        List<Formation> formations = fs.findByJober(j);

        StageService ss = new StageService();
        List<Stage> stages = ss.findByJober(j);

        ExperienceService es = new ExperienceService();
        List<Experience> experiences = es.findByJober(j);

        ProjetService ps = new ProjetService();
        List<Projet> projets = ps.findByJober(j);

        if (formations.size() == 0) {
            messagef = "Vous n'avez aucune formation, Cela nuira a votre CV ";

        } else if (formations.size() <= 2) {
            notecv = notecv + 10;
            noteformation = 10;
        } else if ((formations.size() <= 4) && (formations.size() > 2)) {
            notecv = notecv + 20;
            noteformation = 20;
        } else if (formations.size() > 4) {
            notecv = notecv + 30;
            noteformation = 30;
        }

        if (experiences.size() == 0) {
            messagee = "Vous n'avez aucune experiance,Votre Cv est faible  ";

        } else {
            int nbannee = 0;
            for (int i = 0; i < experiences.size(); i++) {

                int DAY = 24 * 60 * 60 * 1000;

                int daysBetween = (int) (Math.abs(experiences.get(i).getDateFin().getTime() - experiences.get(i).getDateDebut().getTime()) / DAY);

                nbannee = nbannee + (daysBetween / 356);

                System.out.println(nbannee + " temoin ");

            }

            if (nbannee <= 1) {
                notecv = notecv + 2;
                noteexperience = 2;
            } else if (nbannee == 2) {
                notecv = notecv + 5;
                noteexperience = 5;
            } else if ((nbannee > 2) && (nbannee <= 5)) {
                notecv = notecv + 10;
                noteexperience = 10;
            } else if ((nbannee > 5) && (nbannee <= 10)) {
                notecv = notecv + 15;
                noteexperience = 15;
            } else if ((nbannee > 10) && (nbannee < 20)) {
                notecv = notecv + 23;
                noteexperience = 23;
            } else if (nbannee >= 20) {
                notecv = notecv + 30;
                noteexperience = 30;
            }
        }

        if (stages.size() == 0) {
            messages = "Vous n'avez aucun stage  ";

        } else if (stages.size() <= 2) {
            notecv = notecv + 4;
            notestage = 4;
        } else if ((stages.size() <= 4) && (stages.size() > 2)) {
            notecv = notecv + 7;
            notestage = 7;
        } else if (stages.size() > 4) {
            notecv = notecv + 10;
            notestage = 10;
        }

        if (projets.size() == 0) {
            messagep = "Vous n'avez travailler sur aucun Projet ?  ";

        } else if (projets.size() <= 2) {
            notecv = notecv + 4;
            noteprojet = 4;
        } else if ((projets.size() <= 4) && (projets.size() > 2)) {
            notecv = notecv + 7;
            noteprojet = 7;
        } else if (projets.size() > 4) {
            notecv = notecv + 10;
            noteprojet = 10;
        }

        if (competances.size() == 0) {
            messagec = "Vous n'avez pas de compétances, votre cv en Souffrira ";

        } else if (competances.size() <= 2) {
            notecv = notecv + 2;
            notecompetance = 2;
        } else if ((competances.size() <= 6) && (competances.size() > 2)) {
            notecv = notecv + 10;
            notecompetance = 10;
        } else if (competances.size() > 6) {
            notecv = notecv + 20;
            notecompetance = 20;
        }

        if ((formations.size() == 0 && (experiences.size() == 0) & stages.size() == 0)) {
            cv = 0;
        }
        int reste = 100 - notecv;

    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

}
