/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Formation;
import Models.Jober;
import Models.User;
import com.codename1.components.ToastBar;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import java.util.Date;
import service.impl.FormationService;
import service.impl.JoberService;

/**
 *
 * @author oudayblouza
 */
public class AjoutFormation {

    Form f;
    private Resources theme;
    JoberService js = new JoberService();
    User u = LoginController.getCurrentUser();
    Jober j = js.findByUser(u);

    public void AjoutForma() {
        theme = UIManager.initFirstTheme("/theme");
        f = new Form("Ajout Formation", BoxLayout.y());

        Command cmd1 = new Command("Back");

        f.getToolbar().addCommandToOverflowMenu(cmd1);

        f.addCommandListener(evt
                -> {
            if (evt.getCommand() == cmd1) {
                CVController cv = new CVController();
                cv.getF().show();
            }
        }
        );

        //f.setUIID("FormAjout");
        Container ctn = new Container(BoxLayout.y());
        //ctn.setUIID("ContainerTitle");

        TextField orga = new TextField();
        orga.setHint("Institut");
        //orga.setUIID("textf");

        TextField poste = new TextField();
        poste.setHint("Diplome");
        //poste.setUIID("textf");

        TextField desc = new TextField();
        desc.setHint("Description");
        //desc.setUIID("textf");

        Label l1 = new Label("Date Debut :");
        Label l2 = new Label("Date Fin :");

        Picker datedeb = new Picker();
        //datedeb.setUIID("textf");

        Picker datefin = new Picker();
        //datefin.setUIID("textf");
        Button ajou = new Button("Ajouter");
        //ajou.setUIID("ButtonAjout");
        ctn.add(orga);
        ctn.add(poste);
        ctn.add(l1);
        ctn.add(datedeb);
        ctn.add(l2);
        ctn.add(datefin);
        ctn.add(desc);
        f.add(ctn);
        f.add(ajou);
        f.show();

        ajou.addActionListener(e -> {

            if ((!(orga.getText()).equals("")) && (!(poste.getText()).equals("")) && (!(desc.getText()).equals(""))) {
                Formation exp = new Formation();
                exp.setInstitut(orga.getText());
                exp.setDiplome(poste.getText());
                exp.setDescription(desc.getText());
                exp.setDateAjout(datedeb.getDate());
                exp.setDateFin(datefin.getDate());
                exp.setDateAjout(new Date());
                exp.setJober(j);
                FormationService es = new FormationService();
                es.add(exp);
                CVController cv = new CVController();
                cv.getF().show();
            } else {
                ToastBar.showErrorMessage("Veuillez Remplir tout les champs correctement");
            }
        });
    }
}
