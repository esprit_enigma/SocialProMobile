/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.User;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Alaa
 */
public class ListUsersController {

    Form f;
    ArrayList<User> allUsers;
    ArrayList<User> jobbers;
    ArrayList<User> entreps;

    public ListUsersController() {
        f = new Form(BoxLayout.y());
        //f.setUIID("BackgroundA");
        Container all = new Container(BoxLayout.y());
        Container c = new Container(BoxLayout.x());
        Container jobberContainer = new Container(BoxLayout.y());
        Container entrepContainer = new Container(BoxLayout.y());
        Container showuserContainer = new Container(BoxLayout.y());

        RadioButton rb1 = new RadioButton("Jobbers");
        RadioButton rb2 = new RadioButton("Entreprises");
        ButtonGroup bg = new ButtonGroup(rb1, rb2);
        rb1.setSelected(true);
        c.add(rb1).add(rb2);
        all.add(c).add(showuserContainer);

        f.getToolbar().addCommandToSideMenu("Profile", null, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {

                if (LoginController.getCurrentUser().getRoles().equals("a:1:{i:0;s:10:\"ROLE_JOBER\";}")) {
                    ProfileJoberController pc = new ProfileJoberController();
                    pc.getF().show();
                } else {
//                    ProfileEntrepLUController pe = new ProfileEntrepLUController(LoginController.getCurrentUser().getUsername());
//                    pe.getF().show();
                }
            }
        });

        f.getToolbar().addCommandToSideMenu("Notification", null, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                NotificationController n = new NotificationController();
                n.getF().show();
            }
        });

        jobbers = new ArrayList<>();
        entreps = new ArrayList<>();
        ConnectionRequest cr = new ConnectionRequest();
        cr.setUrl("http://localhost/PIMobile/src/PHP/GetUser.php");
        cr.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                allUsers = getAllUser(new String(cr.getResponseData()));
                for (User u : allUsers) {
                    if (u.getRoles().equals("a:1:{i:0;s:10:\"ROLE_JOBER\";}")) {
                        jobbers.add(u);
                    } else if (u.getRoles().equals("a:1:{i:0;s:15:\"ROLE_ENTREPRISE\";}")) {
                        entreps.add(u);
                    }
                }
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(cr);

        for (User u : jobbers) {
            Container cc = new Container(BoxLayout.x());
            Label name = new Label(u.getUsername());
            Button b = new Button("Open Profil");
            cc.add(name).add(b);
            jobberContainer.add(cc);
            b.addActionListener(e -> {
                ProfileJoberLUController pjs = new ProfileJoberLUController(u);
                pjs.getF().show();
            });
        }
        for (User u : entreps) {
            Container cc = new Container(BoxLayout.x());
            Label name = new Label(u.getUsername());
            Button b = new Button("Open Profil");
            cc.add(name).add(b);
            entrepContainer.add(cc);
            b.addActionListener(e -> {
//                ProfileEntrepShown pes = new ProfileEntrepShown(u, currentUser);
//                pes.getF().show();
            });
        }

        showuserContainer.add(jobberContainer);

        rb1.addActionListener(e -> {
            if (rb1.isSelected()) {
                showuserContainer.removeAll();
                showuserContainer.add(jobberContainer);
                f.refreshTheme();
            }
        });
        rb2.addActionListener(e -> {
            if (rb2.isSelected()) {
                showuserContainer.removeAll();
                showuserContainer.add(entrepContainer);
                f.refreshTheme();
            }
        });

        f.add(all);
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

    public ArrayList<User> getAllUser(String json) {
        ArrayList<User> listusers = new ArrayList<>();
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> etudiants = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) etudiants.get("user");

            for (Map<String, Object> obj : list) {
                int id = Integer.parseInt(obj.get("id").toString());
                String usern = obj.get("username").toString();
                String email = obj.get("email").toString();
                String password = obj.get("password").toString();
                String role = obj.get("roles").toString();
                listusers.add(new User(id, usern, usern, email, email, password, role));
            }
        } catch (IOException ex) {
        }
        return listusers;
    }
}
