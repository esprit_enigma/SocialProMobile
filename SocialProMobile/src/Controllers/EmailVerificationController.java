/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.sun.mail.smtp.SMTPTransport;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Alaa
 */
public class EmailVerificationController {

    Form f;

    public EmailVerificationController(String username, String email, String role) {

        Random random = new Random();
        int n = random.nextInt(9000) + 1000;
        //sendMail(n);

        f = new Form(BoxLayout.y());
        Label l = new Label("Verification code sent to email");
        TextField tfcode = new TextField("", "Code", 20, TextField.ANY);
        Button btnok = new Button("Validate");

        btnok.addActionListener(e -> {
//            if (Integer.parseInt(tfcode.getText()) == n) {
            System.out.println(username);
            ConnectionRequest con = new ConnectionRequest();
            con.setUrl("http://localhost/PIMobile/src/PHP/EnableUser.php?username=" + username + "");
            con.addResponseListener(new ActionListener<NetworkEvent>() {
                @Override
                public void actionPerformed(NetworkEvent evt) {
                    byte[] data = (byte[]) evt.getMetaData();
                    String s = new String(data);
                    if (s.equals("success")) {
                        if (role.equals("jober")) {
                            InitProfileJoberController ipjc = new InitProfileJoberController();
                            ipjc.getF().show();
                        } else {
//                            InitProfileEntrepriseController ipec = new InitProfileEntrepriseController();
//                            ipec.getF().show();
                        }
                    } else {
                        Dialog.show("erreur", "error when enabling the user", "OK", "Cancel");
                    }

                }
            });

//            } else {
//                Dialog.show("Wrong code", "Please enter the valid code", "OK", "Cancel");
//            }
            NetworkManager.getInstance().addToQueueAndWait(con);
        });

        f.add(l).add(tfcode).add(btnok);

    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

    public void sendMail(int n) {
        try {

            Properties props = new Properties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtps.host", "smtp.gmail.com");
            props.put("mail.smtps.auth", "true");
            Session session = Session.getInstance(props, null);

            MimeMessage msg = new MimeMessage(session);

            msg.setFrom(new InternetAddress("Social Pro <my_email@myDomain.com>"));
            msg.setRecipients(javax.mail.Message.RecipientType.TO, "alaadhi93@gmail.com");
            msg.setSubject("Email Valiation");
            msg.setSentDate(new Date(System.currentTimeMillis()));

            String txt = "the validation code is:" + n + "";

            msg.setText(txt);
            System.out.println(txt);
            SMTPTransport st = (SMTPTransport) session.getTransport("smtps");
            st.connect("smtp.gmail.com", "SocialPro0@gmail.com", "enigmaEsprit");
            st.sendMessage(msg, msg.getAllRecipients());

            System.out.println("ServerResponse : " + st.getLastServerResponse());
        } catch (MessagingException ex) {
        }
    }

}
