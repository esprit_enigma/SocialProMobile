/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Delegation;
import Models.Jober;
import Models.Nature;
import Models.Specialite;
import Models.User;
import Models.Ville;
import com.codename1.components.ToastBar;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Command;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.list.DefaultListModel;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import java.util.Date;
import java.util.List;
import service.impl.DelegationService;
import service.impl.JoberService;
import service.impl.NatureService;
import service.impl.SpecialiteService;
import service.impl.UserService;
import service.impl.VilleService;

/**
 *
 * @author Alaa
 */
public class InitProfileJoberController {

    Form f;
    private Resources theme;
    User u = LoginController.getCurrentUser();

    public InitProfileJoberController() {
        f = new Form("Init Profile", BoxLayout.y());
        //f.setUIID("FormCV");
        theme = UIManager.initFirstTheme("/theme");

       // Image img2 = theme.getImage("logout.png");
        //Image img3 = img2.scaled(20, 20);

        Command cmd1 = new Command("Logout");
       // cmd1.setIcon(img3);
        f.getToolbar().addCommandToOverflowMenu(cmd1);

        f.addCommandListener(evt
                -> {
            if (evt.getCommand() == cmd1) {
                LoginController l = new LoginController();
                l.getF().show();
            }
        }
        );
        TextField tfNom = new TextField("", "Nom");
        //tfNom.setUIID("textf");
        TextField tfPrenom = new TextField("", "Prenom");
        //tfPrenom.setUIID("textf");

        Picker DSDatenaiss = new Picker();
        //DSDatenaiss.setUIID("textf");
        TextField tfAdresse = new TextField("", "Adresse");
        //tfAdresse.setUIID("textf");
        TextField tfDescription = new TextField("", "Description");
        //tfDescription.setUIID("textf");
        TextField tfNumtel = new TextField("", "Numtel", 4, TextField.NUMERIC);
        //tfNumtel.setUIID("textf");

        ComboBox cbSexe = new ComboBox();

        cbSexe.addItem("Homme");
        cbSexe.addItem("Femme");
        cbSexe.addItem("Autre");
        cbSexe.setHint("Sexe");

        ComboBox cbv = new ComboBox();
        VilleService vs = new VilleService();
        List<Ville> ls = vs.getAll();
        for (Ville v : ls) {
            cbv.addItem(v.getNom());
        }

        ComboBox cbd = new ComboBox();
        cbd.setHint("Delegation");
        cbv.addActionListener((evt) -> {
            DefaultListModel dlm = (DefaultListModel) cbd.getModel();
            dlm.removeAll();

            String ville = cbv.getSelectedItem().toString();
            VilleService vs1 = new VilleService();

            Ville nv = vs.findByNom(ville);

            DelegationService vd = new DelegationService();
            List<Delegation> ls1 = vd.findBynv(nv);
            for (Delegation v : ls1) {
                cbd.addItem(v.getNom());
            }

        });

        ComboBox cbn = new ComboBox();
        NatureService ns = new NatureService();
        List<Nature> lsn = ns.getAll();
        for (Nature n : lsn) {
            cbn.addItem(n.getNom());
        }
        ComboBox cbs = new ComboBox();
        cbs.setHint("Specialite");
        cbn.addActionListener((evt) -> {
            DefaultListModel dlm = (DefaultListModel) cbs.getModel();
            dlm.removeAll();

            String nature = cbn.getSelectedItem().toString();
            NatureService ns1 = new NatureService();
            Nature n = ns1.findByNom(nature);
            SpecialiteService ss = new SpecialiteService();
            List<Specialite> ls2 = ss.findBynv(n);

            for (Specialite v : ls2) {
                cbs.addItem(v.getNom());

            }

        });
        f.add(tfNom);
        f.add(tfPrenom);
        f.add(DSDatenaiss);
        f.add(tfAdresse);
        f.add(tfDescription);
        f.add(tfNumtel);
        f.add(cbSexe);
        f.add(cbv);
        f.add(cbd);
        f.add(cbn);
        f.add(cbs);
        Button btnOk = new Button("Insert");
        f.add(btnOk);

        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                Date d1 = new Date();
                int DAY = 24 * 60 * 60 * 1000;

                int daysBetween = (int) (Math.abs(DSDatenaiss.getDate().getTime() - d1.getTime()) / DAY);

                int nbannee = daysBetween / 356;

                if (nbannee > 15) {

                    if ((!(tfNom.getText().equals(""))) && (!(tfPrenom.getText().equals(""))) && (!(tfAdresse.getText().equals(""))) && (!(tfDescription.getText().equals(""))) && (!(tfNumtel.getText().equals(""))) && (cbd.getSelectedItem() != null) && (cbs.getSelectedItem() != null)) {

                        DelegationService ds = new DelegationService();
                        Delegation d = ds.findByNom(cbd.getSelectedItem().toString());
                        System.out.println("id del " + d.getId());
                        SpecialiteService ss = new SpecialiteService();
                        Specialite s = ss.findByNom(cbs.getSelectedItem().toString());
                        System.out.println("id spec " + s.getId());

                        Jober j = new Jober(d, s, tfNom.getText(), tfPrenom.getText(), DSDatenaiss.getDate(), tfAdresse.getText(), cbSexe.getSelectedItem().toString(), tfDescription.getText(), Integer.parseInt(tfNumtel.getText()), d1, u);
                        JoberService js = new JoberService();

                        js.add(j);

                        System.out.println("Votre Compte A éte créer avec succés");
                        tfNom.setText("");
                        tfPrenom.setText("");
                        DSDatenaiss.setDate(d1);
                        tfAdresse.setText("");
                        tfDescription.setText("");
                        tfNumtel.setText("");
                        cbSexe.setSelectedIndex(0);

                        UserService us = new UserService();
                        us.changerole(u);
                        
                        ProfileJoberController pjc = new ProfileJoberController();
                        pjc.getF().show();
                    } else {
                        ToastBar.showErrorMessage("Veuillez Remplir tout les champs correctement");
                    }

                } else {
                    ToastBar.showErrorMessage("Votre age doit etre superieure a 15 ans");
                }
            }
        });

    }

    public Form getF() {
        return  f;
    }

    public void setF(Form f) {
        this.f = f;
    }

}
