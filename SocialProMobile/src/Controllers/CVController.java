/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Avoir_Competance;
import Models.Experience;
import Models.Formation;
import Models.Jober;
import Models.Projet;
import Models.Stage;
import Models.User;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Slider;
import com.codename1.ui.SwipeableContainer;
import com.codename1.ui.Tabs;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import service.impl.AvoirCompetancesServices;
import service.impl.ExperienceService;
import service.impl.FormationService;
import service.impl.JoberService;
import service.impl.ProjetService;
import service.impl.StageService;

/**
 *
 * @author Alaa
 */
public class CVController {

    Form f;

    int x = 0;
    Image img, img2;
    private Resources theme;
    JoberService js = new JoberService();
    User u = LoginController.getCurrentUser();

    Jober j = js.findByUser(u);
    Tabs t;
    Image img5, img7, img9;

    public CVController() {
        theme = UIManager.initFirstTheme("/theme");
        f = new Form("Mon Profil", BoxLayout.y());

        img5 = theme.getImage("projet.png");
        Image img6 = img5.scaled(20, 20);

        f.getToolbar().addCommandToSideMenu("Mon Profil", img6, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                ProfileJoberController pjc = new ProfileJoberController();
                pjc.getF().show();
            }

        });
        img7 = theme.getImage("experience.png");
        Image img8 = img7.scaled(20, 20);
        f.getToolbar().addCommandToSideMenu("Mon CV", img8, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                CVController cc = new CVController();
                cc.getF().show();
            }
        });

        img9 = theme.getImage("competance.png");
        Image img10 = img9.scaled(20, 20);
        f.getToolbar().addCommandToSideMenu("Statistiques CV", img10, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                CVNoteController cvn = new CVNoteController();
                cvn.getF().show();
            }

        });

        //f.setUIID("FormCV");
        t = new Tabs();
        f.add(t);
        affichComp();
        affichFormation();
        affichExperience();
        affichStage();
        affichProjets();
    }

    public void affichComp() {
        img = theme.getImage("competance.png");
        Image img1 = img.scaled(20, 20);
        img2 = theme.getImage("add.png");
        Image img3 = img2.scaled(20, 20);

        Container concomp = new Container(BoxLayout.y());
        Container comptitle = new Container(new GridLayout(1, 2));

        //concomp.setUIID("ContainerTitle");
        //comptitle.setUIID("ContainerTitle");
        Label comp = new Label("Compet :");

        comp.getAllStyles().setUnderline(true);

        comp.setIcon(img1);
        comptitle.add(comp);
        Button addcomp = new Button();
        addcomp.setIcon(img3);
        //addcomp.setUIID("ouday");
        addcomp.addActionListener(lc1 -> {
            AjoutCompetance ac = new AjoutCompetance();
            ac.AjoutComp();
        });
        addcomp.getAllStyles().setAlignment(addcomp.RIGHT);
        comptitle.add(addcomp);
        comptitle.setFlatten(true);
        concomp.add(comptitle);

        Container concompetances = new Container(BoxLayout.y());
        //concompetances.setUIID("ContainerTitle");
        concompetances.add(new Label(" "));
        AvoirCompetancesServices acs = new AvoirCompetancesServices();

        List<Avoir_Competance> lstc = acs.findbyJober(j);
        for (Avoir_Competance ac : lstc) {
            Slider scomp = new Slider();
            scomp.setEditable(false);
            scomp.setProgress(ac.getNiveau());
            scomp.setText(ac.getCompetance().getNom() + " " + ac.getNiveau() + "%");

            concompetances.add(scomp);
            concompetances.add(new Label(" "));

        }

        comp.addPointerPressedListener(l -> {
            if (x == 0) {
                concompetances.setVisible(false);
                f.refreshTheme();
                x = 1;
            } else {
                concompetances.setVisible(true);
                f.refreshTheme();
                x = 0;
            }

        });
        concomp.add(concompetances);
        t.addTab("", concomp);

    }

    public void affichFormation() {
        img = theme.getImage("formation.png");
        Image img1 = img.scaled(20, 20);
        img2 = theme.getImage("add.png");
        Image img3 = img2.scaled(20, 20);

        Container confor = new Container(BoxLayout.y());
        Container fortitle = new Container(new GridLayout(1, 2));

        //fortitle.setUIID("ContainerTitle");
        //confor.setUIID("ContainerTitle");
        Label formation = new Label("Formation");

        formation.getAllStyles().setUnderline(true);
        formation.setIcon(img1);
        fortitle.add(formation);

        Button addfor = new Button();
        addfor.setUIID("ouday");
        addfor.setIcon(img3);

        addfor.getAllStyles().setAlignment(addfor.RIGHT);
        addfor.addActionListener(lc1 -> {
            AjoutFormation ae = new AjoutFormation();
            ae.AjoutForma();
        });
        fortitle.add(addfor);
        confor.add(fortitle);

        Container conformations = new Container(BoxLayout.y());

        FormationService fs = new FormationService();

        List<Formation> lstf = fs.findByJober(j);
        for (Formation forma : lstf) {
            Container formationbox = new Container(BoxLayout.y());
            //formationbox.setUIID("ContainerTitle");
            DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
            String s1 = sdf.format(forma.getDateDebut());
            String s2 = sdf.format(forma.getDateFin());
            Label date = new Label(s1 + " -> " + s2);
            //date.setUIID("LabelDate");

            Container formaInfo = new Container(new GridLayout(2, 2));
            formaInfo.add(new Label("Institut :"));
            Label inst = new Label(forma.getInstitut());
            //inst.setUIID("LabelContent");
            formaInfo.add(inst);
            formaInfo.add(new Label("Diplome :"));
            Label dip = new Label(forma.getDiplome());
            //dip.setUIID("LabelContent");
            formaInfo.add(dip);
            formaInfo.add(new Label("Description :"));
            Label desc = new Label(forma.getDescription());
            //desc.setUIID("LabelContent");
            formaInfo.add(desc);

            date.getAllStyles().setAlignment(formationbox.CENTER);
            formationbox.add(date);
            formationbox.add(formaInfo);
            Button btnsupp = new Button("Supp");
            //btnsupp.setUIID("ButtonSupp");
            SwipeableContainer sc = new SwipeableContainer(null, GridLayout.encloseIn(1, btnsupp), formationbox);

            conformations.add(sc);

            btnsupp.addActionListener(supp -> {
                fs.delete(forma.getId());
                CVController cvc = new CVController();
                cvc.getF().show();
            });

        }

        formation.addPointerPressedListener(l -> {
            if (x == 0) {
                conformations.setVisible(false);
                f.refreshTheme();
                x = 1;
            } else {
                conformations.setVisible(true);
                f.refreshTheme();
                x = 0;
            }

        });
        confor.add(conformations);
        t.addTab("", confor);

    }

    public void affichExperience() {

        img = theme.getImage("experience.png");
        Image img1 = img.scaled(20, 20);
        img2 = theme.getImage("add.png");
        Image img3 = img2.scaled(20, 20);

        Container conexp = new Container(BoxLayout.y());
        Container exptitle = new Container(new GridLayout(1, 2));

//        exptitle.setUIID("ContainerTitle");
//        conexp.setUIID("ContainerTitle");
        Label experience = new Label("Experience");

        experience.getAllStyles().setUnderline(true);
        experience.setIcon(img1);
        exptitle.add(experience);

        Button addexp = new Button();
//        addexp.setUIID("ouday");
        addexp.setIcon(img3);

        addexp.getAllStyles().setAlignment(addexp.RIGHT);

        addexp.addActionListener(lc1 -> {
            AjoutExperience ae = new AjoutExperience();
            ae.AjoutExp();
        });

        exptitle.add(addexp);
        conexp.add(exptitle);

        Container conexperiences = new Container(BoxLayout.y());

        ExperienceService es = new ExperienceService();

        List<Experience> lste = es.findByJober(j);
        for (Experience forma : lste) {
            Container experiencebox = new Container(BoxLayout.y());
//            experiencebox.setUIID("ContainerTitle");
            DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
            String s1 = sdf.format(forma.getDateDebut());
            String s2 = sdf.format(forma.getDateFin());
            Label date = new Label(s1 + " -> " + s2);
//            date.setUIID("LabelDate");

            Container formaInfo = new Container(new GridLayout(2, 2));
            formaInfo.add(new Label("Organisation :"));
            Label inst = new Label(forma.getOrganisation());
//            inst.setUIID("LabelContent");
            formaInfo.add(inst);
            formaInfo.add(new Label("Poste :"));
            Label dip = new Label(forma.getPoste());
//            dip.setUIID("LabelContent");
            formaInfo.add(dip);
            formaInfo.add(new Label("Description :"));
            Label desc = new Label(forma.getDescription());
//            desc.setUIID("LabelContent");
            formaInfo.add(desc);

            date.getAllStyles().setAlignment(experiencebox.CENTER);
            experiencebox.add(date);
            experiencebox.add(formaInfo);
            Button btnsupp = new Button("Supp");
//            btnsupp.setUIID("ButtonSupp");
            SwipeableContainer sc = new SwipeableContainer(null, GridLayout.encloseIn(1, btnsupp), experiencebox);

            conexperiences.add(sc);

            btnsupp.addActionListener(supp -> {
                es.delete(forma.getId());
                CVController cvc = new CVController();
                cvc.getF().show();
            });

        }

        experience.addPointerPressedListener(l -> {
            if (x == 0) {
                conexperiences.setVisible(false);
                f.refreshTheme();
                x = 1;
            } else {
                conexperiences.setVisible(true);
                f.refreshTheme();
                x = 0;
            }

        });
        conexp.add(conexperiences);
        t.addTab("", conexp);

    }

    public void affichStage() {

        img = theme.getImage("stage.png");
        Image img1 = img.scaled(20, 20);
        img2 = theme.getImage("add.png");
        Image img3 = img2.scaled(20, 20);

        Container consta = new Container(BoxLayout.y());
        Container statitle = new Container(new GridLayout(1, 2));

//        statitle.setUIID("ContainerTitle");
//        consta.setUIID("ContainerTitle");
        Label stage = new Label("Stage");

        stage.getAllStyles().setUnderline(true);
        stage.setIcon(img1);
        statitle.add(stage);

        Button addsta = new Button();
//        addsta.setUIID("ouday");
        addsta.setIcon(img3);

        addsta.getAllStyles().setAlignment(addsta.RIGHT);
        addsta.addActionListener(lc1 -> {
            AjoutStage ae = new AjoutStage();
            ae.AjoutSta();
        });
        statitle.add(addsta);
        consta.add(statitle);

        Container constages = new Container(BoxLayout.y());

        StageService es = new StageService();

        List<Stage> lste = es.findByJober(j);
        for (Stage forma : lste) {
            Container stagebox = new Container(BoxLayout.y());
//            stagebox.setUIID("ContainerTitle");
            DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
            String s1 = sdf.format(forma.getDateDebut());
            String s2 = sdf.format(forma.getDateFin());
            Label date = new Label(s1 + " -> " + s2);
//            date.setUIID("LabelDate");

            Container formaInfo = new Container(new GridLayout(2, 2));
            formaInfo.add(new Label("Organisation :"));
            Label inst = new Label(forma.getOrganisation());
//            inst.setUIID("LabelContent");
            formaInfo.add(inst);

            formaInfo.add(new Label("Description :"));
            Label desc = new Label(forma.getDescription());
//            desc.setUIID("LabelContent");
            formaInfo.add(desc);

            date.getAllStyles().setAlignment(stagebox.CENTER);
            stagebox.add(date);
            stagebox.add(formaInfo);
            Button btnsupp = new Button("Supp");
//            btnsupp.setUIID("ButtonSupp");
            SwipeableContainer sc = new SwipeableContainer(null, GridLayout.encloseIn(1, btnsupp), stagebox);

            constages.add(sc);

            btnsupp.addActionListener(supp -> {
                es.delete(forma.getId());
                CVController cvc = new CVController();
                cvc.getF().show();
            });

        }

        stage.addPointerPressedListener(l -> {
            if (x == 0) {
                constages.setVisible(false);
                f.refreshTheme();
                x = 1;
            } else {
                constages.setVisible(true);
                f.refreshTheme();
                x = 0;
            }

        });
        consta.add(constages);
        t.addTab("", consta);

    }

    public void affichProjets() {

        img = theme.getImage("projet.png");
        Image img1 = img.scaled(20, 20);
        img2 = theme.getImage("add.png");
        Image img3 = img2.scaled(20, 20);

        Container conexp = new Container(BoxLayout.y());
        Container exptitle = new Container(new GridLayout(1, 2));

//        exptitle.setUIID("ContainerTitle");
//        conexp.setUIID("ContainerTitle");
        Label experience = new Label("Projets");

        experience.getAllStyles().setUnderline(true);
        experience.setIcon(img1);
        exptitle.add(experience);

        Button addexp = new Button();
//        addexp.setUIID("ouday");
        addexp.setIcon(img3);

        addexp.getAllStyles().setAlignment(addexp.RIGHT);
        addexp.addActionListener(lc1 -> {
            AjoutProjet ac = new AjoutProjet();
            ac.AjoutPro();
        });
        exptitle.add(addexp);
        conexp.add(exptitle);

        Container conexperiences = new Container(BoxLayout.y());

        ProjetService es = new ProjetService();

        List<Projet> lste = es.findByJober(j);
        for (Projet forma : lste) {
            Container experiencebox = new Container(BoxLayout.y());
//            experiencebox.setUIID("ContainerTitle");
            DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
            String s1 = sdf.format(forma.getDateDebut());
            String s2 = sdf.format(forma.getDateFin());
            Label date = new Label(s1 + " -> " + s2);
//            date.setUIID("LabelDate");

            Container formaInfo = new Container(new GridLayout(2, 2));
            formaInfo.add(new Label("Organisation :"));
            Label inst = new Label(forma.getOrganisation());
//            inst.setUIID("LabelContent");
            formaInfo.add(inst);
            formaInfo.add(new Label("Nom :"));
            Label dip = new Label(forma.getNom());
//            dip.setUIID("LabelContent");
            formaInfo.add(dip);
            formaInfo.add(new Label("Description :"));
            Label desc = new Label(forma.getDescription());
//            desc.setUIID("LabelContent");
            formaInfo.add(desc);

            date.getAllStyles().setAlignment(experiencebox.CENTER);
            experiencebox.add(date);
            experiencebox.add(formaInfo);
            Button btnsupp = new Button("Supp");
//            btnsupp.setUIID("ButtonSupp");
            SwipeableContainer sc = new SwipeableContainer(null, GridLayout.encloseIn(1, btnsupp), experiencebox);

            conexperiences.add(sc);

            btnsupp.addActionListener(supp -> {
                es.delete(forma.getId());
                CVController cvc = new CVController();
                cvc.getF().show();
            });

        }

        experience.addPointerPressedListener(l -> {
            if (x == 0) {
                conexperiences.setVisible(false);
                f.refreshTheme();
                x = 1;
            } else {
                conexperiences.setVisible(true);
                f.refreshTheme();
                x = 0;
            }

        });
        conexp.add(conexperiences);
        t.addTab("", conexp);

    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

}
