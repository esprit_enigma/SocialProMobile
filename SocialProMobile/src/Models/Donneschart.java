/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author ASUS
 */
public class Donneschart {
    Double nbr;
    String year;

    public Donneschart() {
    }

    public Donneschart(Double nbr, String year) {
        this.nbr = nbr;
        this.year = year;
    }

    public Double getNbr() {
        return nbr;
    }

    public void setNbr(Double nbr) {
        this.nbr = nbr;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
    
    
}
