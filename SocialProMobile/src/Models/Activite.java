/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.Collection;


/**
 *
 * @author ASUS
 */
public class Activite {
    public int id;
    public String nom;
    public boolean valide;
    public String description;
    public Categorie categorie;
    public Collection<AvoirActivite> entreprises;

    public Activite() {
    }

    public Activite(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }
    
    public Activite(int id, Collection<AvoirActivite> entreprises) {
        this.id = id;
        this.entreprises = entreprises;
    }
    

  

    public Activite(int id, Categorie categorie) {
        this.id = id;
        this.categorie = categorie;
    }

    public Activite(int id, String nom, boolean valide, Categorie categorie) {
        this.id = id;
        this.nom = nom;
        this.valide = true;
       
        this.categorie = categorie;
    }

    public Activite(Categorie categorie,String nom) {
        this.nom = nom;
        this.categorie = categorie;
    }

    public Activite(int id, Categorie categorie,String nom ) {
        this.id = id;
         this.categorie = categorie;
        this.nom = nom;
       
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public boolean isValide() {
        return valide;
    }

    public void setValide(boolean valide) {
        this.valide = valide;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public Collection<AvoirActivite> getEntreprises() {
        return entreprises;
    }

    public void setEntreprises(Collection<AvoirActivite> entreprises) {
        this.entreprises = entreprises;
    }
    

    @Override
    public String toString() {
        return "Activite{" + "id=" + id + ", nom=" + nom + ", categorie=" + categorie + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + this.id;
        return hash;
    }

//    @Override
//    public boolean equals(Object obj) {
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final Activite other = (Activite) obj;
//        if (this.id != other.id) {
//            return false;
//        }
//        if (!Objects.equals(this.nom, other.nom)) {
//            return false;
//        }
//        return true;
//    }
    
    
}
