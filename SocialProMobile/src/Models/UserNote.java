/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Seddik
 */
public class UserNote {
    private int id ;
    
    private Cours cours ;
    
    private Jober jober ;
    
    private int note ;

    public UserNote() {
    }

    public UserNote(int id, Cours cours, Jober jober, int note) {
        this.id = id;
        this.cours = cours;
        this.jober = jober;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cours getCours() {
        return cours;
    }

    public void setCours(Cours cours) {
        this.cours = cours;
    }

    public Jober getJober() {
        return jober;
    }

    public void setJober(Jober jober) {
        this.jober = jober;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "UserNote{" + "id=" + id + ", cours=" + cours + ", jober=" + jober + ", note=" + note + '}';
    }
    
    
}
