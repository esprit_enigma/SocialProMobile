/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.Date;

/**
 *
 * @author Le Parrain
 */
public class Stage {
    private int id ;
    private String organisation;
    private Date dateDebut;
    private Date dateFin;
    private Date dateAjout;
    private String description;
    private Jober jober;

    public Stage(int id, Jober jober, String organisation, Date dateDebut, Date dateFin, Date dateAjout, String description) {
        this.id = id;
        this.organisation = organisation;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.dateAjout = dateAjout;
        this.description = description;
        this.jober = jober;
    }

    
    
    public Stage( Jober jober,String organisation, Date dateDebut, Date dateFin, Date dateAjout, String description) {
        this.organisation = organisation;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.dateAjout = dateAjout;
        this.description = description;
        this.jober = jober;
    }

    public Stage() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Jober getJober() {
        return jober;
    }

    public void setJober(Jober jober) {
        this.jober = jober;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Stage other = (Stage) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Stage{" + "organisation=" + organisation + ", jober=" + jober + '}';
    }
    
    
}
