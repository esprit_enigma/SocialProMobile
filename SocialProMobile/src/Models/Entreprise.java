/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;



import java.util.Collection;
import java.util.Date;
import javafx.scene.image.Image;

/**
 *
 * @author ASUS
 */
public class Entreprise {
    public int id;
    public String nom;
    public String adresse;
    public String nationalite;
    public String siteWeb;
    public int numTel;
    public String description;
    public String facebook;
    public String twitter;
    public String email;
    public String googlePlus;
    public String linkedin;
    public boolean valide;
    public Date dateAjout;
    public Date dateBloque;
    public String emailentrp;
    public User userId;
    public String imagepath;
    public String pdfpath;
    public boolean upload;
    public CodePostal codePostal;
    public Collection<AvoirActivite>activites;
    public Collection<ProjetEntreprise>projets;
    
    
    public Entreprise() {
    }

    public Entreprise( CodePostal codePostal,User userId,String nom, String adresse, String nationalite, String siteWeb, int numTel, String description, String facebook, String twitter, String googlePlus, String linkedin, boolean valide,String email,  boolean upload) {
        //idcp, user_id, nom, adresse, Nationalite, siteWeb, numTel,
        //description, facebook, twitter, googlePlus, linkedin, valide, emailentrp,upload
        this.nom = nom;
        this.adresse = adresse;
        this.nationalite = nationalite;
        this.siteWeb = siteWeb;
        this.numTel = numTel;
        this.description = description;
        this.facebook = facebook;
        this.twitter = twitter;
        this.email = email;
        this.googlePlus = googlePlus;
        this.linkedin = linkedin;
        this.valide = valide;
        this.userId=userId;
        this.upload = upload;
        this.codePostal = codePostal;
    }
     public Entreprise(String facebook,String twitter,String linkedin,String googlePlus){
         this.facebook = facebook;
        this.twitter = twitter;
         this.googlePlus = googlePlus;
        this.linkedin = linkedin;
     }
    public Entreprise(int id, CodePostal codePostal,User userId,String nom, String adresse, String nationalite, String siteWeb, int numTel, String description, String facebook, String twitter, String googlePlus, String linkedin, boolean valide,String email,Date dateAjout,Date dateBloque,  boolean upload) {
        //idcp, user_id, nom, adresse, Nationalite, siteWeb, numTel,
        //description, facebook, twitter, googlePlus, linkedin, valide, emailentrp,upload
        this.nom = nom;
        this.adresse = adresse;
        this.nationalite = nationalite;
        this.siteWeb = siteWeb;
        this.numTel = numTel;
        this.dateAjout=dateAjout;
        this.dateBloque=dateBloque;
        this.description = description;
        this.facebook = facebook;
        this.twitter = twitter;
        this.email = email;
        this.googlePlus = googlePlus;
        this.linkedin = linkedin;
        this.valide = valide;
        this.userId=userId;
        this.upload = upload;
        this.codePostal = codePostal;
        this.id=id;
    }

    public Entreprise(String nom, String adresse, String nationalite, int numTel, String description, String emailentrp) {
        this.nom = nom;
        this.adresse = adresse;
        this.nationalite = nationalite;
        this.numTel = numTel;
        this.description = description;
        this.emailentrp = emailentrp;
        //this.userId = userId;
    }
    
    public Entreprise( CodePostal codePostal,User userId,String nom, String adresse, String nationalite, String siteWeb, int numTel, String description, String facebook, String twitter, String googlePlus, String linkedin, boolean valide,String email,Date dateAjout,Date dateBloque,  boolean upload) {
        //idcp, user_id, nom, adresse, Nationalite, siteWeb, numTel,
        //description, facebook, twitter, googlePlus, linkedin, valide, emailentrp,upload
        this.nom = nom;
        this.adresse = adresse;
        this.nationalite = nationalite;
        this.siteWeb = siteWeb;
        this.numTel = numTel;
        this.dateAjout=dateAjout;
        this.dateBloque=dateBloque;
        this.description = description;
        this.facebook = facebook;
        this.twitter = twitter;
        this.email = email;
        this.googlePlus = googlePlus;
        this.linkedin = linkedin;
        this.valide = valide;
        this.userId=userId;
        this.upload = upload;
        this.codePostal = codePostal;
       
    }
    public Entreprise(int id, String nom, String adresse, String siteWeb, String description, CodePostal codePostal) {
        this.id = id;
        this.nom = nom;
        this.adresse = adresse;
        this.siteWeb = siteWeb;
        this.description = description;
        this.codePostal = codePostal;
  
        
    }

    public Entreprise(int id,CodePostal codepostal, String nom, String adresse, String nationalite, String siteWeb, int numTel, String description,String email) {
        this.id = id;
        this.nom = nom;
        this.adresse = adresse;
        this.nationalite = nationalite;
        this.siteWeb = siteWeb;
        this.numTel = numTel;
        this.description = description;
              this.email=email;
              this.codePostal=codepostal;
              
    }
        public Entreprise(int id,CodePostal codepostal, String nom, String adresse, String nationalite, String siteWeb, int numTel, String description,String email,String facebook,String twitter,String googleplus,String linkedin) {
        this.id = id;
        this.nom = nom;
        this.adresse = adresse;
        this.nationalite = nationalite;
        this.siteWeb = siteWeb;
        this.numTel = numTel;
        this.description = description;
              this.email=email;
              this.codePostal=codepostal;
              this.facebook=facebook;
              this.googlePlus=googleplus;
              this.twitter=twitter;
              this.linkedin=linkedin;
              
    }
     

    public Entreprise(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }
    

    public Entreprise(int id, Collection<AvoirActivite> activites) {
        this.id = id;
        this.activites = activites;
    }

    public Entreprise(Collection<ProjetEntreprise> projets) {
        
        this.projets = projets;
    }
    

    public boolean isUpload() {
        return upload;
    }

    public void setUpload(boolean upload) {
        this.upload = upload;
    }

    public Collection<AvoirActivite> getActivites() {
        return activites;
    }

    public void setActivites(Collection<AvoirActivite> activites) {
        this.activites = activites;
    }

    public Collection<ProjetEntreprise> getProjets() {
        return projets;
    }

    public void setProjets(Collection<ProjetEntreprise> projets) {
        this.projets = projets;
    }
    

    public Entreprise(String nom, CodePostal codePostal) {
        this.nom = nom;
        this.codePostal = codePostal;
    }

    public Entreprise(String nom, User userId) {
        this.nom = nom;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNationalite() {
        return nationalite;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    public String getSiteWeb() {
        return siteWeb;
    }

    public void setSiteWeb(String siteWeb) {
        this.siteWeb = siteWeb;
    }

    public int getNumTel() {
        return numTel;
    }

    public void setNumTel(int numTel) {
        this.numTel = numTel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGooglePlus() {
        return googlePlus;
    }

    public void setGooglePlus(String googlePlus) {
        this.googlePlus = googlePlus;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public boolean isValide() {
        return valide;
    }

    public void setValide(boolean valide) {
        this.valide = valide;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Date getDateBloque() {
        return dateBloque;
    }

    public void setDateBloque(Date dateBloque) {
        this.dateBloque = dateBloque;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getPdfpath() {
        return pdfpath;
    }

    public void setPdfpath(String pdfpath) {
        this.pdfpath = pdfpath;
    }

    

    public CodePostal getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(CodePostal codePostal) {
        this.codePostal = codePostal;
    }

    public String getEmailentrp() {
        return emailentrp;
    }

    public void setEmailentrp(String emailentrp) {
        this.emailentrp = emailentrp;
    }

    @Override
    public String toString() {
        return "Entreprise{" + "id=" + id + ", nom=" + nom + ", adresse=" + adresse + ", nationalite=" + nationalite + ", siteWeb=" + siteWeb + ", numTel=" + numTel + ", description=" + description + ", facebook=" + facebook + ", twitter=" + twitter + ", email=" + email + ", googlePlus=" + googlePlus + ", linkedin=" + linkedin + ", valide=" + valide + ", dateAjout=" + dateAjout + ", dateBloque=" + dateBloque + ", emailentrp=" + emailentrp + ", userId=" + userId + ", upload=" + upload + ", codePostal=" + codePostal + '}';
    }
    


    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Entreprise other = (Entreprise) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
    
    
}
