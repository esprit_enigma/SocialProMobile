/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Alaa
 */
public class DemandesRecomendation {

    private int id;
    private User expediteur_id;
    private User recepteur_id;
    private String etat;

    public DemandesRecomendation() {
    }

    public DemandesRecomendation(int id) {
        this.id = id;
    }

    public DemandesRecomendation(User exp, User rec, String etat) {
        this.expediteur_id = exp;
        this.recepteur_id = rec;
        this.etat = etat;
    }

    public DemandesRecomendation(int id, User expediteur_id, User recepteur_id, String etat) {
        this.id = id;
        this.expediteur_id = expediteur_id;
        this.recepteur_id = recepteur_id;
        this.etat = etat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getExpediteur_id() {
        return expediteur_id;
    }

    public void setExpediteur_id(User expediteur_id) {
        this.expediteur_id = expediteur_id;
    }

    public User getRecepteur_id() {
        return recepteur_id;
    }

    public void setRecepteur_id(User recepteur_id) {
        this.recepteur_id = recepteur_id;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    @Override
    public String toString() {
        return "DemandesRecomendation{" + "id=" + id + ", expediteur_id=" + expediteur_id.getUsername() + ", recepteur_id=" + recepteur_id.getUsername() + ", etat=" + etat + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DemandesRecomendation other = (DemandesRecomendation) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

}
