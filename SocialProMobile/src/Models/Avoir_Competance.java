/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.Date;

/**
 *
 * @author Le Parrain
 */
public class Avoir_Competance {
    private int id ;
    private int niveau;
    private Date dateAjout;
    private Competance competance;
    private Jober jober;

    public Avoir_Competance(int id, Competance competance, Jober jober, int niveau, Date dateAjout) {
        this.id = id;
        this.niveau = niveau;
        this.dateAjout = dateAjout;
        this.competance = competance;
        this.jober = jober;
    }

    public Avoir_Competance( Competance competance, Jober jober,int niveau, Date dateAjout) {
        this.niveau = niveau;
        this.dateAjout = dateAjout;
        this.competance = competance;
        this.jober = jober;
    }

    public Avoir_Competance() {
   }

    public void setId(int id) {
        this.id = id;
    }

    public int getNiveau() {
        return niveau;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Competance getCompetance() {
        return competance;
    }

    public void setCompetance(Competance competance) {
        this.competance = competance;
    }

    public Jober getJober() {
        return jober;
    }

    public void setJober(Jober jober) {
        this.jober = jober;
    }

    public int getId() {
        return id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Avoir_Competance other = (Avoir_Competance) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return competance.getNom() ;
    }
    
    
}
