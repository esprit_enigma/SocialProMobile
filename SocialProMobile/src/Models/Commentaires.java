/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.Date;



/**
 *
 * @author m_s info
 */
public class Commentaires {
    
    private int id ;
    
    private String contenu ; 
    
    private Date dateAjout ;
    
    private Date datemodification ;
    
    private User jober ;
    
    private Cours cours ;

    public Commentaires(int id, Cours cours, User jober, String contenu, Date dateAjout, Date datemodification, int note) {
 this.id = id;
        this.contenu = contenu;
        this.dateAjout = dateAjout;
        this.datemodification = datemodification;
        this.jober = jober;
        this.cours = cours;
    }

    public Cours getCours() {
        return cours;
    }

    public void setCours(Cours cours) {
        this.cours = cours;
    }

    public Commentaires(int id, Cours cours, User jober, String contenu, Date dateAjout, Date datemodification) {
        this.id = id;
        this.contenu = contenu;
        this.dateAjout = dateAjout;
        this.datemodification = datemodification;
        this.jober = jober;
        this.cours = cours;
    }

    public Commentaires()
    {
        
    }
    public Commentaires(int id, String contenu, Date dateAjout, Date datemodification, User jober) {
        this.id = id;
        this.contenu = contenu;
        this.dateAjout = dateAjout;
        this.datemodification = datemodification;
        this.jober = jober;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Date getDatemodification() {
        return datemodification;
    }

    public void setDatemodification(Date datemodification) {
        this.datemodification = datemodification;
    }

    public User getJober() {
        return jober;
    }

    public void setJober(User jober) {
        this.jober = jober;
    }

   

    @Override
    public String toString() {
        return "Commentaires{" + "id=" + id + ", contenu=" + contenu + ", dateAjout=" + dateAjout + ", datemodification=" + datemodification + ", jober=" + jober + ", cours=" + cours + '}';
    }

   
    
    
    
    
}
