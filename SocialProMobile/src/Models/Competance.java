/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package  Models;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Le Parrain
 */
public class Competance {
    private int id;
    private String nom;
    private Boolean valide ;
    private Date dateAjout;
    private List<Avoir_Competance> jobers;

    public Competance(String nom, Date dateAjout) {
        this.nom = nom;
        this.dateAjout = dateAjout;
    }

    public Competance(String nom, Boolean valide, Date dateAjout) {
        this.nom = nom;
        this.valide = valide;
        this.dateAjout = dateAjout;
    }

    public Competance(int aInt, String string, boolean aBoolean, Date date1) {
    this.id=aInt;
    this.nom=string;
    this.valide=aBoolean;
    this.dateAjout = date1;
    }

    public Competance() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Boolean getValide() {
        return valide;
    }

    public void setValide(Boolean valide) {
        this.valide = valide;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public List<Avoir_Competance> getJobers() {
        return jobers;
    }

    public void setJobers(List<Avoir_Competance> jobers) {
        this.jobers = jobers;
    }

    public int getId() {
        return id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Competance other = (Competance) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nom;
    }
    
    
    
}
