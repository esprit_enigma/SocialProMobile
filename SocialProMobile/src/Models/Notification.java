/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.Date;

/**
 *
 * @author Alaa
 */
public class Notification {

    private int id;
    private User user_id;
    private String text;
    private Date time; 
    private String state;
    private Date time_opened;

    public Notification() {
    }

    public Notification(int id, User user_id, String text, String state) {
        this.id = id;
        this.user_id = user_id;
        this.text = text;
        this.state = state;
    }
    
    
    
    public Notification(User user_id, String text, Date time, String state, Date time_opened) {
        this.user_id = user_id;
        this.text = text;
        this.time = time;
        this.state = state;
        this.time_opened = time_opened;
    }
    
    public Notification(User user_id, String text, String state) {
        this.user_id = user_id;
        this.text = text;
        this.state = state;
    }
    
    public Notification(int id, User user_id, String text, Date time, String state, Date time_opened) {
        this.id = id;
        this.user_id = user_id;
        this.text = text;
        this.time = time;
        this.state = state;
        this.time_opened = time_opened;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser_id() {
        return user_id;
    }

    public void setUser_id(User user_id) {
        this.user_id = user_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getTime_opened() {
        return time_opened;
    }

    public void setTime_opened(Date time_opened) {
        this.time_opened = time_opened;
    }

    @Override
    public String toString() {
        return "Notification{" + "id=" + id + ", user_id=" + user_id + ", text=" + text + ", time=" + time + ", state=" + state + ", time_opened=" + time_opened + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Notification other = (Notification) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
    
    
}
