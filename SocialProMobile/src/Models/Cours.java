/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 *
 * @author m_s info
 * 
 */
public class Cours {
    private int id ;
    
    private String description ;
    
    private String image ;
    
    private int note;
    
    private String tuto;
    
    private String courpdf;
    
    private Date dateAjout;
    
    private Date dateModification;
    
    private boolean etat ;
    
    private String titre ;
    
    private List<Question> questions;
    
    private Entreprise entreprise ;
    
    private Specialite specialite ;

    public Cours() {
        
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }

    public Specialite getSpecialite() {
        return specialite;
    }

    public void setSpecialite(Specialite specialite) {
        this.specialite = specialite;
    }

    public Cours(int id, Entreprise entreprise, Specialite specialite,String description, int note,  String tuto, String courpdf, Date dateAjout, Date dateModification, boolean etat, String titre,String image) {
        this.id = id;
        this.description = description;
        this.image = image;
        this.note = note;
        this.tuto = tuto;
        this.courpdf = courpdf;
        this.dateAjout = dateAjout;
        this.dateModification = dateModification;
        this.etat = etat;
        this.titre = titre;
        questions = new ArrayList<Question>();
        this.entreprise = entreprise;
        this.specialite = specialite;
    }
    
    public Cours( Entreprise entreprise, Specialite specialite,String description, int note,  String tuto, String courpdf, Date dateAjout, Date dateModification, boolean etat, String titre,String image) {
       
        this.description = description;
        this.image = image;
        this.note = note;
        this.tuto = tuto;
        this.courpdf = courpdf;
        this.dateAjout = dateAjout;
        this.dateModification = dateModification;
        this.etat = etat;
        this.titre = titre;
        questions = new ArrayList<Question>();
        this.entreprise = entreprise;
        this.specialite = specialite;
    }
    
    
    

   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public String getTuto() {
        return tuto;
    }

    public void setTuto(String tuto) {
        this.tuto = tuto;
    }

    public String getCourpdf() {
        return courpdf;
    }

    public void setCourpdf(String courpdf) {
        this.courpdf = courpdf;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    public boolean isEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }


   

    
    

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Question questions) {
        this.questions.add(questions);
    }

    @Override
    public String toString() {
        return "Cours{" + "id=" + id + ", description=" + description + ", image=" + image + ", note=" + note + ", tuto=" + tuto + ", courpdf=" + courpdf + ", dateAjout=" + dateAjout + ", dateModification=" + dateModification + ", etat=" + etat + ", titre=" + titre + ", questions=" + questions + ", entreprise=" + entreprise + ", specialite=" + specialite + '}';
    }
    
    

   
    
    
}
