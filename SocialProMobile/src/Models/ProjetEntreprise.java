/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.Date;


/**
 *
 * @author ASUS
 */
public class ProjetEntreprise {
    public int id;
    public String nom;
    public String destination;
    public String description;
    public String dateAjout;
    public Entreprise entreprise;

    public ProjetEntreprise(String nom, String destination, String description, String dateAjout, Entreprise entreprise) {
        this.nom = nom;
        this.destination = destination;
        this.description = description;
        this.dateAjout = dateAjout;
        this.entreprise = entreprise;
    }
    
    
    public ProjetEntreprise(int id,String nom, String destination, String description) {
        this.id=id;
        this.nom = nom;
        this.destination = destination;
        this.description = description;
    }
    

    public ProjetEntreprise() {
    }

    public ProjetEntreprise(int id, String nom) {
        this.id = id;
        this.nom = nom;
        
    }

    public ProjetEntreprise(int id, Entreprise entreprise, String nom, String destination, String description) {
        this.id = id;
        this.nom = nom;
        this.destination = destination;
        this.description = description;
        this.entreprise = entreprise;
    }

  
    

    public ProjetEntreprise(int id, Entreprise entreprise, String nom, String destination, String description, String dateAjout) {
        this.id = id;
        this.nom = nom;
        this.destination = destination;
        this.description = description;
        this.dateAjout = dateAjout;
        this.entreprise = entreprise;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(String dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }

    @Override
    public String toString() {
        return "ProjetEntreprise{" + "id=" + id + ", nom=" + nom + ", destination=" + destination + ", description=" + description + ", dateAjout=" + dateAjout + '}';
    }

    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.id;
        return hash;
    }

//    @Override
//    public boolean equals(Object obj) {
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final ProjetEntreprise other = (ProjetEntreprise) obj;
//        if (this.id != other.id) {
//            return false;
//        }
//        if (!Objects.equals(this.nom, other.nom)) {
//            return false;
//        }
//        return true;
//    }
//    
    
    
    
}
