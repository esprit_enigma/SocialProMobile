/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.Collection;
import java.util.Date;
import java.util.List;


/**
 *
 * @author ASUS
 */
public class Categorie {
    public int id;
    public String nom;
    public String dateAjout;
    public List<Activite> Activites;

    public Categorie() {
    }

    public Categorie(int id, String nom, String dateAjout) {
        this.id = id;
        this.nom = nom;
        this.dateAjout = dateAjout;
    }

    public Categorie(String nom) {
        this.nom = nom;
    }
    

    public Categorie(int id, List<Activite> Activites) {
        this.id = id;
        this.Activites = Activites;
    }
    

    public Categorie(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(String dateAjout) {
        this.dateAjout = dateAjout;
    }

    public List<Activite> getActivites() {
        return Activites;
    }

    public void setActivites(List<Activite> Activites) {
        this.Activites = Activites;
    }

    @Override
    public String toString() {
        return "Categorie{" + "id=" + id + ", nom=" + nom + ", dateAjout=" + dateAjout + '}';
    }
    



    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + this.id;
        return hash;
    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final Categorie other = (Categorie) obj;
//        if (this.id != other.id) {
//            return false;
//        }
//        if (!Objects.equals(this.nom, other.nom)) {
//            return false;
//        }
//        return true;
//    }
    
    
}
