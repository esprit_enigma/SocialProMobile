/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.Date;

/**
 *
 * @author Le Parrain
 */
public class Formation {
    private int id ;
    private String institut;
    private String diplome;
    private Date dateDebut;
    private Date dateFin;
    private Date dateAjout;
    private String description;
    private Jober jober;

    public Formation(int id, Jober jober, String institut, String diplome, String description, Date dateDebut, Date dateFin, Date dateAjout) {
        this.id = id;
        this.institut = institut;
        this.diplome = diplome;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.dateAjout = dateAjout;
        this.description = description;
        this.jober = jober;
    }



    
    public Formation(String institut, String diplome, Date dateDebut, Date dateFin, Date dateAjout, String description, Jober jober) {
        this.institut = institut;
        this.diplome = diplome;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.dateAjout = dateAjout;
        this.description = description;
        this.jober = jober;
    }

    public Formation() {
   }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    
    public String getInstitut() {
        return institut;
    }

    public void setInstitut(String institut) {
        this.institut = institut;
    }

    public String getDiplome() {
        return diplome;
    }

    public void setDiplome(String diplome) {
        this.diplome = diplome;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Jober getJober() {
        return jober;
    }

    public void setJober(Jober jober) {
        this.jober = jober;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Formation other = (Formation) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Formation{" + "institut=" + institut + ", diplome=" + diplome + ", jober=" + jober + '}';
    }
    
    
    
}
