/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.List;

/**
 *
 * @author Le Parrain
 */
public class Delegation {
    private int id ;
    private String nom ;
    private Ville ville;
    private List<Entreprise> entreprise;

    public Delegation() {
    }

    public Delegation(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public void setId(int id) {
        this.id = id;
    }
    

    public Delegation(int id,Ville ville ,String nom) {
        this.id = id;
        this.nom = nom;
        this.ville = ville;
    }
    
    public Delegation(String nom, Ville ville){
        this.nom=nom;
        this.ville=ville;
    }

    public int getId() {
        return id;
    }
    
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Ville getVille() {
        return ville;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }

    public List<Entreprise> getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(List<Entreprise> entreprise) {
        this.entreprise = entreprise;
    }

 
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Delegation other = (Delegation) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nom;
    }
    
    
    
}
