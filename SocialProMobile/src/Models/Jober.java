/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import com.codename1.ui.TextField;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Le Parrain
 */
public class Jober {

    private int id;
    private String nom;
    private String prenom;
    private Date dateDeNaissance;
    private String adresse;
    private String sexe;
    private String description;
    private int numTel;
    private Date dateCreation;

    private User user;
    private Delegation delegation;
    private Specialite specialite;

    private List<Projet> projets;
    private List<Experience> experiences;
    private List<Stage> stages;
    private List<Formation> formations;
    private List<Avoir_Competance> competances;

    public Jober() {
    }

    public Jober(int id) {
        this.id = id;
    }

    public Jober(int id, Delegation delegation, Specialite specialite, String nom, String prenom, Date dateDeNaissance, String adresse, String description, int numTel, Date dateCreation, User user, String sexe) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.dateDeNaissance = dateDeNaissance;
        this.adresse = adresse;
        this.sexe = sexe;
        this.description = description;
        this.numTel = numTel;
        this.dateCreation = dateCreation;
        this.user = user;
        this.delegation = delegation;
        this.specialite = specialite;

    }

    public Jober(Delegation delegation, Specialite specialite, String nom, String prenom, Date datenaiss, String adresse, String sexe, String description, int numTel, Date dateCreation, User user) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.dateDeNaissance = datenaiss;
        this.sexe = sexe;
        this.description = description;
        this.numTel = numTel;
        this.dateCreation = dateCreation;
        this.specialite = specialite;
        this.delegation = delegation;
        this.user = user;
    }

    public Jober(Delegation d, Specialite s, String text, String text0, SimpleDateFormat df, String text1, String toString, String text2, int i, Date d1, User user) {
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateDeNaissance() {
        return dateDeNaissance;
    }

    public void setDateDeNaissance(Date dateDeNaissance) {
        this.dateDeNaissance = dateDeNaissance;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumTel() {
        return numTel;
    }

    public void setNumTel(int numTel) {
        this.numTel = numTel;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Delegation getDelegation() {
        return delegation;
    }

    public void setDelegation(Delegation delegation) {
        this.delegation = delegation;
    }

    public Specialite getSpecialite() {
        return specialite;
    }

    public void setSpecialite(Specialite specialite) {
        this.specialite = specialite;
    }

    public List<Projet> getProjets() {
        return projets;
    }

    public void setProjets(List<Projet> projets) {
        this.projets = projets;
    }

    public List<Experience> getExperiences() {
        return experiences;
    }

    public void setExperiences(List<Experience> experiences) {
        this.experiences = experiences;
    }

    public List<Stage> getStages() {
        return stages;
    }

    public void setStages(List<Stage> stages) {
        this.stages = stages;
    }

    public List<Formation> getFormations() {
        return formations;
    }

    public void setFormations(List<Formation> formations) {
        this.formations = formations;
    }

    public List<Avoir_Competance> getCompetances() {
        return competances;
    }

    public void setCompetances(List<Avoir_Competance> competances) {
        this.competances = competances;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Jober other = (Jober) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nom + " " + prenom;
    }

}
