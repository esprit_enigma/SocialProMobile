/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.Date;

/**
 *
 * @author Alaa
 */
public class Recomendation {

    private int id;
    private User jobber_id;
    private User entreprise_id;
    private Date date_ajout;

    public Recomendation() {
    }

    public Recomendation(int id) {
        this.id = id;
    }
    
    public Recomendation(User jobber_id, User entreprise_id) {
        this.jobber_id = jobber_id;
        this.entreprise_id = entreprise_id;
    }
    
    public Recomendation(User jobber_id, User entreprise_id, Date date_ajout) {
        this.jobber_id = jobber_id;
        this.entreprise_id = entreprise_id;
        this.date_ajout = date_ajout;
    }

    public Recomendation(int id, User jobber_id, User entreprise_id, Date date_ajout) {
        this.id = id;
        this.jobber_id = jobber_id;
        this.entreprise_id = entreprise_id;
        this.date_ajout = date_ajout;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getJobber_id() {
        return jobber_id;
    }

    public void setJobber_id(User jobber_id) {
        this.jobber_id = jobber_id;
    }

    public User getEntreprise_id() {
        return entreprise_id;
    }

    public void setEntreprise_id(User entreprise_id) {
        this.entreprise_id = entreprise_id;
    }

    public Date getDate_ajout() {
        return date_ajout;
    }

    public void setDate_ajout(Date date_ajout) {
        this.date_ajout = date_ajout;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Recomendation other = (Recomendation) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Recomendation{" + "id=" + id + ", jobber_id=" + jobber_id.getUsername() + ", entreprise_id=" + entreprise_id.getUsername() + ", date_ajout=" + date_ajout + '}';
    }

}
