/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import Models.Nature;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Le Parrain
 */
public class Specialite {
    private int id;
    private String nom;
    private Date dateAjout;
    private Boolean valide;
    private Nature nature;
    private List<Jober> jober;

    public Specialite(int id, Nature nature ,String nom,Boolean valide, Date dateAjout) {
        this.id = id;
        this.nom = nom;
        this.dateAjout = dateAjout;
        this.valide = valide;
        this.nature = nature;
    }

    public Specialite( Nature nature,String nom, Boolean valide, Date dateAjout) {
        this.nom = nom;
        this.dateAjout = dateAjout;
        this.valide = valide;
        this.nature = nature;
    }

    public void setId(int id) {
        this.id = id;
    }

        
    
    public Specialite(String nom, Date dateAjout) {
        this.nom = nom;
        this.dateAjout = dateAjout;
    }

    public Specialite() {
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Boolean getValide() {
        return valide;
    }

    public void setValide(Boolean valide) {
        this.valide = valide;
    }

    public Nature getNature() {
        return nature;
    }

    public void setNature(Nature nature) {
        this.nature = nature;
    }

    public List<Jober> getJober() {
        return jober;
    }

    public void setJober(List<Jober> jober) {
        this.jober = jober;
    }

    public int getId() {
        return id;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Specialite other = (Specialite) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nom ;
    }
    
    
    
}
