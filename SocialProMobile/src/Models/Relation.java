/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Alaa
 */
public class Relation {

    private int id;
    private User idfollower;
    private User idfollowed;

    public Relation() {
    }

    public Relation(int id) {
        this.id = id;
    }

    public Relation(User idfollower, User idfollowed) {
        this.idfollowed = idfollowed;
        this.idfollower = idfollower;
    }

    public Relation(int id, User idfollower, User idfollowed) {
        this.id = id;
        this.idfollowed = idfollowed;
        this.idfollower = idfollower;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getIdfollower() {
        return idfollower;
    }

    public void setIdfollower(User idfollower) {
        this.idfollower = idfollower;
    }

    public User getIdfollowed() {
        return idfollowed;
    }

    public void setIdfollowed(User idfollowed) {
        this.idfollowed = idfollowed;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Relation other = (Relation) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Relation{" + "id=" + id + ", idfollower=" + idfollower.getUsername() + ", idfollowed=" + idfollowed.getUsername() + '}';
    }

}
