/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.Collection;


/**
 *
 * @author ASUS
 */
public class CodePostal {
    public int id;
    public String nom;
    public Collection<Entreprise> entreprises;
    public Delegation delegation;

    public CodePostal() {
    }

    public CodePostal(int id) {
        this.id = id;
       
    }
    

    public CodePostal(int id, String nom) {
        this.id = id;
        this.nom=nom;
    }
    
    public CodePostal(int id, String nom, Delegation delegation) {
        this.id = id;
        this.nom = nom;
        this.delegation = delegation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Delegation getDelegation() {
        return delegation;
    }

    public void setDelegation(Delegation delegation) {
        this.delegation = delegation;
    }

    public Collection<Entreprise> getEntreprises() {
        return entreprises;
    }

    public void setEntreprises(Collection<Entreprise> entreprises) {
        this.entreprises = entreprises;
    }
    

    @Override
    public String toString() {
        return "CodePotal{" + "id=" + id + ", nom=" + nom + ", delegation=" + delegation + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + this.id;
        return hash;
    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final CodePostal other = (CodePostal) obj;
//        if (this.id != other.id) {
//            return false;
//        }
//        if (!Objects.equals(this.nom, other.nom)) {
//            return false;
//        }
//        return true;
//    }
    
    
}
