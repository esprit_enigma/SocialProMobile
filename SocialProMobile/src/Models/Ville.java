/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.List;

/**
 *
 * @author Le Parrain
 */
public class Ville {
   private int id ;
   private String nom;
   
   private List<Delegation> delegations;

    public Ville(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public Ville(int id, String nom, List<Delegation> delegations) {
        this.id = id;
        this.nom = nom;
        this.delegations = delegations;
    }

    public Ville() {
    }
   
   
   public Ville(String nom){
       this.nom=nom;
   }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Delegation> getDelegations() {
        return delegations;
    }

    public void setDelegations(List<Delegation> delegations) {
        this.delegations = delegations;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ville other = (Ville) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nom;
    }
   
   
   
}
