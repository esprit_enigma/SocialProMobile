/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.Date;

/**
 *
 * @author ASUS
 */
public class AvoirActivite {
    private int id;
    private String description;
    private String dateAjout;
    public Activite activite;
    public Entreprise entreprise;

    public AvoirActivite(String description, Activite activite) {
        this.description = description;
        this.activite = activite;
    }
    
    public AvoirActivite() {
    }
    
    public AvoirActivite(int id, String description, String dateAjout) {
        this.id = id;
        this.description = description;
        this.dateAjout = dateAjout;
    }

    public AvoirActivite(int id) {
        this.id = id;
        
    }

    public AvoirActivite(int id, String description, String dateAjout, Activite activite, Entreprise entreprise) {
        this.id = id;
        this.description = description;
        this.dateAjout = dateAjout;
        this.activite = activite;
        this.entreprise = entreprise;
    }

    public AvoirActivite(int id, String description, Activite activite) {
         this.id = id;
         this.description = description;
         this.activite = activite;
    }
    
  

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(String dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Activite getActivite() {
        return activite;
    }

    public void setActivite(Activite activite) {
        this.activite = activite;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }

    @Override
    public String toString() {
        return "AvoirActivite{" + "id=" + id + ", description=" + description + ", dateAjout=" + dateAjout + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AvoirActivite other = (AvoirActivite) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    

    
    
    
}
