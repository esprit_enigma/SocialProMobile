<?php
require_once('connect.php');



$sql = "SELECT * FROM cours";
$result = $conn->query($sql);
$json = new SimpleXMLElement('<xml/>');
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      $mydata = $json->addChild('cours');
        $mydata->addChild('id',$row['id']);
        $mydata->addChild('iduser',$row['iduser']);
        $mydata->addChild('idspec',$row['idspec']);
        $mydata->addChild('description',$row['description']);
        $mydata->addChild('note',$row['note']);
        $mydata->addChild('tuto',htmlspecialchars($row["tuto"]));
        $mydata->addChild('courspdf',$row['courpdf']);
        $mydata->addChild('date_ajout',$row['date_ajout']);
        $mydata->addChild('date_modification',$row['date_modification']);
        $mydata->addChild('etat',$row['etat']);
        $mydata->addChild('titre',$row['titre']);
        $mydata->addChild('image',$row['image']);
        }
} else {
    echo "0 results";
}
$conn->close();
	 echo( json_encode ($json));
?>